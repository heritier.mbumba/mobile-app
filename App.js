import React, {useState, useEffect} from 'react';
import { Provider } from 'react-redux'
import {Provider as PaperProvider} from 'react-native-paper'
import { PersistGate } from 'redux-persist/lib/integration/react'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import {AppState} from 'react-native';
import { AppLoading } from 'expo';
import { Ionicons } from '@expo/vector-icons'
import { storage, persistor } from './app/redux/store/store';
import Theme from './app/theme/Theme';
import {enableScreens} from 'react-native-screens'
import Root from './navigation/v5/Root';
import * as Network from 'expo-network'
import * as Sentry from 'sentry-expo';
import Constants from 'expo-constants';


Sentry.init({
  dsn: 'https://fd92e02aebbf4a7d8d2087b3a15ce9b7@sentry.io/5042114',
  enableInExpoDevelopment: true,
});
Sentry.setRelease(Constants.manifest.revisionId);



enableScreens();

export default function App() {

  /**
   * COMPONENT STATE
   */
  const [isAppLoading, setAppLoading] = useState(false)
  const [getAppState, setAppState] = useState(AppState.currentState)
  const [getNetwork, setNetwork] = useState()

  /**
   * HANDLER APP STATE 
   * @param {*} nextAppState 
   */
  const _handleAppStateChange = nextAppState => {
    if(getAppState.match(/inactive|background/) && nextAppState === 'active'){
      setAppState(nextAppState)
    }
  }

  /**
   * CHECK NETWORK STATUS
   */
  const _getNetworkStatus = async () => {
    const {isConnected} = await Network.getNetworkStateAsync()

    setNetwork(isConnected)
   
  }

  /**
   * EFFECT FOR MOUNT AND UNMOUNT
   */
  useEffect(()=>{
    _getNetworkStatus()
    AppState.addEventListener('change', _handleAppStateChange)

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange)
    }
  }, [getAppState, getNetwork])

 
  /**
   * CHECK IF THE APP IS IN LOADING STATE
   */
  if(!isAppLoading){
    return <AppLoading
              startAsync={loadResourcesAsync}
              onError={handleLoadingError}
              onFinish={handleFinishLoading(setAppLoading)} />
  }

  /**
   * NOT NETWORK
   */
  if(!getNetwork){
    return <AppLoading />
  }
  
/**
 * RENDER THE APP
 */
  return <Provider store={storage}>
              <PersistGate loading={null} persistor={persistor}>
                  <PaperProvider theme={Theme}>
                    <Root />
                  </PaperProvider>
              </PersistGate>
          </Provider>

/**
 * LOAD RESOURCES ASYNC
 */
  async function loadResourcesAsync() {
    await Promise.all([
      Font.loadAsync({
        ...Ionicons.font,
       
  
      }),
      Asset.loadAsync([
        //ANDROID
        require('./assets/images/android/logo-hdpi.png'),
        require('./assets/images/android/logo-ldpi.png'),
        require('./assets/images/android/logo-mdpi.png'),
        require('./assets/images/android/logo-xhdpi.png'),
        require('./assets/images/android/logo-xxxhdpi.png'),
        require('./assets/images/android/xxhdpi.png'),
        //IOS
        require('./assets/images/ios/logo@1.png'),
        require('./assets/images/ios/logo@2.png'),
        require('./assets/images/ios/logo@3.png'),
        require('./assets/icon.png')
      ]),
      
    ])

    setAppLoading(true)
  }

  
  
}


/**
 * 
 * @param {*} error 
 */

function handleLoadingError(error){
  console.warn(error)
}

/**
 * 
 * @param {*} setAppLoading 
 */

function handleFinishLoading(setAppLoading){
  setAppLoading(true)
}
