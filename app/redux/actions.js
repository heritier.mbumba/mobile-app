export const V = 'v1'
export const PORT = '2888'
export const DEV = 'http://mobile.hostserv.co.za'
export const LIVE = 'http://mobile-api.1-grid.com'
export const ENDPOINT = `${LIVE}:${PORT}/${V}`
export const CONNECTED = 'CONNECTED'
export const LOGIN_SUCCESS_MSG = 'LOGIN_SUCCESS_MSG'
export const RESPONSE = 'RESPONSE'
export const ERROR = 'ERROR'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const NOT_LOGIN = 'NOT_LOGIN'
export const IS_LOADING = 'IS_LOADING'
export const SHOW_MENU_CONTEXT = 'SHOW_MENU_CONTEXT'
export const LOGOUT = 'LOGOUT'
export const USE_DIFFERENT_ACCOUNT = 'USE_DIFFERENT_ACCOUNT'
export const GET_NOTIFICATIONS = 'GET_NOTIFICATIONS'
export const RESET_MSG = 'RESET_MSG'
export const CLOSE_SNACK = 'CLOSE_SNACK'
export const SVGP = 'SVGP'
export const SUCCESS = 'SUCCESS'
export const CLEAR_MESSAGE = 'CLEAR_MESSAGE'
export const EXPIRYING_PRODUCTS = 'EXPIRYING_PRODUCTS'
export const UPDATE_PROFILE = 'UPDATE_PROFILE'
export const EDIT_PROFILE = 'EDIT_PROFILE'
export const IS_EDITING = 'IS_EDITING'
export const GET_FEELING = 'GET_FEELING'
export const IS_LOGGING_OUT = 'IS_LOGGING_OUT'
export const HEADER_HEIGHT = 'HEADER_HEIGHT'
export const FETCHING_PROFILE = 'FETCHING_PROFILE'
export const PROFILE_FETCHED = 'PROFILE_FETCHED'
export const REMOTE_ADDRESS = 'https://1-grid.com/client/'
export const NOTIFICATIONS = 'NOTIFICATIONS'
export const UPDATE_NOTIFICATION = 'UPDATE_NOTIFICATION'
export const IS_SAVING = 'IS_SAVING'
export const IS_UPDATING = 'IS_UPDATING'
export const BUTTON_LOADING = 'BUTTON_LOADING'
export const LOAD_MORE = 'LOAD_MORE'
export const INCREMENT_INDEX = 'INCREMENT_INDEX'
export const DECREMENT_INDEX = 'DECREMENT_INDEX'


/**
 * BILLING
 */
export const UNPAID_INVOICES = 'UNPAID_INVOICES'
export const GET_AUTO_AUTH_URL = 'GET_AUTO_AUTH_URL'
export const IS_FETCHING_URL = 'IS_FETCHING_URL'
export const GET_INVOICES = 'GET_INVOICES'




/**
 * SERVICES
 */

 export const GET_ACTIVE_SERVICES = 'GET_ACTIVE_SERVICES'
 export const GET_SUSPENDED_SERVICES = 'GET_SUSPENDED_SERVICES'
 export const VIEW_SERVICE = 'VIEW_SERVICE'
 export const GET_SERVICE_SETTING = 'GET_SERVICE_SETTING'
 export const SUSPENDED_SERV_INVOICE = 'SUSPENDED_SERV_INVOICE'
 export const INVOICE_PAID = 'INVOICE_PAID'


 /**
  * DOMAINS
  */

  export const GET_ACTIVE_DOMAINS = 'GET_ACTIVE_DOMAINS'
  export const GET_EXPIRED_DOMAINS = 'GET_EXPIRED_DOMAINS'
  export const GET_SINGLE_DOMAIN = 'GET_SINGLE_DOMAIN'
  export const AUTO_RENEW_DOMAIN = 'AUTO_RENEW_DOMAIN'


  /**
   * SUPPORT
   */

   export const GET_GROUPS = 'GET_GROUPS'
   export const OPEN_TICKET = 'OPEN_TICKET'
   export const GET_TICKETS = 'GET_TICKETS'
   export const GET_CONVERSATIONS = 'GET_CONVERSATIONS'
   export const GET_SALES_TICKETS = 'GET_SALES_TICKETS'
   export const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION'
   export const FEEDBACK_SEND = 'FEEDBACK_SEND'
   export const FRESHDESK_ERROR = 'FRESHDESK_ERROR'
   export const IS_REPLYING_TICKET = 'IS_REPLYING_TICKET'
   export const ACTIVE_TICKET = 'ACTIVE_TICKET'
   export const FETCHING_CONVERSATIONS = 'FETCHING_CONVERSATIONS'
   export const TICKET_RESOLVED = 'TICKET_RESOLVED'
   export const TICKET_CLOSED = 'TICKET_RESOLVED'



   /**
    * RESET PASS
    */
   export const STEP_ONE = 'STEP_ONE'
   export const STEP_TWO = 'STEP_TWO'
   export const STEP_THREE = 'STEP_THREE'
   export const RESET_PASSWORD = 'RESET_PASSWORD'

  