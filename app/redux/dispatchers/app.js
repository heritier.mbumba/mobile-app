import { 
    GET_NOTIFICATIONS, RESET_MSG, CLOSE_SNACK, 
    IS_LOADING, CLEAR_MESSAGE, 
    EXPIRYING_PRODUCTS, IS_EDITING, GET_FEELING, UPDATE_NOTIFICATION, FEEDBACK_SEND, SUCCESS, ERROR } from '../actions'

import moment from 'moment'
import {api} from '../functions'
import { route } from '../routes'


/**
 * SAVE DEVICE EXPO UNIQUE TOKEN
 * @param {*} token 
 */
export const saveExpoToken = (token, platform) => {
    return async (dispatch, state) => {

        const response = await api.auth(route.ADD_EXPO_TOKEN, {token:token, platform:platform}, {method:'POST', auth:state().auth.token})
            
            if(response.status == 409){
                return
            }

            if(response.status == 504){
                dispatch({type:ERROR, payload:response.message})
                return
            }

    }
}

/**
 * GET NOTIFICATIONS
 */

export const getNotification = () => {
    return async (dispatch, state) => {
        const response = await api.auth(route.GET_NOTIFICATIONS, '', {auth:state().auth.token})
            
        if(response.status == 200){
            
            dispatch({
                type:GET_NOTIFICATIONS,
                payload:response.data
            })
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            
            return
        }
    }
}


/**
 * RESET RESPONSE MESSAGE
 */

export const resetMsg = () => {
    return dispatch => {
        dispatch({
            type:RESET_MSG
        })
    }
}

/**
 * CLOSE SNACK
 */
export const closeSnack = () => {
    return dispatch => {
        dispatch({
            type:CLOSE_SNACK
        })
    }
}




export const clearErrorDispatcher = () => {
    return dispatch => {
        const timer = setTimeout(()=>{
            
            dispatch({
                type:CLEAR_MESSAGE
            })
        }, 6000)
        
    }
} 


/**
 * GET EXPIRING PRODUCTS
 */
export const getExpiredProducts = () => {
    return (dispatch, state) => {
        
        let object = []
        if(state().domain.activeDomains.length > 0){
            const expiryingDomains = state().domain.activeDomains.filter(domain=>{
                const expirydate = moment(domain.expirydate)
                const now = moment()

                return expirydate.diff(now, 'days') <= 30
    
            })

            expiryingDomains.map(domain=>{
                object.push({key:'Domain', id:domain.id, name:domain.domain, date:domain.expirydate, duedate:domain.nextduedate, amount:domain.recurringamount})
            })
            
        }

        if(state().service.activeServices.length > 0){
            
            const expiryingServices = state().service.activeServices.filter(service=>{
                const expirydate = moment(service.nextduedate)
                const now = moment()

                return expirydate.diff(now, 'days') <= 10 && service.billingcycle !== 'Free Account'
    
            })

            expiryingServices.map(service=>{
                object.push({key:'Hosting', id:service.id, name:service.product.name, expirydate:service.nextduedate, duedate:service.nextduedate, recurringamount:service.amount})
            })
        }
        
        if(object.length > 0){
            dispatch(
                {
                    type:EXPIRYING_PRODUCTS,
                    payload:object
                }
            )
        }
        
        
    }
}

/**
 * IS EDITING
 */
export const isEditing = () => {
    return dispatch => {
        dispatch({type:IS_EDITING})
    }
}

/**
 * SEND FEEDBACK
 */

 export const feedBackDispather = obj => {
     return async (dispatch, state) => {
         const data = {feeling:state().app.feeling, message:obj}

         const response = await api.auth(route.FEEDBACK, data, {method:'POST', auth:state().auth.token})
             if(response.status == 200){
                 dispatch(
                     {
                         type:FEEDBACK_SEND
                     }
                 )
             }
             // NETWORD FAILED
             if(response.status == 504){
                dispatch({type:ERROR, payload:response.message})
                return
            }
         
     }
 }

 /**
  * GET FEELING
  */

  export const getFeeling = feeling=> {
      return dispatch => {
          dispatch(
              {
                  type:GET_FEELING,
                  payload:feeling
              }
          )
      }
  }

/**
 * GET FEEDBACK
 */

export const getFeedbackMessage = (message, deviceinfo) => {
    return async (dispatch, state) => {
        dispatch({type:IS_LOADING})
        const feedback = state().auth.user ? {
            feeling:state().app.feeling, 
            message:message, 
            userid:state().auth.user.id, 
            firstname:state().auth.user.firstname, 
            lastname:state().auth.user.lastname,
            email:state().auth.user.email,
            device:{...deviceinfo}
        } : {
            feeling:'', 
            message:message, 
            userid:'', 
            firstname:'', 
            lastname:'',
            email:'',
            device:{...deviceinfo}
        }
        
        const response = await api.auth(route.FEEDBACK, feedback, {method:'POST', auth:state().auth.token})
        
        if(response.status == 200){
            dispatch(
                {
                    type:SUCCESS,
                    payload:'We have successfully received your feedback'
                }
            )
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            return
        }
    }
}


/**
 * ACTIVITY
 */
export const logActivity = (action, obj) => {
    return async (dispatch, state) => {
        const activityObj = state().auth.user ? {...obj, userid:state().auth.user.id, action:action} : {...obj, userid:'', action:action}
            
            const response = await api.auth(route.LOG_ACTIVITY, activityObj, {method:'POST', auth:state().auth.token})
            
            if(response.status == 200){
               
                return 
            }
            // NETWORD FAILED
            if(response.status == 504){
                
                dispatch({type:ERROR, payload:response.message})
                return
            }
    }
}