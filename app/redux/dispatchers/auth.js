import {api, errorReport} from '../functions'
import {
    NOT_LOGIN, 
    IS_LOADING, 
    CONNECTED, 
    RESPONSE, ERROR, LOGIN_SUCCESS, 
    LOGIN_SUCCESS_MSG, LOGOUT, 
    USE_DIFFERENT_ACCOUNT, 
    SVGP, SUCCESS, 
    EDIT_PROFILE, PROFILE_FETCHED, FETCHING_PROFILE } from '../actions'
import { route } from '../routes';
import { logActivity } from './app';


/**
 * CHECK AUTH
 */
export const checkAuth = () => {
    return (dispatch, getState) => {

            if(getState().auth.expiresIn == null){
                dispatch(
                    {
                        type:NOT_LOGIN
                    }
                )
                return
            }

            const isTimeOut = getState().auth.expiresIn < new Date().getTime()

            if(isTimeOut){
                dispatch({
                    type:NOT_LOGIN
                })
            }
                 
    }
}

/**
 * 
 * @param {*} user 
 */

export const LoginRequestDispatcher = user => {
    return async dispatch => {
        
        dispatch({type:IS_LOADING})

        const response = await api.nonauth(route.LOGIN, user, {method:'POST'})
            
            dispatch({type:CONNECTED})
 
            //THIS IS JUST FOR GIVING A CLIENT A FEELING THAT WE ARE PROCESSING THE REQUEST
            const timer = setTimeout(()=>{

                //DEPLAY A MESSAGE 
                dispatch({type:RESPONSE})

                if(!response){
                    dispatch({type:ERROR, payload:'Something went wrong. Please check your internet connection'})
                    return
                }

                if(response){
                    //DEPLAY A MESSAGE 
                clearTimeout(timer)
                }

                
                // NETWORD FAILED
                if(response.status == 504){
                    dispatch({type:ERROR, payload:response.message})
                    clearTimeout(timer)
                    return
                }

                // UNAUTHORIZED REQUEST
                if(response.status == 412){
                    dispatch({type:ERROR, payload:response.message})
                    clearTimeout(timer)
                    return
                }


                //IF THE CLIENT EMAIL DOES NOT EXIST
                if(response.status == 404) {
                    dispatch({type:ERROR, payload:response.message})
                    clearTimeout(timer)
                    return
                }

                //BRUTE FORCE LOCK
                if(response.status == 429) {
                    
                    dispatch({type:ERROR, payload:response.message})
                    clearTimeout(timer)
                    return
                }
                //TIME OUT
                if(response.status == 500) {
                    
                    dispatch({type:ERROR, payload:'Something went wrong. Please try again later'})
                    clearTimeout(timer)
                    return
                }

                //FORBIDDEN
                if(response.status == 403) {
                    
                    dispatch({type:ERROR, payload:response.message})
                    clearTimeout(timer)
                    return
                }

                //SUCCESSFUL LOGGED IN
                if(response.status == 200) {
                    clearTimeout(timer)
                    dispatch({type:LOGIN_SUCCESS_MSG})
                    dispatch({type:SVGP, payload:user.password})

                    setTimeout(()=>{
                        dispatch(
                            {
                                type:LOGIN_SUCCESS,  
                                token:response.token, 
                                expiresIn:new Date().getTime() + (response.expiresIn * 1000)
                            }
                        )
                    }, 2000) 
                    
                }
            }, 2000)

        
    }
}

/**
 * LOGOUT
 */

export const logout = () => {
    return dispatch => {
        dispatch(logActivity('logout', {}))
        dispatch({type:IS_LOADING})
        setTimeout(()=>{
            dispatch({
                type:LOGOUT
            })
        }, 1000)    
    }
}

/**
 * 
 * @param {*} timeout 
 */

export const autoLogout = timeout => {
    return (dispatch, state) => {
        timer = setTimeout(()=>{}, timeout)
    }
}

/**
 * USE DIFFERENCE ACCOUNT
 */

export const useDifferentAccount = () => {
    return dispatch => {
        dispatch(
            {
                type:USE_DIFFERENT_ACCOUNT
            }
        )
    }
}





/**
 * UPDATE CLIENT PROFILE DISPATCHER
 */

export const updateProfileInfo = () => {
    return async (dispatch, state) => {
        
        dispatch({type:IS_LOADING})

        const response = await api.auth(route.UPDATE_PROFILE, state().auth.user, {method:'PATCH', auth:state().auth.token})
            
        if(response.status == 202) {
            dispatch(
                    {
                        type:SUCCESS,
                        payload:'Account Info Updated',
                        newClient:response.client
                    }
                )
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            
            return
        }

            //FORBIDDEN
        if(response.status == 403) {
            
            dispatch({type:ERROR, payload:response.message})
            
            return
        }

        
    }
}


/**
 * GET PROFILE
 */

export const getProfile = () => {
    return async (dispatch, state) => {
        dispatch({type:IS_LOADING})
        
        const response = await api.auth(route.PROFILE, '', {auth:state().auth.token})

        dispatch({type:FETCHING_PROFILE})

        if(response.status == 200){
            
            dispatch(
                {
                    type:PROFILE_FETCHED,
                    payload:response.data
                }
            )
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            
            return
        }
       
    }
}


/**
 * EDIT PROFILE
 * @param {EDIT} object 
 */

export const editProfile = object => {
    return dispatch => {
        //dispatch({type:IS_EDITING})
        dispatch({
            type:EDIT_PROFILE,
            payload:object
        })
    }
}

