import { UNPAID_INVOICES, GET_AUTO_AUTH_URL, IS_FETCHING_URL, GET_INVOICES, NOT_LOGIN, ERROR, IS_LOADING } from "../actions"
import _ from 'lodash'
import { api, errorReport } from "../functions"
import { route } from "../routes"


/**
 * GET PAID AND UNPAID INVOICES
 */
export const getInvoices = () => {
    return dispatch => {
        dispatch({type:IS_LOADING})
        dispatch(getUnpaidInvoices())
        dispatch(getPaidInvoices())
    }
}

/**
 * GET UNPAID INVOICES
 */
export const getUnpaidInvoices = () => {
    return async (dispatch, state) => {
       
        const response =  await api.auth(route.GET_UNPAID_INVOICES, '', {auth:state().auth.token})
           
        if(response.status==200){
            dispatch(
                {
                    type:UNPAID_INVOICES,
                    payload:response.data
                }
            )
        }
        // NETWORD FAILED
        if(response.status == 504){
         dispatch({type:ERROR, payload:response.message})
         return
        }
        
    }
}

/**
 * GET PAID INVOICES
 */
export const getPaidInvoices = () => {
    return async (dispatch, state) => {
        const response =  await api.auth(route.GET_PAID_INVOICES, '', {auth:state().auth.token})
            
        if(response.status==200){
            dispatch(
                {
                    type:GET_INVOICES,
                    payload:response.data
                }
            )
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            return
        }
    }
}
