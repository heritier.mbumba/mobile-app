import { GET_ACTIVE_DOMAINS, AUTO_RENEW_DOMAIN, GET_EXPIRED_DOMAINS, IS_LOADING, LOAD_MORE, BUTTON_LOADING, INCREMENT_INDEX, DECREMENT_INDEX, ERROR } from "../actions"
import moment from "moment"
import { api, errorReport } from "../functions"
import { route } from "../routes"
import { getProfile } from "./auth"


/**
 * GET DOMAINS
 */
export const getDomains = () => {
    return  (dispatch) => {
        dispatch({type:IS_LOADING})
        dispatch(getActiveDomains())
        dispatch(getExpiredDomains())
    }
}

/**
 * 
 * @param {*} id 
 */
export const getActiveDomains = () => {
    return async (dispatch, state)=>{
        const response = await api.auth(route.GET_ACTIVE_DOMAINS, '', {auth:state().auth.token})
         
        if(response.status == 200) {
            dispatch({
                type:GET_ACTIVE_DOMAINS,
                payload:response.data
            })
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            return
        }
    }
    
}

/**
 * 
 * @param {*} id 
 */

export const getExpiredDomains = () => {
    return async (dispatch, state) => {
        const response = await api.auth(route.GET_EPIRED_DOMAINS, '', {auth:state().auth.token})
        if(response.status == 200) {
            dispatch({
                type:GET_EXPIRED_DOMAINS,
                payload:response.data
            })
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            return
        }
    }
}

/**
 * AUTO RENIEW DOMAIN
 */

export const RenewDomainDispatcher = (id) => {
    return async (dispatch, state) => {
        
        const response = await api.auth(route.AUTO_RENEW_DOMAIN, {id:id}, {auth:state().auth.token, method:'PATCH'})

        if(response == 200) {
            dispatch({type:AUTO_RENEW_DOMAIN, status:response.data.donotrenew, payload:response.data})
            dispatch(getProfile())
        }

    }
}


export const loadMore = (instance, index) => {
    return (dispatch) => {
        dispatch({type:BUTTON_LOADING})

        dispatch(incrementIndex())

        if(instance == 'domains'){
           
            setTimeout(()=>{
                dispatch(
                    {
                        type:LOAD_MORE,
                    }
                )
            }, 1000)
            
        }
      
    }
}

export const previousLoad = () => {
    return dispatch => {
        dispatch({type:BUTTON_LOADING})
        dispatch(decrementIndex())
        setTimeout(()=>{
            dispatch(
                {
                    type:LOAD_MORE,
                }
            )
        }, 1000)

    }
}

export const incrementIndex = () => {
    return (dispatch, state)=>{
        const nextIndex = state().domain.index + 20
        dispatch({type:INCREMENT_INDEX, payload:nextIndex})
    }
}


export const decrementIndex = () => {

    return (dispatch, state)=>{
        const nextIndex = state().domain.index - 20
        dispatch({type:DECREMENT_INDEX, payload:nextIndex})
    }

}