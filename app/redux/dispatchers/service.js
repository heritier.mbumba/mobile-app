import { SUSPENDED_SERV_INVOICE, GET_ACTIVE_SERVICES, GET_SUSPENDED_SERVICES, IS_LOADING, INVOICE_PAID, ERROR} from "../actions"
import { api, errorReport } from "../functions"
import { route } from "../routes"

export const getServices = () => {
    return  (dispatch) => {
        dispatch({type:IS_LOADING})
        dispatch(getActiveServices())
        dispatch(getSuspendedServices())
    }
}


/**
 * 
 * @param {*} id 
 */

export const getActiveServices = () => {
    return async (dispatch, state) => {
        const response = await api.auth(route.GET_ACTIVE_SERVICES, '', {auth:state().auth.token})
        if(response.status == 200){
            let addons = []
            response.data.map(d=>{
                
                if(d.hostingaddons.length > 0){
                    
                    d.hostingaddons.map(a=>{
                        
                        addons.push({...a})
                    })
                }
            })
            
            dispatch(
                {
                    type:GET_ACTIVE_SERVICES,
                    payload:response.data,
                    addons:addons
                }
            )
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            
            return
        }
    }
}


/**
 * 
 */
export const getSuspendedServices = () => {
    return async (dispatch, state) => {
        const response = await api.auth(route.GET_SUSPENDED_SERVICES, '', {auth:state().auth.token})

        if(response.status == 200){
            dispatch(
                {
                    type:GET_SUSPENDED_SERVICES,
                    payload:response.data
                }
            )
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            
            return
        }
    }
}


/**
 * GET INVOICE ID FOR SUSPENDED SERVICE
 */
export const getInvoiceIdForSuspendedService = id => {
    return async (dispatch, state) => {
        dispatch({type:IS_LOADING})
        const response = await api.auth(route.GET_INVOICEID_SUSP + id, '', {auth:state().auth.token})
            
        if(response.status==204){
            
            dispatch({
                type:INVOICE_PAID
            })
        }
        if(response.status == 200){
            
            dispatch({
                type:SUSPENDED_SERV_INVOICE,
                payload:response.data
            })
        }
        // NETWORD FAILED
        if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            return
        }
    }
}