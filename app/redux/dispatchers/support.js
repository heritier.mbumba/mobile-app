import {GET_GROUPS, IS_LOADING, OPEN_TICKET, GET_TICKETS, 
    GET_CONVERSATIONS, ERROR, FRESHDESK_ERROR, 
    IS_REPLYING_TICKET, ACTIVE_TICKET, FETCHING_CONVERSATIONS, 
    UPDATE_NOTIFICATION, IS_UPDATING, TICKET_RESOLVED, TICKET_CLOSED, BUTTON_LOADING } from "../actions";
import { api, errorReport } from "../functions";
import { route } from "../routes";
import {getNotification} from './app'

export const getTickets = () => {
    return async (dispatch, state) => {

        
        try {
            dispatch({type:IS_LOADING})
           const response = await api.auth(route.GET_TICKETS, '', {auth:state().auth.token})
           
           
           /**
            * CHECK RESPONSE
            */
           if(response.status == 200)
           { 
               dispatch(
                   {
                       type:GET_TICKETS,
                       payload:response.data
                       
                   }
               )
               return
           }

           // NETWORD FAILED
           if(response.status == 504){
            dispatch({type:ERROR, payload:response.message})
            clearTimeout(timer)
            return
        }

           /**
            * CHECK ERROR
            */
           if(response.status == 500){
            dispatch(
                {
                    type:ERROR,   
                }
            )
            return 
           }
       } catch (error) {
        dispatch(errorReport())
       }

    }
}

export const getGroups = () => {
    return async (dispatch, state) => {

        try {
            const response = await api.auth(route.TICKETS_GROUPS, '', {auth:state().auth.token})

                if(response.status == 403){
                    dispatch(
                        {
                            type:FRESHDESK_ERROR
                        }
                    )
                }

                // NETWORD FAILED
                if(response.status == 504){
                    dispatch({type:ERROR, payload:response.message})
                    clearTimeout(timer)
                    return
                }

                if(response.status == 200)
                    {
                        dispatch(
                            {
                                type:GET_GROUPS,
                                payload:response.groups
                            }
                        )
                    }

        } catch (error) {
            dispatch(errorReport())
        }

    }
}

export const openTicket = data => {
    return async (dispatch, state) => {

        const ticketData = state().auth.user ? {...data, email:state().auth.user.email, uniqueid:state().auth.user.id, name:state().auth.user.firstname} : {
            ...data, email:'', uniqueid:'', name:''
        }
        
        
        dispatch(
            {
                type:BUTTON_LOADING
            }
        )

        try {
            const response = await api.auth(route.OPEN_TICKET, ticketData, {method:'POST', auth:state().auth.token})
            
            if(response.status == 201){
                dispatch(
                    {
                        type:OPEN_TICKET,
                        payload:response.message
                    }
                )
            }
            // NETWORD FAILED
            if(response.status == 504){
                dispatch({type:ERROR, payload:response.message})
                clearTimeout(timer)
                return
            }

            if(response.status == 409){
                dispatch(
                    {
                        type:ERROR,
                        payload:response.message
                    }
                )
            }

        } catch (error) {
            
            dispatch(errorReport())
        }

        
    }
}

/**
 * RESOLVE TICKET
 * @param {*} id 
 */
export const resolveTicket = id => {
    return async (dispatch, state) => {
        dispatch(
            {
                type:BUTTON_LOADING
            }
        )
        try {
            
            const response = await api.auth(route.RESOLVE_TICKET, {id:id}, {method:'PUT', auth:state().auth.token})
            
            if(response.status == 200){
                dispatch(
                    {
                        type:TICKET_RESOLVED,
                        payload:response.message
                    }
                )
            }
            if(response.status == 500){
                dispatch(
                    {
                        type:ERROR,
                        payload:response.message
                    }
                )
            }
            // NETWORD FAILED
            if(response.status == 504){
                dispatch({type:ERROR, payload:response.message})
                clearTimeout(timer)
                return
            }
        } catch (error) {
            dispatch(errorReport())
        }
    }
}

export const closeTicket = id => {
    return async (dispatch, state) => {
        dispatch(
            {
                type:BUTTON_LOADING
            }
        )
        try {
            const response = await api.auth(route.CLOSE_TICKET, {id:id}, {method:'PUT', auth:state().auth.token})
            if(response.status == 200){
                dispatch(
                    {
                        type:TICKET_CLOSED,
                        payload:response.message
                    }
                )
            }
            // NETWORD FAILED
            if(response.status == 504){
                dispatch({type:ERROR, payload:response.message})
                clearTimeout(timer)
                return
            }
        } catch (error) {
            dispatch(errorReport())
        }
    }
}

export const getConversations = id => {
    return async (dispatch, state) => {
        dispatch(
            {
                type:FETCHING_CONVERSATIONS
            }
        )
        try {
            const response = await api.auth(route.TICKET_CONVERSATIONS + id, '', {auth:state().auth.token})

            
            
            if(response.status == 200){
                
                dispatch(
                    {
                        type:GET_CONVERSATIONS,
                        payload:response.conversations
                    }
                )

            }
            // NETWORD FAILED
            if(response.status == 504){
                dispatch({type:ERROR, payload:response.message})
                clearTimeout(timer)
                return
            }

            
            
        } catch (error) {
            dispatch(errorReport())
        }
        
    
    }
}

export const replyToTicket = data => {
    return async (dispatch, state) => {
        try {
            dispatch(
                {
                    type:IS_REPLYING_TICKET
                }
            )
            const response =  await api.auth(route.REPLY_TO_TICKET, data, {method:'POST', auth:state().auth.token})
            if(response.status == 200){
                dispatch(getConversations(state().group.ticket.id))
            }
            // NETWORD FAILED
            if(response.status == 504){
                dispatch({type:ERROR, payload:response.message})
                clearTimeout(timer)
                return
            }
        } catch (error) {
            dispatch(errorReport())
        }
    }
}


export const updateNotification = (action, obj) => {
    return async (dispatch, state) => {
        try {

            
            dispatch({type:IS_UPDATING})
            
            
            const response = await api.auth(route.UPDATE_NOTIFICATION, {status:action, id:obj.id, token:obj.token}, {method:'POST', auth:state().auth.token})

            if(response.status == 200) {
                
                dispatch({type:UPDATE_NOTIFICATION, reduce:action})
                dispatch(getNotification())
            }
            // NETWORD FAILED
            if(response.status == 504){
                dispatch({type:ERROR, payload:response.message})
                return
            }
        } catch (error) {
            dispatch(errorReport())
        }
    }
}


export const setActiveTicket = ticket => {
    return dispatch => {
        dispatch({type:IS_LOADING})
        dispatch ({type:ACTIVE_TICKET, payload:ticket})
    }
}

