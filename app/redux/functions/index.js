import { ERROR } from "../actions"

export const api = {
    auth:async (path, body=null, opts=null) => {
        try {
            if(opts.method == 'POST' || opts.method == 'PATCH' || opts.method == 'PUT'){
                const request = await fetch(path, {
                    method:opts.method,
                    headers:{
                        Authorization:opts.auth,
                        'Content-Type':'application/json'
                    },
                    body:JSON.stringify(body)
                })
    
                const response = await request.json()
    
                return response
            }
    
            const request = await fetch(path, {
                method:'GET',
                headers:{
                    Authorization:opts.auth,
                    'Content-Type':'application/json'
                }
            })
    
            const response = await request.json()
    
            return response
    
        } catch (error) {
            return {status:504, message:"Network error"}
        }
    },
    nonauth:async (path, body, opts)=>{
        try {
            if(opts.method == 'POST'){
                const request = await fetch(path, {
                    method:opts.method,
                    headers:{
                        'Content-Type':'application/json'
                    },
                    body:JSON.stringify(body)
                })

                const response = await request.json()

                return response
            }
        } catch (error) {
            return {status:504, message:"Network error"}
        }
    }
}


export const errorReport = () => {
    return {
        type:ERROR,
        payload:'Something went wrong, Please try again later.'
    }
}

export const requestTimeOut = (time, promise) => {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>reject({status:500, message:'Request time out'}), time)
        promise.then(resolve, reject)
    })
}