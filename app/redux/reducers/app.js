import { 
    IS_LOADING, 
    CONNECTED, RESPONSE, 
    ERROR, LOGIN_SUCCESS_MSG, LOGIN_SUCCESS, 
    IS_FETCHING_URL, GET_AUTO_AUTH_URL, SHOW_MENU_CONTEXT, 
    OPEN_TICKET, GET_CONVERSATIONS, STEP_TWO, STEP_THREE, 
    STEP_ONE, RESET_PASSWORD, RESET_MSG, CLOSE_SNACK, SUCCESS, 
    CLEAR_MESSAGE, GET_SINGLE_DOMAIN, GET_SERVICE_SETTING, GET_TICKETS, 
    EXPIRYING_PRODUCTS, IS_EDITING, GET_FEELING, NOT_LOGIN, LOGOUT, 
    IS_LOGGING_OUT, HEADER_HEIGHT, FETCHING_PROFILE, PROFILE_FETCHED, 
    NOTIFICATIONS, SUSPENDED_SERV_INVOICE, UNPAID_INVOICES, 
    GET_INVOICES, GET_ACTIVE_DOMAINS, GET_EXPIRED_DOMAINS, 
    GET_ACTIVE_SERVICES, GET_SUSPENDED_SERVICES, 
    FRESHDESK_ERROR, IS_SAVING, INVOICE_PAID, IS_REPLYING_TICKET, ACTIVE_TICKET, FETCHING_CONVERSATIONS, IS_UPDATING, UPDATE_NOTIFICATION, TICKET_RESOLVED, TICKET_CLOSED, BUTTON_LOADING, LOAD_MORE, INCREMENT_INDEX } from "../actions";

const INITIAL_STATE = {
    isLoading:false,
    isSaving:false,
    isLoggingOut:false,
    isFetching:false,
    buttonLoading:false,
    response:'',
    error:'',
    authURL:'',
    isFetchingUrl:false,
    showMenu:false,
    successMsg:'',
    resetSteps:{
        step1:'active',
        step2:'',
        step3:''
    },
    domain:null,
    service:null,
    expiringProducts:null,
    isEditing:false,
    isUpdating:false,
    feeling:null,
    height:60,
    invoiceIdSuspendedService:null,
    isReplyingTicket:false
}

export default function appReducer(state=INITIAL_STATE, action){
    switch (action.type) {
       case IS_LOADING:
        
           return {
               ...state,
               isLoading:true,
               error:'',
               successMsg:'',
           }

        case IS_LOGGING_OUT:
            return{
                ...state,
                isLoggingOut:true
            }
        case FETCHING_CONVERSATIONS:
            return {
                ...state,
                isFetching:true
            }

        case LOAD_MORE:
           
            return {
                ...state,
                buttonLoading:false
            }

        case INCREMENT_INDEX:
            return {
                ...state,
                // buttonLoading:false
            }

        case IS_UPDATING:
            return {
                ...state,
                isUpdating:true
            }

        case UPDATE_NOTIFICATION:
            return {
                ...state,
                isUpdating:false
            }

        case NOT_LOGIN:
            return {
                ...state,
                isLoading:false,
                error:'',
               successMsg:''
            }
        case LOGOUT:
            return {
                ...state,
                isLoading:false
            }
        case CONNECTED:
            return {
                ...state,
                response:'Connected to server...'
            }
        case RESPONSE:
            return {
                ...state,
                response:'Fetching...'
            }
        case ERROR:
            
            return {
                ...state,
                isLoading:false,
                error:action.payload,
                buttonLoading:false
            }
        case LOGIN_SUCCESS_MSG:
            return {
                ...state,
                response:'Successfully logged in'
            }
        case FETCHING_PROFILE:
            return {
                ...state,
                response:'Fetching profile'
            }
        case PROFILE_FETCHED:
            return {
                ...state,
                response:'',
                isLoading:false
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoading:false,
                response:'',
                
            }
        case IS_FETCHING_URL:
            return {
                ...state,
                isFetchingUrl:true
            }
        case GET_AUTO_AUTH_URL:
            
            return {
                ...state,
                authURL:action.url,
                isFetchingUrl:false
            }
        
        case OPEN_TICKET:
            
            return{
                ...state,
                successMsg:action.payload,
                isLoading:false,
                buttonLoading:false
            }
        case GET_CONVERSATIONS:
            
            return {
                ...state,
                isFetching:false,
                isReplyingTicket:false,
            }
        case RESET_MSG:
            return {
                ...state,
                successMsg:''
            }
        
        case SUCCESS:
            return {
                ...state,
                successMsg:action.payload,
                isLoading:false,
                isEditing:false,
                isSaving:false,
                isReplyingTicket:false,
                feeling:null
            }

        case CLEAR_MESSAGE:
            
            return {
                ...state,
                successMsg:'',
                error:''
            }
        case GET_SINGLE_DOMAIN:
            return {
                ...state,
                domain:action.payload
            }

        case GET_SERVICE_SETTING:
            return {
                ...state,
                service:action.payload
            }

        case GET_TICKETS:
            return {
                ...state,
                isLoading:false
            }

        case EXPIRYING_PRODUCTS:
            return {
                ...state,
                expiringProducts:action.payload
            }
        case IS_EDITING:
            return {
                ...state,
                isEditing:true
            }

        case GET_FEELING:
            return{
                ...state,
                feeling:action.payload
            }

        case HEADER_HEIGHT:
            return {
                ...state,
                height:action.payload
            }
        case SUSPENDED_SERV_INVOICE:
        
            return {
                ...state,
                invoiceIdSuspendedService:action.payload,
                isLoading:false
            }

        case INVOICE_PAID:
            return {
                ...state,
                invoiceIdSuspendedService:'already paid',
                isLoading:false
            }
    
        case UNPAID_INVOICES:
            return {
                ...state,
                isLoading:false
            }

        case GET_INVOICES:
            return {
                ...state,
                isLoading:false
            }
        case GET_ACTIVE_DOMAINS:
            return {
                ...state,
                isLoading:false
            }
        case GET_EXPIRED_DOMAINS:
            return {
                ...state,
                isLoading:false
            }
        case GET_ACTIVE_SERVICES:
            return {
                ...state,
                isLoading:false
            }
        case GET_SUSPENDED_SERVICES:
            return {
                ...state,
                isLoading:false
            }

        case FRESHDESK_ERROR:
            return {
                ...state,
                error:'Support channel is not available'
            }
        case IS_SAVING:
            return{
                ...state,
                isSaving:true
            }

        case IS_REPLYING_TICKET:
            return {
                ...state,
                isReplyingTicket:true
            }

        case BUTTON_LOADING:
            return {
                ...state,
                buttonLoading:true
            }

        case TICKET_RESOLVED:
            return {
                ...state,
                successMsg:action.payload,
                buttonLoading:false
            }

        case TICKET_CLOSED:
            return {
                ...state,
                successMsg:action.payload,
                buttonLoading:false
            }

        case ACTIVE_TICKET:
            return {
                ...state,
                isLoading:false
            }
        default:
            return state;
    }
}