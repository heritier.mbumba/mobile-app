import { NOT_LOGIN, LOGIN_SUCCESS, LOGOUT, USE_DIFFERENT_ACCOUNT, SVGP, UPDATE_PROFILE, EDIT_PROFILE, FETCHING_PROFILE, PROFILE_FETCHED, UPDATE_NOTIFICATION } from "../actions";

const INITIAL_STATE = {
    isAuth:false,
    user:null,
    token:null,
    expiresIn:null,
    svgp:null
}

export default function authReducer(state=INITIAL_STATE, action){
    switch (action.type) {
        
       case NOT_LOGIN:
           return {
               ...state,
               isAuth:false,
               expiresIn:null,
               token:null
           }
        case SVGP:
            return {
                ...state,
                svgp:action.payload
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                token:action.token,
                expiresIn:action.expiresIn,
                isAuth:true
            }
        case PROFILE_FETCHED:
            return {
                ...state,
                user:action.payload.profile

            }
        case LOGOUT:
            return {
                ...state,
                expiresIn:null,
                token:null,
                isAuth:false,
                svgp:null
            }
        case USE_DIFFERENT_ACCOUNT:
            
            return {
                ...state,
                isAuth:false,
                token:null,
                user:null,
                expiresIn:null,
                svgp:null
            }
        case UPDATE_PROFILE:
            return {
                ...state,
                user:{
                    ...state.user, 
                    firstname:action.newClient.firstname,
                    lastname:action.newClient.lastname,
                    email:action.newClient.email,
                    phonenumber:action.newClient.phonenumber,
                    country:action.newClient.country,
                    state:action.newClient.state,
                    postcode:action.newClient.postcode,
                    city:action.newClient.city,
                    companyname:action.newClient.companyname
                }
            }

        

        case EDIT_PROFILE:
            return {
                ...state,
                user:action.payload
                
            }
        default:
            return state;
    }
}