import { UNPAID_INVOICES, GET_AUTO_AUTH_URL, GET_INVOICES, LOGOUT, PROFILE_FETCHED } from "../actions";

const INITIAL_STATE = {
    invoices:[],
    unpaidInvoices:[],
    totalUnpaid:0
}

export default function billingReducer(state=INITIAL_STATE, action){
    switch (action.type) {
       case UNPAID_INVOICES:
          
           return {
               ...state,
               unpaidInvoices:action.payload
           }
        case GET_INVOICES:
            return {
                ...state,
                invoices:action.payload
            }

        case PROFILE_FETCHED:
            return {
                ...state,
                totalUnpaid:action.payload.totalUnpaid.toFixed(2)
            }

        case LOGOUT:
            return {
                ...state,
                invoices:[],
                unpaidInvoices:[],
                totalUnpaid:0
            }

        case PROFILE_FETCHED:
            const totalunpaid = action.payload.unpaidinvoices.reduce((prev, current)=>prev + parseInt(current.total), 0)
            return {
                ...state,
                unpaidInvoices:action.payload.unpaidinvoices,
                totalUnpaid:totalunpaid
            }
        
        default:
            return state;
    }
}