import { GET_ACTIVE_DOMAINS, LOGOUT, AUTO_RENEW_DOMAIN, PROFILE_FETCHED, GET_EXPIRED_DOMAINS, LOAD_MORE, INCREMENT_INDEX, DECREMENT_INDEX } from "../actions";
import _ from 'lodash'

const INITIAL_STATE = {
    domains:[],
    transferringDomains:[],
    expiredDomains:[],
    activeDomains:[],
    totalActiveDomains:0,
    totalExpiredDomains:0,
    activeLoaded:[],
    index:0
}

export default function domainReducer(state=INITIAL_STATE, action){
    switch (action.type) {
        case GET_ACTIVE_DOMAINS:
           
            return {
                ...state,
                activeDomains:action.payload,
                activeLoaded:action.payload.length > 20 ? action.payload.slice(state.index, 20) : action.payload,
                index:0
            }

        case GET_EXPIRED_DOMAINS:
            return {
                ...state,
                expiredDomains:action.payload
            }

        case PROFILE_FETCHED:
            return {
                ...state,
                totalActiveDomains:action.payload.activeDomains,
                totalExpiredDomains:action.payload.expiredDomains,
                index:0
            }
        case LOGOUT:
            return {
                ...state,
                domains:[],
                transferringDomains:[],
                expiredDomains:[],
                activeDomains:[],
                activeLoaded:[],
                totalActive:0,
                totalExpired:0,
                index:0
            }

        case AUTO_RENEW_DOMAIN:
            const updatedDomains = [...state.activeDomains]
            const domain = state.activeDomains.findIndex(domain=>domain.name==action.payload.domain)
            updatedDomains[domain].donotrenew = action.status
            
            return {

                ...state,
                activeDomains:updatedDomains
            }

        

        case INCREMENT_INDEX:
            const left = state.activeDomains.length - state.index

            const canIncrement = left > 10
            return {
                ...state,
                index:canIncrement ? action.payload : state.index
            }

        case DECREMENT_INDEX:
            
            const canDecrement = state.index > 0;
            return {
                ...state,
                index:canDecrement ? action.payload : state.index
            }

        case LOAD_MORE:
            const copyDomains = [...state.activeDomains]
            const end = state.index + 20
            const isLast = copyDomains.length - end <=20
            const nextLoad = state.activeDomains.slice(state.index + 1, isLast ? copyDomains.length : end)
            return {
                ...state,
                activeLoaded:nextLoad
            }
        default:
            return state;
    }
}