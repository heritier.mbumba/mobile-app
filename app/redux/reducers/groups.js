import { GET_GROUPS, LOGOUT, ACTIVE_TICKET } from "../actions"

const INITIAL_STATE = {
    groups:[],
    ticket:null
}

export default function supportGroupsReducer(state=INITIAL_STATE, action){
    switch (action.type) {
        case GET_GROUPS:
            return {
                ...state,
                groups:action.payload
            }

        case ACTIVE_TICKET:
            return {
                ...state,
                ticket:action.payload
            }
        // case LOGOUT:
        //     return {
        //         ...state,
        //         groups:[]
        //     }
        default:
            return state
    }
}