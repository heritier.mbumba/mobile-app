import {combineReducers} from 'redux'

import authReducer from './auth'
import appReducer from './app'
import billingReducer from './billing'
import serviceReducer from './service'
import domainReducer from './domain'
import supportReducer from './support'
import supportGroupsReducer from './groups'
import notificationReducer from './notification'


const rootReducer = combineReducers(
    {
        auth:authReducer,
        app:appReducer,
        service:serviceReducer,
        billing:billingReducer,
        domain:domainReducer,
        support:supportReducer,
        group:supportGroupsReducer,
        notification:notificationReducer
    }
)

export default rootReducer