import { GET_NOTIFICATIONS, UNPAID_INVOICES, NOTIFICATIONS, DELETE_NOTIFICATION, LOGOUT, PROFILE_FETCHED, UPDATE_NOTIFICATION } from "../actions";

const INITIAL_STATE = {
    notifications:[],
    totalNofications:0
}

export default function notificationReducer(state=INITIAL_STATE, action)
{
    switch (action.type) {
        case GET_NOTIFICATIONS:
            return {
                ...state,
                notifications:action.payload
                
            }

        case DELETE_NOTIFICATION:
            let notifications = [...state.notifications]
            notifications = notifications.filter((n, i)=>i != action.payload)
            
            return {
                ...state,
                notifications:notifications
            }

        case PROFILE_FETCHED:
            return {
                ...state,
                totalNofications:action.payload.totalNotifications
            }

        case UPDATE_NOTIFICATION:
       
        return {
            ...state,
            totalNofications:state.totalNofications == 0 ? 0 : (action.reduce == 'delete' || action.reduce == 'read' ? state.totalNofications - 1 : state.totalNofications)
        }
        case LOGOUT:
            return {
                ...state,
                notifications:[]
            }
        default:
            return state;
    }
}