import { LOGOUT, PROFILE_FETCHED, SUSPENDED_SERV_INVOICE, GET_ACTIVE_SERVICES, GET_SUSPENDED_SERVICES } from "../actions";

const INITIAL_STATE = {
    services:[],
    suspendedServices:[],
    addons:[],
    nextDueService:[],
    activeServices:[],
    freeServices:[],
    totalActiveService:0,
    totalSuspendedService:0
}

export default function appReducer(state=INITIAL_STATE, action){
    switch (action.type) {
        case GET_ACTIVE_SERVICES:
            
            return {
                ...state,
                activeServices:action.payload,
                addons:action.addons
            }

        case GET_SUSPENDED_SERVICES:
            return {
                ...state,
                suspendedServices:action.payload
            }

        case PROFILE_FETCHED:
            return {
                ...state,
                totalActiveService:action.payload.activeServices,
                totalSuspendedService:action.payload.suspendedServices
            }
        case LOGOUT:
            return {
                ...state,
                services:[],
                suspendedServices:[],
                nextDueService:[],
                activeServices:[],
                totalActive:0,
                totalSuspended:0
            }
        default:
            return state;
    }
}