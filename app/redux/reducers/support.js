import { GET_GROUPS, GET_TICKETS, GET_CONVERSATIONS, LOGOUT, GET_SALES_TICKETS } from "../actions"

const INITIAL_STATE = {
    tickets:[],
    conversations:[],
    sales:null
}

export default function supportReducer(state=INITIAL_STATE, action){
    switch (action.type) {
        case GET_TICKETS:
            return {
                ...state,
                tickets:action.payload
            }
        case GET_CONVERSATIONS:
            
            return {
                ...state,
                conversations:action.payload
            }

        case LOGOUT:
            return {
                ...state,
                conversations:null
            }
        case GET_SALES_TICKETS:
            return {
                ...state,
                sales:action.payload
            }
        default:
            return state
    }
}