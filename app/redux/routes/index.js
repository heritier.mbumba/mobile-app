import { ENDPOINT } from "../actions";

export const route = {

    /**
     * AUTH
     */
    LOGIN:ENDPOINT + '/login',

    /**
     * PROFILE 
     */
    UPDATE_PROFILE:ENDPOINT + '/auth/profile/update',
    PROFILE:ENDPOINT + '/auth/profile',

    /**
     * SUPPORT TICKETS
     */
    GET_TICKETS:ENDPOINT + '/tickets/open-and-pending',
    TICKETS_GROUPS:ENDPOINT + '/tickets/groups',
    OPEN_TICKET:ENDPOINT + '/tickets/open',
    TICKET_CONVERSATIONS: ENDPOINT + '/tickets/conversations/',
    REPLY_TO_TICKET:ENDPOINT + '/tickets/reply',
    RESOLVE_TICKET:ENDPOINT + '/tickets/resolve',
    CLOSE_TICKET:ENDPOINT + '/tickets/close',

    /**
     * DOMAINS
     */
    AUTO_RENEW_DOMAIN:ENDPOINT + '/auth/domain-auto-renew',
    GET_ACTIVE_DOMAINS:ENDPOINT + '/auth/active-domains',
    GET_EPIRED_DOMAINS:ENDPOINT + '/auth/expired-domains',
    
    /**
     * SERVICES
     */
    GET_ACTIVE_SERVICES:ENDPOINT + '/auth/active-services',
    GET_SUSPENDED_SERVICES:ENDPOINT + '/auth/suspended-services',
    GET_INVOICEID_SUSP:ENDPOINT + '/auth/suspended-service-invoice/',


    /**
     * BILLING
     */
    GET_UNPAID_INVOICES:ENDPOINT + '/auth/unpaid-invoices',
    GET_PAID_INVOICES:ENDPOINT + '/auth/paid-invoices',

    /**
     * SYSTEM
     */
    FEEDBACK:ENDPOINT + '/auth/feedback',
    ADD_EXPO_TOKEN:ENDPOINT + '/add-expo-token',
    ERROR_REPORTING:ENDPOINT + '/error-reporting',
    GET_NOTIFICATIONS:ENDPOINT + '/auth/notifications',
    UPDATE_NOTIFICATION:ENDPOINT + '/auth/update-notification',
    LOG_ACTIVITY:ENDPOINT + '/auth/activity'


}