import { AsyncStorage } from 'react-native'
import { createStore, applyMiddleware } from 'redux'
import {persistReducer, persistStore, persistCombineReducers,} from 'redux-persist'
import {composeWithDevTools} from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import rootReducer from '../reducers/index'


const config = {
    key:'storage',
    storage:AsyncStorage,
    blacklist:['app'],
    whitelist:['auth', 'billing', 'service', 'domain', 'group', 'notification'],
    
}

const persistConfigReducer = persistReducer(config, rootReducer)


const storage = createStore(persistConfigReducer, composeWithDevTools(applyMiddleware(thunk)))
const persistor = persistStore(storage)

export{
    storage, persistor
}