import React from 'react'
import Colors from '../../constants/Colors'
import {DefaultTheme, configureFonts} from 'react-native-paper'

const fontConfig = {
    default: {
      regular: {
        fontFamily: 'pfdin-bold',
        fontWeight: 'normal',
      },
      medium: {
        fontFamily: 'pfdin-medium',
        fontWeight: 'normal',
      },
      light: {
        fontFamily: 'open-sans',
        fontWeight: 'normal',
      },
      thin: {
        fontFamily: 'helvetica',
        fontWeight: 'normal',
      },
    },
  };

const Theme = {
    ...DefaultTheme,
    roundness:2,
    colors:{
        ...DefaultTheme.colors,
        primary:'#20bfdd',
        accent:'#03a67b',
        background:'#fff',
        text:'#56565b'
    },
    // fonts:configureFonts(fontConfig)
}
export default Theme