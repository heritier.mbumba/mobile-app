import moment from "moment";

export const date = d => moment(d).format('DD-MM-YYYY')
export const dateFromNow = d =>moment(d).fromNow()
export const getPaymentMethod = m => {
    let name
    switch (m) {

        case 'mygateDebit':
            name = 'My gate debit'
        break;
        case 'payfasteft':
            name = 'Payfast EFT'

    }

    return name
}
export const getPriority = value => {
    switch (value) {
        case 1:
            return 'low'
        case 2:
            return 'medium'
        case 3:
            return 'high'
        case 4:
            return 'urgent'
    
    }
}

export const waitToRefresh = time => {
    return new Promise(resole=>{
        setTimeout(resole, time)
    })
}

export const navigation = {
    Dashboard:'Dashboard',
    Profile:'Profile',
    Account:'Account',
    Notification:'Notification',
    Feedback:'Feedback',
    ViewInvoice:'ViewInvoice',
    ReadNotification:'ReadNotification',
    Billing:'Billing',
    InvoiceDetail:'InvoiceDetail',
    Services:'Services',
    ServiceDetail:'ServiceDetail',
    ServiceAddonDetail:'ServiceAddonDetail',
    SuspendedService:'Suspended',
    SuspendedServiceDetail:'SuspendedDetail',
    Domains:'Domains',
    ExpiredDomain:'Expired',
    Support:'Support',
    OpenedTicket:'OpenedTicket',
    PendingTicket:'PendingTicket',
    OpenSupportTicket:'OpenSupportTicket',
    TicketDetail:'TicketDetail',
    More:'More',
}

export const lang = {
    /**
     * GENERAL
     */
    Currency:'R',
    PayButton:'Pay now',
    UnpaidAmount:'Unpaid amount',
    SubmitButton:'Submit',
    ReadMore:'Read more',
    Delete:'Delete',
    Remove:'Remove',
    ExpiredDate:'Expiry date',
    NoNotification:'No notifications',
    ClickHere:'Click here',
    Login:'Login',
    Password:'Password',
    Sorry:'Sorry',
    SorryMessage:'This function is not yet available and will be added in future update. You can use the web browser instead',

    /**
     * DASHBOARD
     */
    CompanyName : 'Company name',
    CustomerCode: 'Customer Code: #',
    ActiveService:'Active service',
    ActiveServices:'Active services',
    ViewYourActiveServices:'View active services',
    ViewYourActiveService:'View active service',
    ViewYourBillingStatus:'View billing status',
    ViewSuspendedServices:'View suspended services',
    ViewYourActiveDomains:'View active domains',
    ViewYourExpiredDomains:'View expired domains',
    ViewAndOpenTicket:'View, open and reply to tickets',
    ViewAccountSetting:'View account settings',
    ViewAccountDetails:'View account details',
    SuspendedService:'Suspended service',
    SuspendedServices:'Suspended services',
    ActiveDomains:'Active domains',
    ExpiredDomains:'Expired domains',
    Billing:'Billing',
    SupportAndQueries:'Support and Queries',
    AccountSettings:'Account Settings',
    AccountDetail:'Account detail',
    EnterYourFeedback:'Enter your feedback',
    Feedback:'Feedback',
    SendFeedBack:'Send us your feedback',
    FeedbackTellUs:'Tell us a bit more about your experience',
    FeedbackWhatDidYouLike:'What did you like, what didn\'t you like, where could we improve?',
    FeedbackWhatDoYouThink:'What do you think about the new 1-grid app?',
    Notification:'Notification',
    ViewNotification:'View billing, service and network notification',

    /**
     * PROFILE
     */
    PersonalInfo:'Personal Info',
    FirstName:'First name',
    LastName:'Last name',
    Email:'Email',
    PhoneNumber:'Phone number',
    CompanyName:'Company name',
    AddressInfo:'Address info',
    Address1:'Address 1',
    Address2:'Address 2',
    City:'City',
    PostalCode:'Postal code',
    State:'State',
    Country:'Country',
    /**
     * LOGIN SCREEN
     */
    UseDifferentAccount:'Use different account',
    ForgetPassword:'Forgot password?',

    /**
     * TICKET
     */
    NoOpenedTicket:'No opened ticket',
    NoPendingTicket:'No pending ticket',
    ResolvedTicketMessage:'Viewing resolved tickets is not yet available in the app. Would like to view via web browser?'
}