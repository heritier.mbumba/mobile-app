import * as React from 'react'
import {TouchableOpacity, Text, View, StyleSheet} from 'react-native'
import Colors from '../../constants/Colors'

export function CustomButtom(props){
    
    return <View style={styles.container}>
            <TouchableOpacity style={props.color ? {...styles.viewButton, borderColor:props.color} : styles.viewButton} {...props}>
                <Text style={{color:props.color ? props.color : Colors.accent}}>{props.title}</Text>
            </TouchableOpacity>
        </View>
}

export const ViewButton = React.memo(CustomButtom)

const styles = StyleSheet.create(
    {
        container:{
            flexDirection:'row', 
            justifyContent:'space-between', 
            alignItems:'center'
        },
        viewButton:{
            justifyContent:'center', 
            alignItems:'center', 
            borderRadius:3, 
            borderWidth:1, 
            borderColor:Colors.accent, 
            width:70, 
            paddingVertical:0, 
            height:30, 
            marginTop:10, }
    }
)