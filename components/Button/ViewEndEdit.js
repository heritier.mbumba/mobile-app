import React, { Fragment, useState, memo } from 'react'
import { List, Divider } from 'react-native-paper'
import {ViewButton} from './ViewButton'
import { TextInput, StyleSheet, View } from 'react-native'
import Colors from '../../constants/Colors'

export function ViewEndEditButtom(props){
    
    return <Fragment>
                <View style={styles.editInput}>
                    <TextInput placeholder="name" style={styles.input} value={props.title} onChangeText={()=>props.value()}/>
                </View>
                <Divider />
            </Fragment>
}

export const ViewEndEdit = memo(ViewEndEditButtom)

const styles = StyleSheet.create({
    container:{

    },
    input:{
        paddingVertical:22
    },
    editInput:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingRight:8
    }
})