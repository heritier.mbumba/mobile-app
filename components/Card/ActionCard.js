import React, { memo } from 'react'
import { View, StyleSheet, TouchableOpacity,TouchableHighlight, Platform } from 'react-native'
import { Card, Subheading, Button, List } from 'react-native-paper'
import Colors from '../../constants/Colors'
import { normalize } from 'react-native-elements'

export  function customActionCard(props){
    
    return <TouchableOpacity style={{...styles.header, borderLeftWidth:props.padding ? 10 : null, borderLeftColor:props.color}} {...props} underlayColor={Colors.grayText} activeOpacity={.6}>
                <Card style={{paddingVertical:props.padding ? 15 : 5}}>
                    <List.Item 
                        title={props.title} titleStyle={{textTransform:'uppercase', fontSize:normalize(14), fontWeight:'bold', color:props.color ? props.color : Colors.secondary}}
                        description={props.description}
                        left={()=>props.icon ? 
                                <List.Icon icon={props.icon} color={Colors.gray}/> : 
                                <View style={{justifyContent:'center', paddingHorizontal:10, width:70, borderRightWidth:1, borderRightColor:Colors.grayText}}>
                                    <Subheading style={{fontSize:normalize(20), color:props.color, lineHeight:40, fontWeight:'bold', textAlign:'center'}}>{props.content}</Subheading>
                                </View>}
                        right={()=><List.Icon icon={"chevron-right"} color={props.color}/>}/>
                </Card>
            </TouchableOpacity>
    }

export const ActionCard = memo(customActionCard)

const styles = StyleSheet.create({
    header:{
        width:'100%',
        marginTop:8,
        
    },
    activeServiceText:{
        color:Colors.accent
    },
    title:{fontSize:normalize(15), textTransform:'uppercase'},
    
})