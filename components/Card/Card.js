import React from 'react'
import {Card, Title, Subheading, Button} from 'react-native-paper'
import { View, StyleSheet, TouchableOpacity, TouchableHighlight, Text } from 'react-native'
import Colors from '../../constants/Colors'
import {ViewButton} from '../Button/ViewButton'
import { normalize } from 'react-native-elements'


export default function CardBox(props){
    
    
    return  <TouchableOpacity style={styles.header} {...props} underlayColor={Colors.grayText} activeOpacity={.6}>
                <View style={styles.cardBox}>
                    <View>
                        <Title>
                            <Text style={{color:Colors.danger, paddingVertical:0, fontSize:normalize(20), fontWeight:'bold'}}>{props.title}</Text>
                        </Title>
                        <Subheading style={{paddingVertical:0, marginTop:-4}}>{props.description}</Subheading>
                    </View>
                    <View style={styles.action}>
                        {props.single ? <Text style={{paddingRight:20, textTransform:'uppercase', color:Colors.danger}}>Pay now</Text> : <Button color={Colors.danger}>{props.buttonName}</Button>}
                        
                    </View>
                </View>
            </TouchableOpacity>
}

const styles = StyleSheet.create({
    header:{
        width:'100%',
        marginTop:5,
        justifyContent:'center',
        alignItems:'center',
        paddingTop:2


    },
    cardBox:{
        width:'100%',
        paddingHorizontal:12,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        backgroundColor:'#fff',
        paddingVertical:30,
        shadowColor: "#f1f1f1",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        borderRadius:2,
        elevation: 2,
    },
    action:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    }
    
})