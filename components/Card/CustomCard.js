import React from 'react'
import { View, StyleSheet, TouchableOpacity, TouchableHighlight } from 'react-native'
import { Card, Subheading, Button, List } from 'react-native-paper'
import Colors from '../../constants/Colors'
import { normalize } from 'react-native-elements'

export default function CustomCard(props){
    
    return <TouchableOpacity style={styles.header} {...props} activeOpacity={.6}>
                <Card>
                    <Card.Title title={props.title} subtitle={props.subtitle} titleStyle={styles.title}/>
                    <Card.Content style={styles.cardContent}>
                        <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                            <View style={{}}>{props.children}</View>
                        </View>
                        
                    </Card.Content> 
                </Card>
            </TouchableOpacity>}

const styles = StyleSheet.create({
    header:{
        width:'100%',
        marginTop:8,
        
    },
    activeServiceText:{
        color:Colors.accent
    },
    title:{fontSize:normalize(15), textTransform:'uppercase'}
})