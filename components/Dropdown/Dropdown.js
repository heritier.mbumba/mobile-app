import React, { Component, useState } from 'react'

import RNPickerSelect from 'react-native-picker-select';
import {StyleSheet, View} from 'react-native'
import Colors from '../../constants/Colors';
import { Ionicons } from '@expo/vector-icons';
import { useSelector } from 'react-redux';
import { normalize } from 'react-native-elements';

export default function DropdownInput(props){
    
    
    return <View>
                <RNPickerSelect
                        placeholder={
                            {
                                label:props.label,
                                color:Colors.grayText,
                                value:null
                            }
                        }
                        items={props.data}
                        Icon={()=><Ionicons name="ios-arrow-down" size={15} color={Colors.gray}/>}
                        style={
                            {...pickerSelectStyles, 
                                iconContainer: {
                                    top: 30,
                                    right: 10,
                                },
                            }
                        }
                    {...props}/>
            </View>
}

const styles = StyleSheet.create({
    container:{
        borderColor:Colors.grayText, 
        borderWidth:1, 
        marginTop:10, 
        borderRadius:2, 
        justifyContent:'center', 
        alignItems:'center'}
})

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize: normalize(14),
      paddingVertical: 10,
      borderRadius: 2,
      paddingLeft:10,
      color: Colors.secondary,
      paddingRight: 30, // to ensure the text is never behind the icon
      marginVertical:15,

    },
    inputAndroid: {
      fontSize: normalize(14),
      paddingVertical: 15,
      borderRadius: 0,
      color: Colors.secondary,
      paddingRight: 30, // to ensure the text is never behind the icon
      marginVertical:10,
        
    },
  });