import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Colors from '../../constants/Colors'
import { Subheading } from 'react-native-paper'
import { normalize } from 'react-native-elements'
import * as utils from '../../app/utils'
import { useDispatch, useSelector } from 'react-redux'
import { clearErrorDispatcher } from '../../app/redux/dispatchers/app'


export default function Error({title, persist}){

    const dispatch = useDispatch()
    
    useEffect(()=>{
        if(persist && !persist){
            dispatch(clearErrorDispatcher())
        }  
    }, [])

    
    
    
    return title ?    <View style={styles.errorContainer}>
                                <Subheading style={{ color:Colors.dangerLight, paddingHorizontal:10, fontSize:normalize(12), textAlign:'center', lineHeight:20}}>{title}</Subheading>
                            </View> : null
}

const styles = StyleSheet.create({
    errorContainer:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#f8d7da',
        marginBottom:10,
        borderWidth:1,
        borderColor:'#f5c6cb',
        paddingTop:5,
        paddingBottom:5,
    }
})