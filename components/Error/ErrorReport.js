import React from 'react'
import { View } from "react-native";
import { Subheading } from "react-native-paper";

export default function ErrorReporting(props){
    return <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                <Subheading>Something went wrong</Subheading>
            </View>
}