import * as React from 'react'
import { View } from 'react-native'
import { Subheading } from 'react-native-paper'
import { normalize } from 'react-native-elements'
import Colors from '../../constants/Colors'
import AppLoading from '../Loaders/AppLoader'

const wait = timer => {
    return new Promise(resolve=>{
        setTimeout(resolve, timer)
    })
}
export default function Offline(props){

    const [canRender, setCanRender] = React.useState(false)

    React.useEffect(()=>{
        let isCancelled = false

        if(!isCancelled) wait(2000).then(()=>setCanRender(true))
        
        return () => isCancelled = true
    }, [canRender])

    return canRender ? <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                            <Subheading style={{fontSize:normalize(25), lineHeight:35, color:Colors.secondary}}>You are offline</Subheading>
                        </View> : <AppLoading title="Loading"/>
}