import * as React from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import {MaterialCommunityIcons} from '@expo/vector-icons'
import { Subheading } from 'react-native-paper'
import { normalize } from 'react-native-elements'
import { useDispatch } from 'react-redux'
import { getFeeling } from '../../app/redux/dispatchers/app'
import Colors from '../../constants/Colors'

export default function FeelingEmoji(props){
    const [feeling, setFeeling] = React.useState('')

    const dispatch = useDispatch()

    React.useEffect(()=>{
        dispatch(getFeeling(feeling))
    }, [feeling, setFeeling])

    return  <View style={{paddingHorizontal:20, flexDirection:'row', justifyContent:'space-between', paddingVertical:10}}>
                
                <TouchableOpacity style={{justifyContent:'center', alignItems:'center'}} onPress={()=>setFeeling('Bad')}>
                    <Image source={require('../../assets/images/icons/002-sad.png')} style={{width:20, height:20, opacity:feeling == 'Bad' ? 1 : .3}}/>
                    <Subheading style={{textAlign:'center', fontSize:normalize(12)}}>Bad</Subheading>
                </TouchableOpacity>
                <TouchableOpacity style={{justifyContent:'center', alignItems:'center'}}  onPress={()=>setFeeling('Average')}>
                    <Image source={require('../../assets/images/icons/001-meh.png')} style={{width:20, height:20, opacity:feeling == 'Average' ? 1 : .3}}/>
                    <Subheading style={{textAlign:'center', fontSize:normalize(12)}}>Average</Subheading>
                </TouchableOpacity>
                <TouchableOpacity style={{justifyContent:'center', alignItems:'center'}} onPress={()=>setFeeling('Excellent')}>
                    <Image source={require('../../assets/images/icons/003-smile.png')} style={{width:20, height:20, opacity:feeling == 'Excellent' ? 1 : .3}}/>
                    <Subheading style={{textAlign:'center', fontSize:normalize(12)}}>Excellent</Subheading>
                </TouchableOpacity>
            </View>
}