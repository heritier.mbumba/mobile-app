import * as React from 'react'
import { Header } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native';
import Colors from '../../constants/Colors';
import { RightElement, LeftElement } from './HeaderElement';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import { SmallLogo } from '../Logo/Logo';


export function myHeader(props){
    const navigation = useNavigation()
    
    
    return <Header
                leftComponent={<LeftElement />}
                centerComponent={<SmallLogo />}
                rightComponent={<RightElement />}
                backgroundColor={Colors.tintColor} containerStyle={{}}/>
}

export const CustomHeader  = React.memo(myHeader)