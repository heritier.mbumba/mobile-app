import React, { useEffect, Fragment } from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {Appbar} from 'react-native-paper'
import Colors from '../../constants/Colors'
import { logout } from '../../app/redux/dispatchers/auth'
import { View, Text } from 'react-native'



export default function Header(props){

    const dispatch = useDispatch()

    const isAuth = useSelector(state=>state.auth.isAuth)

    
    useEffect(()=>{

    }, [isAuth])

    const _logoutHandler = () => {
        dispatch(logout())
        props.navigate()
    }

    return <Appbar.Header>
                {props.showBackButton == 'yes' ? <Appbar.BackAction onPress={()=>props.goBack()} color={Colors.light}/> : null}
                
                <Appbar.Content title={props.title} subtitle={props.subtitle} color={Colors.light}/>
                {props.hideButton ? null : <Fragment>
                    <View style={{backgroundColor:Colors.danger, borderRadius:50, width:20, height:20, alignItems:'center', justifyContent:'center', marginRight:-18, zIndex:9, marginTop:-15}}><Text style={{color:Colors.light}}>2</Text></View>
                    <Appbar.Action icon="bell" color={Colors.light} onPress={()=>props.navigateTo()}/>
                    <Appbar.Action icon="logout" color={Colors.light} onPress={_logoutHandler}/>
                    </Fragment>}
                {/* <Appbar.Action icon="dots-vertical" color={Colors.light} onPress={_toggleMenu}/> */}
            </Appbar.Header>
}