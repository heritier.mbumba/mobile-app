import React, { useContext } from 'react'
import {MaterialCommunityIcons } from '@expo/vector-icons'
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native'
import Colors from '../../constants/Colors'
import { useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native';
import { AuthContext } from '../../context/Auth'
import * as utils from '../../app/utils'

export function RightElement(){
    const {logout} = useContext(AuthContext)
    const navigation = useNavigation()

    const _logoutHandler = () => logout()

    /**
     * REDUX STATE
     */
    const {totalNofications} = useSelector(state=>state.notification)
    
    return  <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                <TouchableOpacity style={{paddingHorizontal:10, marginEnd:5, paddingVertical:10}} onPress={()=>navigation.navigate(utils.navigation.Notification)} activeOpacity={.6}>
                    <View style={{position:'absolute', backgroundColor:Colors.secondary, width:20, left:0, height:20, borderRadius:50, zIndex:9, top:6}}><Text style={{color:Colors.light, textAlign:'center',paddingTop:1}}>{totalNofications > 9 ? totalNofications + '+' : totalNofications}</Text></View>
                    <MaterialCommunityIcons name="bell" color={Colors.light} size={25}/>
                </TouchableOpacity>
                <TouchableOpacity style={{paddingHorizontal:10, paddingVertical:10}} onPress={_logoutHandler} activeOpacity={.6}>
                    <MaterialCommunityIcons name="logout" color={Colors.light} size={23}/>
                </TouchableOpacity>

            </View>
}

export function LeftElement(){
    const navigation = useNavigation()
    return <TouchableOpacity style={{paddingHorizontal:15,paddingVertical:10}} onPress={()=>navigation.navigate(utils.navigation.Dashboard)}>
                <MaterialCommunityIcons name="home" color={Colors.light} size={25}/>
            </TouchableOpacity>
}
