import * as React from 'react'
import {TouchableOpacity, Text} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { updateProfileInfo } from '../../app/redux/dispatchers/auth'
import Colors from '../../constants/Colors'
import {useNavigation} from '@react-navigation/native'
import * as utils from '../../app/utils'

export function RightButton(props){
    /**
     * REDUX STATE
     * 
     */
    const {isEditing} = useSelector(state=>state.app)
    const dispatch = useDispatch()
    return isEditing ? <TouchableOpacity onPress={()=>dispatch(updateProfileInfo())} style={{paddingHorizontal:15, marginEnd:10, paddingVertical:10}}>
                            <Text style={{color:Colors.light, textTransform:"uppercase"}}>{props.title}</Text>
                        </TouchableOpacity> : null
}

export function RightPayInvoiceButton(props){
    const navigation = useNavigation()
    const id = props.id.substr(1, props.id.length)
    
    return <TouchableOpacity onPress={()=>navigation.navigate(utils.navigation.ViewInvoice, {id:id})} style={{paddingHorizontal:15, marginEnd:10, paddingVertical:10}}>
                <Text style={{color:Colors.light, textTransform:"uppercase"}}>{props.title}</Text>
            </TouchableOpacity>
}


export function RightSubmitTicketButton(props){
    return <TouchableOpacity onPress={()=>{}} style={{paddingHorizontal:15, marginEnd:10, paddingVertical:10}}>
                <Text style={{color:Colors.light, textTransform:"uppercase"}}>{props.title}</Text>
            </TouchableOpacity>
}