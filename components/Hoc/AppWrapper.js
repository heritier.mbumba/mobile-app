import React, {useEffect} from 'react'
import { useSelector } from 'react-redux'
import { View } from 'react-native'
import AppLoading from '../Loaders/AppLoader';
import Error from '../Error/Error'

export default function AppWrapper({backgroundColor, ...props}){
    
    /**
     * REDUX STATE
     */
    const { isLoading, response } = useSelector(state=>state.app)


    let loader = <AppLoading title={response ? response : 'Loading'}/>


    useEffect(()=>{
        
    }, [isLoading])

    
    return isLoading ? loader :  <View style={{justifyContent:'flex-start', backgroundColor:props.color ? props.color : null}}>
                                                        
                                                        <View style={{width:'100%',paddingHorizontal:10,marginTop:10}}>
                                                            {props.children}
                                                        </View>
                                                    </View>
}