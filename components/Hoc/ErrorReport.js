import * as React from 'react'
import {connect} from 'react-redux'
import { sendCrashReport } from '../../app/redux/dispatchers/app'
import ErrorReporting from '../Error/ErrorReport'
import AppLoading from '../Loaders/AppLoader'
import { View } from 'react-native'

class ErrorReport extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            hasError:false
        }
    }

    static getDerivedStateFromError(error){
        
        return {hasError:true}
    }


    componentDidCatch(error, errorInfo){
        
        this.props.sendCrashReportError(error, errorInfo.componentStack)
    }


   render(){
        let loader = <AppLoading title={this.props.response}/>
        
       if(this.state.hasError){
        return <ErrorReporting />
       }
       

        return   this.props.isLoading ?  loader :   <View style={{justifyContent:'flex-start'}}>
                                                        <View style={{width:'100%',paddingHorizontal:10,marginTop:10}}>
                                                            {this.props.children}
                                                        </View>
                                                    </View>
   }
}

const mapStateToProps = state => {
    const {isLoading, response} = state.app
    const {user} = state.auth
    return {
        isLoading:isLoading,
        response:response,
        user:user

    }
}

const mapDispathToProps = dispatch => {
    return {
        sendCrashReportError:(error, errorInfo)=>dispatch(sendCrashReport(error, errorInfo))
    }
}

export default connect(mapStateToProps, mapDispathToProps)(ErrorReport)