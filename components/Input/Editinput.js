import React, { Fragment, useState, useEffect } from 'react'
import { List, Divider, Subheading } from 'react-native-paper'
import { TextInput, StyleSheet, View, TouchableOpacity } from 'react-native'
import Colors from '../../constants/Colors'
import { Input, normalize } from 'react-native-elements'
import { MaterialCommunityIcons } from '@expo/vector-icons'

export default function EditInput(props){
   

    return <Fragment>
                {props.isEditing ? 
                <Input 
                    placeholder={props.labelText}
                    label={props.inputLabel} 
                    containerStyle={{paddingHorizontal:0, paddingVertical:10}}
                    autoCorrect={false}
                    rightIcon={()=><TouchableOpacity onPress={props.onLeave}><View style={{padding:15, marginTop:-15}}><MaterialCommunityIcons name="check" size={normalize(20)} color={Colors.accent}/></View></TouchableOpacity>}
                    inputContainerStyle={{borderBottomColor:Colors.light}}
                    onChangeText={(value)=>props.onUpdate(value)}
                    inputStyle={{color:Colors.secondary, fontSize:normalize(12)}}
                    autoFocus={props.isEditing}
                    {...props}
                    /> :
                <List.Item 
                    title={props.labelText}
                    titleStyle={{fontSize:normalize(10), textTransform:'uppercase', paddingVertical:5, color:Colors.gray}}
                    description={props.data}
                    descriptionStyle={{fontSize:normalize(14), color:'#888'}}
                    style={{paddingLeft:0, marginLeft:-8}} 
                    right={()=><TouchableOpacity onPress={()=>props.onEdit(props.target)} style={{justifyContent:'center'}}><View style={{padding:15}}><MaterialCommunityIcons name="pencil" size={normalize(14)} color={Colors.tintColor}/></View></TouchableOpacity>}
                    
                    />}
                <Divider />
            </Fragment>
}

const styles = StyleSheet.create({
    container:{

    },
    input:{
        paddingTop:20,
        paddingBottom:10,
        color:Colors.secondary,
        width:'100%',
        marginTop:5,
        paddingLeft:6
    },
    editInput:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingRight:8
    }
})