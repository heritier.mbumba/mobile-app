import React from 'react'
import {TouchableOpacity, Text, View, StyleSheet} from 'react-native'
import Colors from '../../constants/Colors'

export default function InvoicePayButton(props){
    
    return <View style={styles.container}>
            <Text style={styles.amount}>{'R' + props.amount}</Text>
            <TouchableOpacity style={styles.payButton} {...props}>
                <Text style={{color:Colors.tintColor}}>{props.title}</Text>
            </TouchableOpacity>
        </View>
}

const styles = StyleSheet.create(
    {
        container:{
            flexDirection:'row', 
            justifyContent:'space-between', 
            alignItems:'center'
        },
        amount:{
            color:Colors.secondary, 
            paddingRight:10, 
            paddingTop:10, 
            color:Colors.accent
        },
        payButton:{
            justifyContent:'center', 
            alignItems:'center', 
            borderRadius:3, 
            borderWidth:1, 
            borderColor:Colors.tintColor, 
            width:70, paddingVertical:0, height:30, marginTop:10, }
    }
)