import React, { Fragment } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Divider, List, Subheading } from 'react-native-paper'
import Colors from '../../constants/Colors'
import { ListItem, normalize } from 'react-native-elements'

export function ListItems(props)
{
    return  <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', paddingVertical:10, backgroundColor:'#333', width:'100%', marginHorizontal:0}}>
                <Subheading style={{paddingLeft:0}}>{props.title}</Subheading>
                <Subheading>{props.value}</Subheading>
            </View>
}


export function CustomList(props){
    return <ListItem 
                title={props.title} 
                subtitle={props.subtitle}  {...props} 
                rightTitle={props.content} 
                style={{marginVertical:5}} 
                titleStyle={styles.title} 
                rightTitleStyle={{color:props.danger ? Colors.danger : Colors.tintColor, fontSize:normalize(20), fontWeight:'bold'}} 
                containerStyle={styles.container} 
                chevron/>
}

const styles = StyleSheet.create({

    container:{
        borderRadius:3, shadowColor: Colors.grayText,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 1.5,
    },
    title:{
        textTransform:'uppercase', 
        color:'#777',
        fontWeight:'700',
        fontSize:normalize(14),
        paddingVertical:10,
        lineHeight:18,
        letterSpacing:.8
    }
})