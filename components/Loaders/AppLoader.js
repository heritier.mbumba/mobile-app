import React from 'react'

import { ActivityIndicator, View, Text } from 'react-native'
import Colors from '../../constants/Colors'
import { useSelector } from 'react-redux'

export default function AppLoading(props){
    
    return <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:Colors.light}}>
                <ActivityIndicator color={Colors.tintColor} size="large"/>
                <Text style={{color:Colors.secondary, paddingVertical:10}}>{props.title}</Text>
            </View>
}
