import React from 'react'
import {Image} from 'react-native'

export  function Logo(props){
    return <Image source={require('../../assets/images/android/logo-hdpi.png')} resizeMode="contain" style={{height:60, width:160}}/>
}

export function SmallLogo(){
    return <Image source={require('../../assets/logo.png')} style={{width:80, height:30}} resizeMode="contain"/>
}