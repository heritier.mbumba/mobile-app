import * as React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import Colors from '../../constants/Colors';
import { Subheading } from 'react-native-paper';
import { normalize } from 'react-native-elements';
import * as WebBrowser from 'expo-web-browser';

export default function Attachement({data}){
    const [notice, setNotice] = React.useState()

    const _handlePressButtonAsync = async () => {
        await WebBrowser.openBrowserAsync(data[0].attachment_url);
       
      };

    return data.length > 0 ?    <TouchableOpacity onPress={()=>_handlePressButtonAsync()} style={{width:'100%', justifyContent:'center', paddingHorizontal:10}}>
                                    <View style={{flexDirection:'row', alignItems:'center'}}>
                                        <Subheading style={{color:Colors.gray, paddingRight:10}}>{data.length} attachment</Subheading>
                                        <MaterialCommunityIcons name="attachment" size={20} color={Colors.gray}/>
                                    </View>
                                    {notice ? <Subheading style={{fontSize:normalize(12), color:Colors.danger, lineHeight:12}}>{notice}</Subheading>:null}
                                </TouchableOpacity> : null
}