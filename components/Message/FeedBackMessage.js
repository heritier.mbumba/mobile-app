import * as React from 'react'
import { View } from 'react-native'
import { Subheading, Card, Divider } from 'react-native-paper'
import Colors from '../../constants/Colors'
import { normalize } from 'react-native-elements'
import * as utils from '../../app/utils'
import { AuthContext } from '../../context/Auth'

export default function FeedBackMessage({title}){

    const { getTickets } = React.useContext(AuthContext)

    React.useEffect(()=>{
        utils.waitToRefresh(4000).then(()=>getTickets())
    }, [])

    return title ? <View>
                        <Card>
                            <Card.Content>
                                <Subheading style={{textAlign:'center', color:Colors.accent, fontSize:normalize(18), paddingHorizontal:10, paddingBottom:10}}>{title}</Subheading>
                                <Divider />
                                <Subheading style={{textAlign:'center', paddingVertical:10}}>
                                    Thank you for taking the time to share feedback with us.
                                </Subheading>
                                <Divider />
                                <Subheading style={{textAlign:'center', paddingVertical:10, fontSize:normalize(12)}}>
                                    Feel free to share your thought and feeling with us at any time, the more information your give us the better we can make this app.
                                </Subheading>
                            </Card.Content>
                        </Card>
                    </View> :  null
}