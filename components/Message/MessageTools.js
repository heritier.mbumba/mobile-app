import * as React from 'react'
import {View, TouchableOpacity, ActivityIndicator} from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import Colors from '../../constants/Colors'
import { Subheading, Button } from 'react-native-paper'

export default function MessageTools({media, onResolveTicket, onCloseTicket, loading}){
    return  <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', flexWrap:'wrap', borderTopColor:Colors.grayText, borderTopWidth:1, paddingVertical:5, width:'100%', paddingLeft:10}}>
                {media ? <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={{paddingRight:20}}>
                        <MaterialCommunityIcons name="camera" size={25} color={Colors.secondary}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{paddingRight:20}}>
                        <MaterialCommunityIcons name="attachment" size={25} color={Colors.secondary}/>
                    </TouchableOpacity>
                </View> : null}
                {loading ? <ActivityIndicator size="small" color={Colors.tintColor}/> : <View style={{flexDirection:'row'}}>
                    <Button mode="contained" labelStyle={{color:Colors.light}} onPress={onResolveTicket}>Mark resolved</Button>
                </View>}
            </View>
}