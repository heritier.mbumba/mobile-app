import React from 'react'
import {StyleSheet, View, Text} from 'react-native'
import { normalize } from 'react-native-elements'
import Colors from '../../constants/Colors'
import { Subheading, Divider, Button, Card } from 'react-native-paper'


export default function Notify(props){
    return <View style={styles.container}>
                <View style={styles.content}>
                    <Card style={{borderRadius:0}}>
                        <Card.Content style={styles.cardContent}>
                            {/* <Card.Title subtitle="Domain" subtitleStyle={{}}/> */}
                            <Subheading style={{textAlign:'center', textTransform:'uppercase'}}>Domain</Subheading>
                            <Subheading style={styles.title}>harry.co.za</Subheading>
                            <Text style={{textAlign:'center'}}>is expiring in 30 days</Text>
                        </Card.Content>
                        
                    </Card>
                </View>
            </View>
}

const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:10,
        marginTop:10
    },
    content:{
        width:'100%',
        
    },
    titleTop:{
        textTransform:'uppercase'
    },
    title:{
        fontSize:normalize(25),
        textAlign:'center',
        color:Colors.dangerLight
    },
    actions:{
        flexDirection:'row',

    },
    cardContent:{
        backgroundColor:'#f8d7da',
        borderRadius:0
    }
})