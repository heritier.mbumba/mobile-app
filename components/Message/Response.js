import React, { useEffect } from 'react'
import {View, Text, StyleSheet} from 'react-native'
import Colors from '../../constants/Colors'
import {Ionicons} from '@expo/vector-icons'
import { Subheading } from 'react-native-paper'
import * as utils from '../../app/utils'
import { clearErrorDispatcher } from '../../app/redux/dispatchers/app'
import { useDispatch } from 'react-redux'

export default function Response({message}){

    const dispatch = useDispatch()

    useEffect(()=>{
        if(message){
            dispatch(clearErrorDispatcher())
        }
    }, [])

    return  message ? <View style={styles.container}>
                <View style={styles.innerContainer}>
                    <Ionicons name="ios-checkmark-circle-outline" color="#155724" size={25}/>
                    <Subheading style={{paddingLeft:10}}>{message}</Subheading>
                </View>
            </View> : null
}

const styles = StyleSheet.create({
    container:{
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    innerContainer:{
        width:'100%',
        paddingVertical:5,
        paddingHorizontal:10,
        backgroundColor:Colors.success,
        borderWidth:1,
        borderColor:'#c3e6cb',
        flexDirection:'row',
        alignItems:'center',
        marginBottom:10
    }
})