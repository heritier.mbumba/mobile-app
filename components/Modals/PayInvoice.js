import * as React from 'react'
import {Modal, View, Alert} from 'react-native'
import { useSelector } from 'react-redux'
import { ActivityIndicator, Subheading, Card, Button } from 'react-native-paper'
import WebView from 'react-native-webview'
import Colors from '../../constants/Colors'
import { normalize } from 'react-native-elements'
import {Linking} from 'expo'

export default function PayInvoice({uri, isModalOpened, onCloseModal, invoiceNumber}){

    const {user, svgp} = useSelector(state=>state.auth)
    const [isLoading, setIsLoading] = React.useState(true)

    const INJECTED_SCRIPT = `
                                document.getElementById('inputEmail').value = '${user.email}';
                                document.getElementById('inputPassword').value = '${svgp}';
                                document.forms[0].submit();
                                document.querySelector('.sidebar').style.display = 'none'`

    const shouldLoadWithRequest = request => {
        if (!request || !request.url) {
            return true;
          }
        
          // list of schemas we will allow the webview
          // to open natively
          if(request.url.startsWith("tel:") ||
            request.url.startsWith("mailto:") ||
            request.url.startsWith("maps:") ||
            request.url.startsWith("geo:") ||
            request.url.startsWith("sms:") ||
            request.url.startsWith('masterpass.app.scheme:') ||
            request.url.startsWith('zapper.app.scheme:')
            ){
            Linking.openURL(request.url).catch(er => {
              Alert.alert('Error', request.url.startsWith('zapper.app.scheme:') ? 'Zapper app is not installed on your device' : 'Masterpass app is not installed on your device')
            });
            return false;
          }
        
          // let everything else to the webview
          return true;
    }
    const loading = <View style={{flex:1, justifyContent:'flex-start', alignItems:'center'}}><ActivityIndicator size="large" color={Colors.tintColor}/></View>

    return <Modal visible={isModalOpened} animationType="slide" transparent>
                <View style={{width:'100%', height:'100%', justifyContent:'flex-end', alignItems:'flex-end', backgroundColor:'rgba(0,0,0,.2)'}}>
                    <View style={{backgroundColor:'#fff', height:'100%', width:'100%', paddingVertical:40}}>
                        <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', paddingHorizontal:20, paddingVertical:10}}>
                            <View>
                                <Subheading style={{fontWeight:'bold', fontSize:normalize(20), lineHeight:30}}>#{invoiceNumber}</Subheading>
                                <Subheading>Invoice payment</Subheading>
                            </View>
                            <Button mode="outlined" onPress={()=>onCloseModal()} labelStyle={{color:Colors.danger, fontSize:normalize(16)}}>Close</Button>
                        </View>
                        <Card>
                            <Card.Content style={{height:'100%'}}> 
                                <WebView
                                    source={
                                        {
                                            uri:`${uri}`
                                        }
                                    }
                                    onShouldStartLoadWithRequest={request=>shouldLoadWithRequest(request)}
                                    incognito={true}
                                    injectedJavaScript={INJECTED_SCRIPT}
                                    originWhitelist={['*']}
                                    startInLoadingState={isLoading}
                                    renderLoading={() => loading}
                                    scalesPageToFit={false}
                                    textZoom={100}
                                    renderError={errorName => <Subheading>{errorName}</Subheading>}
                                    style={{backgroundColor:Colors.light, flex:1}}/>
                            </Card.Content>
                        </Card>
                    </View>
                </View>
            </Modal>
}