import React, {PureComponent, Fragment} from 'react'
import { Card, Divider, Button, Title, List } from 'react-native-paper'
import {ViewButton} from '../Button/ViewButton'
import { View } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import Colors from '../../constants/Colors'

export default class ListItem extends PureComponent {
    
    render()
    {
        return <View style={{height:60, justifyContent:'center',}}>
                    <List.Item 
                        title={this.props.title} 
                        description={this.props.description}
                        style={{paddingLeft:0, marginLeft:-8}}
                        right={()=><View style={{flexDirection:'row', alignItems:'center'}}><MaterialCommunityIcons name="chevron-right" size={25} color={Colors.secondary}/></View>}
                        onPress={()=>this.props.navigate()}
                    />
                    <Divider />
                </View>
    
    }
}

