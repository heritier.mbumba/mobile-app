import React from 'react'
import { useSelector } from 'react-redux'
import { View, StyleSheet } from 'react-native'
import { Subheading, Title } from 'react-native-paper'
import { normalize } from 'react-native-elements'

export default function User(props){
    /**
     * REDUX STATE
     */
    const user = useSelector(state=>state.auth.user)
    return  <View style={styles.container}>
                
                <Subheading style={{fontSize:20, fontWeight:'bold', marginBottom:-5, textTransform:'uppercase'}}>{user.firstname + ' ' + user.lastname}</Subheading>
                <Subheading style={{fontSize:normalize(14)}}>{user.email}</Subheading>
            </View>
}

const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        
        //paddingHorizontal:30
    }
})