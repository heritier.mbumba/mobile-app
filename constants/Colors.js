const tintColor = '#20bfdd'

export default {
    tintColor,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColor,
    tabBar: '#fefefe',
    errorBackground: 'red',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
    secondary:'#56565b',
    grayText:'#f1f1f1',
    accent:'#03a67b',
    light:'#fff',
    danger:'#cc0000',
    success:'#d4edda',
    gray:'#ccc',
    dangerLight:'#721c24'
}