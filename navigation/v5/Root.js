import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import DashboardScreen from '../../screens/Dashboard';
import DomainScreen from '../../screens/Domain/Domain';
import ServiceScreen from '../../screens/Service/Service';
import MenuScreen from '../../screens/App/Menu';
import TicketScreen from '../../screens/Support/Ticket';
import NotificationScreen from '../../screens/App/Notification';
import { AuthContext } from '../../context/Auth';
import Colors from '../../constants/Colors';
import { CustomHeader } from '../../components/Header/Custom';
import { logout, getProfile, checkAuth, LoginRequestDispatcher } from '../../app/redux/dispatchers/auth';
import { getDomains, RenewDomainDispatcher } from '../../app/redux/dispatchers/domain';
import { getServices, getInvoiceIdForSuspendedService } from '../../app/redux/dispatchers/service';
import { getInvoices } from '../../app/redux/dispatchers/billing';
import { isEditing, getExpiredProducts, getNotification, getFeedbackMessage, logActivity } from '../../app/redux/dispatchers/app';
import { getGroups, getTickets, openTicket, updateNotification, setActiveTicket } from '../../app/redux/dispatchers/support';
import { useSelector, useDispatch } from 'react-redux';
import ExpiredDomainScreen from '../../screens/Domain/ExpiredDomain';
import DomainTransferScreen from '../../screens/Domain/DomainTransfer';
import DomainDetailScreen from '../../screens/Domain/DomainSetting';
import { normalize } from 'react-native-elements';
import ServiceAddonScreen from '../../screens/Service/ServiceAddon';
import SuspendedServiceScreen from '../../screens/Service/SuspendedService';
import BillingScreen from '../../screens/Billing/Billing';
import InvoiceScreen from '../../screens/Billing/Invoice';
import PaidInvoiceScreen from '../../screens/Billing/PaidInvoice';
import { Dimensions } from 'react-native';
import { SmallLogo } from '../../components/Logo/Logo';
import { RightElement } from '../../components/Header/HeaderElement';
import OpenedTicketScreen from '../../screens/Support/OpenedTicket';
import AccountScreen from '../../screens/App/Account';
import ProfileScreen from '../../screens/App/Profile';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import LoginScreen from '../../screens/Auth/Login';
import OpenSupportTicketScreen from '../../screens/Support/OpenSupportTicket';
import PendingTicketScreen from '../../screens/Support/pendingTicket';
import FeedbackScreen from '../../screens/App/Feedback';
import InvoiceDetailScreen from '../../screens/Billing/InvoiceDetail';
import { RightButton } from '../../components/Header/RightButton';
import ViewInvoiceScreen from '../../screens/Billing/ViewInvoice';
import ServiceDetailScreen from '../../screens/Service/ServiceDetail';
import SuspendedServiceDetailScreen from '../../screens/Service/SuspendedServiceDetail';
import ServiceAddonDetailScreen from '../../screens/Service/ServiceAddonDetail';
import TicketDetailScreen from '../../screens/Support/TicketDetail';
import { navigation } from '../../app/utils';
import ReadNotificationScreen from '../../screens/App/ReadNotification';
import { Updates } from 'expo'
import FreeServiceScreen from '../../screens/Service/FreeService';

const Tab = createBottomTabNavigator()
const Stack = createStackNavigator()
const TopTab = createMaterialTopTabNavigator()

const defaultConfig = {
    headerStyle:{
        backgroundColor:Colors.tintColor}, 
        headerTitleStyle:{color:Colors.light, 
        fontSize:normalize(16),
        borderBottomWidth: 0,
        borderBottomColor:Colors.tintColor,
        elevation: 0,
        shadowOpacity: 0
        },

    headerBackTitleStyle:{
        color:Colors.light
    },
    headerBackTitle:'',

    headerTintColor:Colors.light
}


/**
 * TOP TAB DEFAULT CONFIG
 */
const defaultTopTabConfig = {
   scrollEnabled:true,
   indicatorStyle:{
       borderBottomColor: Colors.light,
       borderBottomWidth: 2
   },
   activeTintColor:Colors.tintColor,
   inactiveTintColor:Colors.gray
}

/**
 * DOMAINS SCREEN TABS
 */
function domainTabs(){
    return (
        <TopTab.Navigator tabBarOptions={{...defaultTopTabConfig, tabStyle:{width:Dimensions.get('screen').width / 3}}}>
             <TopTab.Screen name="Active" component={DomainScreen}/>
             <TopTab.Screen name="Expired" component={ExpiredDomainScreen} />
             <TopTab.Screen name="Transfer" component={DomainTransferScreen} />
        </TopTab.Navigator>
    )
}

/**
 * DOMAINS SCREEN TABS
 */
function serviceTabs(){
    /**
     * REDUX
     */
    const {addons} = useSelector(state=>state.service)
    return (
        <TopTab.Navigator tabBarOptions={{...defaultTopTabConfig, tabStyle:{width:Dimensions.get('screen').width / 3}}}>
             <TopTab.Screen name="Active" component={ServiceScreen}/>
             {addons != undefined && addons.length > 0 ? <TopTab.Screen name="Addons" component={ServiceAddonScreen} />:null}
             <TopTab.Screen name="Suspended" component={SuspendedServiceScreen} />
        </TopTab.Navigator>
    )
}


/**
 * TAB NAVIGATOR
 */
function Home() {
    return (
      <Tab.Navigator tabBarOptions={
        {
            activeTintColor:Colors.tintColor, 
            inactiveTintColor:Colors.secondary
        }
    }>
        <Tab.Screen name={navigation.Dashboard} component={DashboardScreen} options={{tabBarLabel:'Dashboard', tabBarIcon:({focused})=><MaterialCommunityIcons name="home" color={focused ? Colors.tintColor : Colors.gray} size={20}/>}} />
        <Tab.Screen name="Domains" component={domainTabs} options={{tabBarLabel:'Domains', tabBarIcon:({focused})=><MaterialCommunityIcons name="earth" color={focused ? Colors.tintColor : Colors.gray} size={20}/>}} />
        <Tab.Screen name="Services" component={serviceTabs} options={{tabBarLabel:'Services', tabBarIcon:({focused})=><MaterialCommunityIcons name="briefcase" color={focused ? Colors.tintColor : Colors.gray} size={20}/>}}/>
        <Tab.Screen name="More" component={MenuScreen} options={{tabBarLabel:'More', tabBarIcon:({focused})=><MaterialCommunityIcons name="dots-horizontal" color={focused ? Colors.tintColor : Colors.gray} size={20}/>}}/>
      </Tab.Navigator>
    );
  }

function Billing(){
    return (
        <TopTab.Navigator tabBarOptions={{...defaultTopTabConfig, tabStyle:{width:Dimensions.get('screen').width / 3}}}>
            <TopTab.Screen name="BillingOverview" component={BillingScreen} options={{tabBarLabel:'Overview'}}/>
            <TopTab.Screen name="Unpaid" component={InvoiceScreen}/>
            <TopTab.Screen name="Paid" component={PaidInvoiceScreen}/>
        </TopTab.Navigator>
    )
}


function App() {

    /**
     * REDUX
     */
    const {isAuth, user} = useSelector(state=>state.auth)
    const {tickets} = useSelector(state=>state.support)
    const {error} = useSelector(state=>state.app)

    const dispatch = useDispatch()

    React.useEffect(()=>{
        
        (async ()=>{
            
            try {
                const update = await Updates.checkForUpdateAsync();
                if (update.isAvailable) {  
                  await Updates.fetchUpdateAsync();
                  // ... notify user of update ...
                  Updates.reloadFromCache();
                }
              } catch (e) {
                // handle or log error
              }
        })()

        let isCancelled = false
        let timer;
        if(!isCancelled){
            dispatch(checkAuth())
            if(isAuth){
                timer = setInterval(() => dispatch(checkAuth()), 1000);
                
            }
        }
        
        return () => {
            isCancelled = true
            clearInterval(timer)
        }

    }, [isAuth, tickets, user])

    const authContext = React.useMemo(()=>({
        login:(data)=>{
            dispatch(LoginRequestDispatcher(data))
        },
        logout:() => dispatch(logout()),
        fetchProfile:()=>dispatch(getProfile()),
        fetchDomains:()=>dispatch(getDomains()),
        fetchServices:()=>dispatch(getServices()),
        fetchInvoices:()=>dispatch(getInvoices()),
        isProfileEditing:()=>dispatch(isEditing()),
        feedBack:(obj, device)=>dispatch(getFeedbackMessage(obj, device)),
        expiredProducts:()=>dispatch(getExpiredProducts()),
        RenewDomain:(id) => dispatch(RenewDomainDispatcher(id)),
        user:user,
        tickets:tickets,
        getDepartments:()=>dispatch(getGroups()),
        getTickets:()=>dispatch(getTickets()),
        submitTicket:data=>dispatch(openTicket(data)),
        getNotifications:()=>dispatch(getNotification()),
        updateNotification:(action, obj)=>dispatch(updateNotification(action, obj)),
        getInvoiceIdSuspendedService:id=>dispatch(getInvoiceIdForSuspendedService(id)),
        setActiveTicket:(ticket) => dispatch(setActiveTicket(ticket)),
        logActivity:(action, obj)=>dispatch(logActivity(action, obj))
    }), [user, isAuth])


    return (
      <AuthContext.Provider value={authContext}>
          <NavigationContainer>
           {isAuth ? <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home}  options={{header:()=><CustomHeader title="Dashboard"/>}}/>
                <Stack.Screen name="ServiceDetail" component={ServiceDetailScreen}  options={({route})=>({...defaultConfig, title:route.params.serviceName.length > 12 ? route.params.serviceName.substr(0, 12) + '...' : route.params.serviceName})}/>
                <Stack.Screen name="ServiceAddonDetail" component={ServiceAddonDetailScreen}  options={({route})=>({...defaultConfig, title:route.params.addonName.length > 12 ? route.params.addonName.substr(0, 12) + '...' : route.params.addonName})}/>
                <Stack.Screen name="SuspendedDetail" component={SuspendedServiceDetailScreen}  options={({route})=>({...defaultConfig, title:route.params.serviceName.length > 12 ? route.params.serviceName.substr(0, 12) + '...' : route.params.serviceName})}/>
                <Stack.Screen name="DomainDetail" component={DomainDetailScreen}  options={({route})=>({...defaultConfig, title:route.params.domainName.length > 12 ? route.params.domainName.substr(0, 12) + '...' : route.params.domainName})}/>
                
                <Stack.Screen name="Billing" component={Billing} options={{...defaultConfig, headerRight:()=><RightElement />}}/>
                <Stack.Screen name="InvoiceDetail" component={InvoiceDetailScreen} options={({route})=>({...defaultConfig, title:route.params.invoiceNo})}/>
                <Stack.Screen name="ViewInvoice" component={ViewInvoiceScreen} options={{...defaultConfig, title:"Payment"}}/>
                <Stack.Screen name="Support" component={TicketScreen} options={{...defaultConfig, headerRight:()=><RightElement />}}/>
                <Stack.Screen name="OpenSupportTicket" component={OpenSupportTicketScreen} options={({route})=>({...defaultConfig, title:route.params.department.length > 12 ? route.params.department.substr(0, 12) + '...' : route.params.department,})}/>
                <Stack.Screen name="OpenedTicket" component={OpenedTicketScreen} options={{...defaultConfig, title:"Opened Tickets", headerRight:()=><RightElement />}}/>
                <Stack.Screen name="TicketDetail" component={TicketDetailScreen} options={({route})=>({...defaultConfig, title:route.params.title.length > 12 ? route.params.title.substr(0, 12) + '...' : route.params.title})}/>
                <Stack.Screen name="PendingTicket" component={PendingTicketScreen} options={{...defaultConfig, title:"Pending Tickets", headerRight:()=><RightElement />}}/>
                <Stack.Screen name="Notification" component={NotificationScreen} options={{...defaultConfig, headerRight:()=><RightElement />}}/>
                <Stack.Screen name="ReadNotification" component={ReadNotificationScreen} options={({route})=>({...defaultConfig, title:route.params.title.length > 12 ? route.params.title.substr(0, 12) + '...': route.params.title})}/>
                <Stack.Screen name="Account" component={AccountScreen} options={{...defaultConfig, headerRight:()=><RightElement />}}/>
                <Stack.Screen name="Profile" component={ProfileScreen} options={{...defaultConfig, headerRight:()=><RightButton title="Save"/>}}/>
                <Stack.Screen name="Feedback" component={FeedbackScreen} options={{...defaultConfig, headerRight:()=><RightElement />}}/>
            </Stack.Navigator>:

            <Stack.Navigator headerMode="none">
                <Stack.Screen name="Login" component={LoginScreen}/>          
            </Stack.Navigator>}
        </NavigationContainer>
      </AuthContext.Provider>
    );
  }

  export default App