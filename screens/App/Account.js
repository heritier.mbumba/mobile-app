import React from 'react'
import {  ScrollView } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, List } from 'react-native-paper'
import * as utils from '../../app/utils'


export default function AccountScreen(props){
    
    return  <AppWrapper>
                <ScrollView>
                        <Card>
                            <Card.Content>
                            <List.Item
                                onPress={()=>props.navigation.navigate(utils.navigation.Profile)}
                                title={utils.lang.AccountDetail}
                                description={utils.lang.ViewAccountDetails}
                                left={()=><List.Icon {...props} icon="account"/>}
                                right={()=><List.Icon {...props} icon="chevron-right"/>} />
                           
                            </Card.Content>
                        </Card>
                        </ScrollView>
            </AppWrapper>
          
           
}
