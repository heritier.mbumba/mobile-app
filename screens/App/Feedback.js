import React, { useState, useEffect, useContext, useRef } from 'react'
import { View, KeyboardAvoidingView, ScrollView, StyleSheet, Platform } from 'react-native'
import Colors from '../../constants/Colors'
import { useSelector } from 'react-redux'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Subheading, Card, Divider, Button } from 'react-native-paper'
import FeelingEmoji from '../../components/FeelingEmoji/FeelingEmoji'
import { Input, normalize,  } from 'react-native-elements'
import { AuthContext } from '../../context/Auth'
import * as Device from 'expo-device';
import FeedBackMessage from '../../components/Message/FeedBackMessage'
import * as utils from '../../app/utils'
import Error from '../../components/Error/Error';


export default function FeedbackScreen(props){
    const [getFeedback, setGetFeeback] = useState()
    const [error, setError] = useState()
    const [device, setDevice] = useState()
    const {feedBack} = useContext(AuthContext)

    const _scroll = useRef()
    const _inputRef = useRef()

    /**
     * REDUX STATUS
     */
    const {successMsg, feeling} = useSelector(state=>state.app)
    

    useEffect(()=>{

        setDevice({
            isDevice:Device.isDevice,
            brand:Device.brand,
            model:Device.modelName,
            osName:Device.osName,
            osVersion:Device.osVersion
        })

        if(feeling){
            _inputRef.current.focus()
            _scroll.current.scrollToEnd()
        }

        

    }, [successMsg, getFeedback, feeling])

    const _getFeedbackHandler = message => {
        setGetFeeback(message)
    }

    const _onSubmit = () => {
        if(!getFeedback){
            setError('You cannot submit empty form')
            return
        }

        if(!feeling){
            setError('Please add your feeling')
            return
        }
        
        feedBack(getFeedback, device)
    }

    
    
    
    return  <AppWrapper>
            {successMsg ? <FeedBackMessage title={successMsg}/> : 
                <View style={{height:'100%', alignItems:'flex-start'}}>
                    <ScrollView contentContainerStyle={{paddingBottom:150}} ref={_scroll}>
                        <Card>
                            <Card.Content>
                                <Subheading style={{textAlign:'center', paddingVertical:20, fontSize:normalize(18), paddingHorizontal:20}}>{utils.lang.FeedbackWhatDoYouThink}</Subheading>
                                <Divider />
                                <FeelingEmoji />
                                <Divider />
                                <Subheading style={{textAlign:'center', fontSize:normalize(14),  paddingHorizontal:10, paddingTop:10}}>{utils.lang.FeedbackTellUs}</Subheading>
                                <Subheading style={{textAlign:'center', fontSize:normalize(12), paddingHorizontal:10, paddingBottom:10}}>{utils.lang.FeedbackWhatDidYouLike}</Subheading>
                                <Divider />
                            </Card.Content>
                            <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} keyboardVerticalOffset={Platform.OS=='ios' ? 64:0}>
                                <Input 
                                    placeholder={utils.lang.EnterYourFeedback}
                                    multiline
                                    numberOfLines={10}
                                    textAlignVertical="top"
                                    ref={_inputRef}
                                    inputContainerStyle={{borderBottomColor:Colors.grayText, paddingTop:10,}}
                                    onChangeText={message=>_getFeedbackHandler(message)}
                                    onContentSizeChange={()=>setError('')}
                                    inputStyle={{color:Colors.secondary, fontSize:normalize(14), height:Platform.OS == 'ios' ? 100 : null, paddingHorizontal:10}}
                                    />
                                <Button mode="contained" onPress={()=>_onSubmit()} style={{marginVertical:10, marginHorizontal:10}} labelStyle={{color:Colors.light}}>{utils.lang.SubmitButton}</Button>
                                <View style={{paddingHorizontal:10}}>
                                <Error title={error}/>
                                </View>
                            </KeyboardAvoidingView>
                        </Card>
                    </ScrollView> 
                </View>   }   
            </AppWrapper>
}

