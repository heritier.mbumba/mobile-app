import React from 'react'
import { ScrollView } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, List, Divider } from 'react-native-paper'
import * as utils from '../../app/utils'


export default function MenuScreen({ navigation }){

    return  <AppWrapper>
                <ScrollView>
                    <Card>
                        <Card.Content>
                            <List.Item
                                onPress={()=>navigation.navigate(utils.navigation.Account)}
                                title={utils.lang.AccountSettings}
                                description={utils.lang.ViewAccountSetting}
                                left={()=><List.Icon  icon="account"/>}
                                right={()=><List.Icon  icon="chevron-right"/>}
                                 />
                            <Divider />
                            <List.Item
                                onPress={()=>navigation.navigate(utils.navigation.Billing)}
                                title={utils.lang.Billing}
                                description={utils.lang.ViewYourBillingStatus}
                                left={()=><List.Icon  icon="bank"/>}
                                right={()=><List.Icon  icon="chevron-right"/>} />
                            <Divider />
                            <List.Item
                                onPress={()=>navigation.navigate(utils.navigation.Support)}
                                title={utils.lang.SupportAndQueries}
                                description={utils.lang.ViewAndOpenTicket}
                                left={()=><List.Icon  icon="face-agent"/>}
                                right={()=><List.Icon  icon="chevron-right"/>} />
                            <Divider />
                            <List.Item
                                onPress={()=>navigation.navigate(utils.navigation.Notification)}
                                title={utils.lang.Notification}
                                description={utils.lang.ViewNotification}
                                left={()=><List.Icon  icon="bell"/>}
                                right={()=><List.Icon  icon="chevron-right"/>} />
                            <Divider />
                            
                            <List.Item
                                onPress={()=>navigation.navigate(utils.navigation.Feedback)}
                                title={utils.lang.Feedback}
                                description={utils.lang.SendFeedBack}
                                left={()=><List.Icon  icon="message-draw"/>}
                                right={()=><List.Icon  icon="chevron-right"/>} />
                            <Divider />
                        </Card.Content>
                    </Card>
                </ScrollView>
        </AppWrapper>
}
