import React, {useContext, useEffect, useState, useRef} from 'react'
import { View, Text, ActivityIndicator, ScrollView,  TouchableOpacity } from 'react-native'
import Colors from '../../constants/Colors'
import {  useSelector } from 'react-redux'
import AppWrapper from '../../components/Hoc/AppWrapper'
import {  Card, Divider, Subheading } from 'react-native-paper'
import { AuthContext } from '../../context/Auth'
import { normalize } from 'react-native-elements'
import _ from 'lodash'
import * as utils from '../../app/utils'


export default function NotificationScreen({route, navigation}){


    const {getNotifications, updateNotification} = useContext(AuthContext)


    /**
     * REDUX STATE
     */
    const {notifications} = useSelector(state=>state.notification)
    const {isUpdating} = useSelector(state=>state.app)

    useEffect(()=>{
        getNotifications()
        
    }, [notifications])

    /**
     * 
     * @param {*} id 
     */
    

    const _actionReaderRef = useRef()
   
    


    let content = <View style={{justifyContent:'center', alignItems:'center'}}><Subheading>No notification</Subheading></View>
    if(notifications.length > 0){
        let data = []
        const getType = type => {
            switch (type) {
                case 'DomainRenwelNotification':
                return 'Domain'    
                
                case 'InvoiceReminderNotification':
                    return 'Billing'
            }
        }
        notifications.map(n=>{
            data.push({id:n.id, type:getType(n.commentable_type), name:n.title, date:"", message:n.message, data:n.data, token:n.token, userid:n.userid, status:n.status})
        })

        
       
        content = data.map((n, i)=><Card style={{marginTop:5, }} key={i}>
            
                    <Card.Content>
                        <View>
                            <View>
                                <Subheading style={{textTransform:'uppercase', color:Colors.tintColor, fontSize:normalize(13)}}>{n.type}</Subheading>
                                <Subheading style={{color:'#888', fontSize:normalize(20), paddingRight:10, lineHeight:25}}>{JSON.parse(n.data).length > 1 ? n.name : JSON.parse(n.data)[0].name}</Subheading>

                                {JSON.parse(n.data).length == 1 ? <Subheading style={{paddingBottom:10}}><Text style={{color:Colors.danger}}>R{JSON.parse(n.data)[0].price}</Text> | {utils.lang.ExpiredDate} {JSON.parse(n.data)[0].date}</Subheading> :  null}
                                <Subheading style={{paddingBottom:10}}>{n.message}</Subheading>
                            </View>
                            <Divider />
                            
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'flex-start', paddingTop:5, }} ref={_actionReaderRef}>
                            {JSON.parse(n.data).length > 1 ? 
                            (n.status == 2 ? <TouchableOpacity style={{paddingRight:20, justifyContent:'center', alignItems:'center', paddingVertical:10}} onPress={()=>{navigation.navigate(utils.navigation.ReadNotification, {notification:n, title:n.name, type:n.type}); updateNotification('markread', n)}}>
                                <Subheading style={{textTransform:'uppercase', fontSize:normalize(14), color:Colors.tintColor,}}>{utils.lang.ReadMore}</Subheading></TouchableOpacity>:null): null}
                            {isUpdating ? <ActivityIndicator size="small" color={Colors.danger}/> : <TouchableOpacity style={{paddingLeft:10, paddingVertical:10}} onPress={()=>updateNotification('delete', n)}><Subheading style={{textTransform:'uppercase', fontSize:normalize(14), color:Colors.danger}}>{utils.lang.Delete}</Subheading></TouchableOpacity>}
                        </View>
                    </Card.Content>
            </Card>
            )
    }
    
    return  <AppWrapper>
                <ScrollView>
                    {content}
                </ScrollView>
            </AppWrapper>
            
}
