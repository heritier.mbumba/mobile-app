import React, { useState, useContext, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, Divider } from 'react-native-paper'
import { ScrollView } from 'react-native-gesture-handler'
import {normalize} from 'react-native-elements'
import AppLoading from '../../components/Loaders/AppLoader'
import { editProfile } from '../../app/redux/dispatchers/auth'
import Response from '../../components/Message/Response'
import EditInput from '../../components/Input/Editinput'
import { AuthContext } from '../../context/Auth'
import _ from 'lodash'
import * as utils from '../../app/utils'
import Error from '../../components/Error/Error'


export default function ProfileScreen(props){

    const {isProfileEditing} = useContext(AuthContext)

    const [profile, setProfile] = useState()

    const [isEditing, setIsEditing] = useState({
            address1: false,
            address2: false,
            city: false,
            companyname: false,
            country: false,
            email: false,
            firstname: false,
            lastname: false,
            phonenumber: false,
            postcode: false,
            state: false,
    })



    /**
     * dispatch instance hook
     */
    const dispatch = useDispatch()

    /**
     * REDUX STATE
     */
    const user = useSelector(state=>state.auth.user)
    const isLoading = useSelector(state=>state.app.isLoading)
    const {successMsg, error} = useSelector(state=>state.app)


    /**
     * Effect hook
     */
    useEffect(()=>{
        let isCancelled = false

        if(!isCancelled){
            setProfile(user)
            
        }


        return () => {
            isCancelled = true

        }
    }, [successMsg, error])

    /**
     * on edit handler
     * @param {*} name 
     */

    const _edit = key => {
       
        setIsEditing(prev=>({...prev, [key]:true}))
        
    }

    /**
     * On blur handler
     * @param {*} key 
     */
    const _onBlur = key => {
        setIsEditing(prev=>({...prev, [key]:false}))

        if(!_.isEqual(profile, user)){
            isProfileEditing()
            dispatch(editProfile(profile))
        }
        
    }


    /**
     * on update handler
     * @param {*} key 
     * @param {*} text 
     */
    const _update = (key, text) => {
        setProfile(prev=>({...prev, [key]:text}))
    }

    
    return  <AppWrapper>
                    <Response message={successMsg}/>
                    <Error title={error}/>
                    {profile == undefined || isLoading ? <AppLoading title="Updating..."/> : <ScrollView contentContainerStyle={{paddingBottom:30}}>
                       <Card>
                                <Card.Title title={utils.lang.PersonalInfo} titleStyle={{textTransform:'uppercase', fontSize:normalize(15)}}/>
                                <Divider />
                                <Card.Content>
                                    
                                    <EditInput
                                        labelText={utils.lang.FirstName}
                                        inputLabel={profile.firstname}
                                        target='firstname' 
                                        onLeave={()=>_onBlur('firstname')} 
                                        onUpdate={firstname=>_update('firstname', firstname)} 
                                        data={profile.firstname} isEditing={isEditing.firstname} onEdit={()=>_edit('firstname')}
                                        onSubmitEditing={()=>_onBlur('firstname')}
                                        
                                        />

                                    <EditInput
                                        labelText={utils.lang.LastName}
                                        inputLabel={profile.lastname}
                                        target='lastname' 
                                        onLeave={()=>_onBlur('lastname')} 
                                        onUpdate={lastname=>_update('lastname', lastname)} 
                                        onSubmitEditing={()=>_onBlur('lastname')}
                                        data={profile.lastname} isEditing={isEditing.lastname} onEdit={()=>_edit('lastname')}/>

                                    <EditInput
                                        labelText={utils.lang.Email}
                                        inputLabel={profile.email}
                                        target='email' 
                                        onLeave={()=>_onBlur('email')} 
                                        onUpdate={email=>_update('email', email)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('email')}
                                        data={profile.email} isEditing={isEditing.email} onEdit={()=>_edit('email')}/>

                                    <EditInput
                                        labelText={utils.lang.PhoneNumber}
                                        inputLabel={profile.phonenumber}
                                        target='phonenumber' 
                                        onLeave={()=>_onBlur('phonenumber')} 
                                        onUpdate={phonenumber=>_update('phonenumber', phonenumber)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('phonenumber')}
                                        data={profile.phonenumber} isEditing={isEditing.phonenumber} onEdit={()=>_edit('phonenumber')}/>

                                    <EditInput
                                        labelText={utils.lang.CompanyName}
                                        inputLabel={profile.companyname}
                                        target='companyname' 
                                        onLeave={()=>_onBlur('companyname')} 
                                        onUpdate={companyname=>_update('companyname', companyname)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('companyname')}
                                        data={profile.companyname} isEditing={isEditing.companyname} onEdit={()=>_edit('companyname')}/>

                                    
                                </Card.Content>
                        </Card>

                        <Card style={{marginTop:10}}>
                                <Card.Title title={utils.lang.AddressInfo} titleStyle={{textTransform:'uppercase', fontSize:normalize(15)}}/>
                                <Divider />
                                <Card.Content>

                                    

                                    <EditInput
                                        labelText={utils.lang.Address1}
                                        inputLabel={profile.address1}
                                        target='address1' 
                                        onLeave={()=>_onBlur('address1')} 
                                        onUpdate={address1=>_update('address1', address1)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('address1')}
                                        data={profile.address1} isEditing={isEditing.address1} onEdit={()=>_edit('address1')}/>

                                    <EditInput
                                        labelText={utils.lang.Address2}
                                        inputLabel={profile.address2}
                                        target='address2' 
                                        onLeave={()=>_onBlur('address2')} 
                                        onUpdate={address2=>_update('address2', address2)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('address2')}
                                        data={profile.address2} isEditing={isEditing.address2} onEdit={()=>_edit('address2')}/>

                                    <EditInput
                                        labelText={utils.lang.City}
                                        inputLabel={profile.city}
                                        target='city' 
                                        onLeave={()=>_onBlur('city')} 
                                        onUpdate={city=>_update('city', city)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('city')}
                                        data={profile.city} isEditing={isEditing.city} onEdit={()=>_edit('city')}/>

                                    <EditInput
                                        labelText={utils.lang.PostalCode}
                                        inputLabel={profile.postcode}
                                        target='postcode' 
                                        onLeave={()=>_onBlur('postcode')} 
                                        onUpdate={postcode=>_update('postcode', postcode)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('postcode')}
                                        data={profile.postcode} isEditing={isEditing.postcode} onEdit={()=>_edit('postcode')}/>

                                    <EditInput
                                        labelText={utils.lang.State}
                                        inputLabel={profile.state}
                                        target='state' 
                                        onLeave={()=>_onBlur('state')} 
                                        onUpdate={state=>_update('state', state)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('state')}
                                        data={profile.state} isEditing={isEditing.state} onEdit={()=>_edit('state')}/>
                                        
                                    <EditInput
                                        labelText={utils.lang.Country}
                                        inputLabel={profile.country}
                                        target='country' 
                                        onLeave={()=>_onBlur('country')} 
                                        onUpdate={country=>_update('country', country)}
                                        autoCapitalize="none"
                                        onSubmitEditing={()=>_onBlur('country')}
                                        data={profile.country} isEditing={isEditing.country} onEdit={()=>_edit('country')}/>
                                    
                                </Card.Content>
                            </Card>
                    </ScrollView>}
            </AppWrapper>
            
}
