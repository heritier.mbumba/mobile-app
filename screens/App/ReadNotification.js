import React, {useContext, useEffect, useState, Fragment} from 'react'
import { View, Text, ScrollView, StyleSheet } from 'react-native'
import Colors from '../../constants/Colors'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, Divider, Subheading } from 'react-native-paper'
import { AuthContext } from '../../context/Auth'
import { normalize } from 'react-native-elements'
import * as utils from '../../app/utils'


export default function ReadNotificationScreen({route}){

    const {updateNotification} = useContext(AuthContext)

    const [notification, setNotification] = useState()


    useEffect(() => {
        setNotification(route.params.notification)
        updateNotification('read', route.params.notification)
    }, [])

    let content = <View style={{justifyContent:'center', alignItems:'center'}}><Subheading>{utils.lang.NoNotification}</Subheading></View>
    
    const getTotal = () => {
        const amount = JSON.parse(notification.data).reduce((p, c)=>p+=+c.price, 0)
        return amount.toFixed(2)
    }

    if(notification){
        
    const notificationEl = JSON.parse(notification.data).map((d, i)=><Fragment key={i}>
                            <Subheading style={{paddingTop:5, fontSize:normalize(18)}}>{d.name} - <Text style={{color:Colors.accent, fontSize:normalize(14)}}>R{d.price}</Text></Subheading>
                            <Subheading style={{paddingBottom:10, color:Colors.gray, fontStyle:'italic'}}>{utils.lang.ExpiredDate} <Text style={{fontSize:normalize(12)}}>{utils.date(d.expirydate)}</Text></Subheading>
                            <Divider />
                            </Fragment>)

    
        
        content = <Card>
                        <Card.Content>
                            <Subheading style={{paddingBottom:10, fontSize:normalize(30), lineHeight:35, color:Colors.tintColor}}>{notification.name}</Subheading>
                            <Divider />
                            {notificationEl}
                            <Subheading style={{paddingVertical:10}}>{notification.message}</Subheading>
                            <Divider/>
                            <Subheading style={{textTransform:'uppercase', paddingTop:10}}>Total</Subheading>
                            <Subheading style={{paddingBottom:10, fontSize:normalize(30), lineHeight:40, color:Colors.danger}}>{utils.lang.Currency}{getTotal()}</Subheading>
                        </Card.Content>
                    </Card>
    }
    
    
    return  <AppWrapper>
                <ScrollView contentContainerStyle={{paddingBottom:40}}>
                    {content}
                </ScrollView>
            </AppWrapper>
            
}
