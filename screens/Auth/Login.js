import React, {useState, useEffect, useContext, useRef} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { View, Text, StyleSheet, KeyboardAvoidingView, TouchableOpacity, Platform, ScrollView, ActivityIndicator } from 'react-native'
import {Logo} from '../../components/Logo/Logo'
import {Button, Subheading} from 'react-native-paper';
import Colors from '../../constants/Colors'
import { useDifferentAccount } from '../../app/redux/dispatchers/auth';
import Error from '../../components/Error/Error';
import AppLoading from '../../components/Loaders/AppLoader';
import User from '../../components/User/User';
import {Input,  normalize} from 'react-native-elements'
import {MaterialCommunityIcons} from '@expo/vector-icons'
import { AuthContext } from '../../context/Auth'
import * as WebBrowser from 'expo-web-browser';
import {REMOTE_ADDRESS} from '../../app/redux/actions'
import * as utils from '../../app/utils'
import { Header } from 'react-navigation-stack'

export default function LoginScreen(props){

    /**
     * 
     */
    const [getUser, setUser] = useState({email:'', password:''})
    const [isEmpty, setIsEmpty] = useState()
    const [securePass, setsecurePass] = useState(true)

    const { login} = useContext(AuthContext)

    /**
     * REDUX STATE 
     */

     const isLoading = useSelector(state=>state.app.isLoading)
     const isConnected = useSelector(state=>state.app.response)
     const {error} = useSelector(state=>state.app)
     const {user, isAuth, svgp, expiresIn} = useSelector(state=>state.auth)


    const dispatch = useDispatch()

    /**
     * 
     * @param {*} email 
     */
    const _emailHandler = email => {
        
        setUser(prev=>({...prev, email:email}))
        
    }

    /**
     * 
     * @param {*} password 
     */
    const _passwordHandler = password => {
        
        setUser(prev=>({...prev, password:password}))
        
    }


    /**
     * 
     */
    const _submitHandler = () => {
        
        if(getUser.email == '' && getUser.password == ''){
            setIsEmpty('You cannot submit empty form')
            return
        }

        if(user != null) {
               const loggedInUser = {email:user.email, password:getUser.password}
               login(loggedInUser)
               return
               
        }else{
            login(getUser)
            return
        }

    }



    const _togglePasswordSecure = () => {
        setsecurePass(!securePass)
    }

    const _handlePressButtonAsync = async () => {
        await WebBrowser.openBrowserAsync(REMOTE_ADDRESS + 'pwreset.php?mobile=true');
      };

      const _passwordRef = useRef()

    
    return  isLoading ? 
                <AppLoading title={isConnected ? isConnected : "Please wait..."}/> : 
                <View style={{justifyContent:'center', height:'100%', backgroundColor:Colors.light}}>
                    
                    <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : null} keyboardVerticalOffset={Header.HEIGHT}>
                    {user != null ?<User /> : <View style={{alignItems:'center', paddingVertical:30}}>
                            <Logo />
                        </View>}
                        <View style={{ paddingHorizontal:10}}>
                            <ScrollView contentContainerStyle={{paddingBottom:40}} keyboardShouldPersistTaps="handled">
                                {user != null ? null : <Input
                                    label="Email address"
                                    labelStyle={{fontWeight:'400', fontSize:normalize(16), paddingBottom:6}}
                                    leftIconContainerStyle={{alignItems:'flex-start', marginLeft:0, paddingRight:10}}
                                    placeholder="john.doe@example.com"
                                    leftIcon={<MaterialCommunityIcons name="email" size={25} color={Colors.gray}/>}
                                    containerStyle={{marginVertical:10, }}
                                    inputContainerStyle={{borderBottomColor:Colors.gray}}
                                    inputStyle={{color:Colors.secondary}}
                                    onChangeText={email=>_emailHandler(email)}
                                    autoCapitalize="none"
                                    keyboardType="email-address"
                                    autoFocus
                                    returnKeyType="next"
                                    onSubmitEditing={()=>_passwordRef.current.focus()}
                                    defaultValue={getUser.email}
                                    />}
                                
                                <Input
                                    label={utils.lang.Password}
                                    secureTextEntry={securePass?true:false}
                                    labelStyle={{fontWeight:'400', fontSize:normalize(14), paddingBottom:10}}
                                    leftIconContainerStyle={{alignItems:'flex-start', marginLeft:0, paddingRight:10}}
                                    placeholder="******"
                                    leftIcon={<MaterialCommunityIcons name="lock" size={normalize(25)} color={Colors.gray}/>}
                                    rightIcon={<TouchableOpacity onPress={_togglePasswordSecure}><MaterialCommunityIcons name={securePass ? "eye-off": "eye"} size={25} color={Colors.secondary}/></TouchableOpacity>}
                                    containerStyle={{marginVertical:20, }}
                                    inputContainerStyle={{borderBottomColor:Colors.gray}}
                                    inputStyle={{color:Colors.secondary}}
                                    onChangeText={password=>_passwordHandler(password)}
                                    autoCapitalize="none"
                                    returnKeyType="done"
                                    ref={_passwordRef}
                                    autoFocus={user != null ? true : false}
                                    onSubmitEditing={()=>_submitHandler()}
                                    
                                    />
                                <View style={{paddingHorizontal:10}}>
                                    <Error title={isEmpty}/>
                                    <Error title={error} persist={true}/>
                                </View>
                                <Button mode="contained" onPress={() => _submitHandler()} style={styles.loginButton} loading={isLoading ? true : false} disabled={isLoading ? true : false}>
                                    <Text style={styles.loginButtonText}>{utils.lang.Login}</Text>
                                </Button>
                                <View style={styles.forgetAction}>
                                    <Text style={{fontSize:normalize(16), color:Colors.secondary,}}>{utils.lang.ForgetPassword}</Text>
                                    <Button onPress={()=>_handlePressButtonAsync()} style={{paddingLeft:0}} labelStyle={{paddingLeft:0, fontSize:normalize(14)}}><Text>{utils.lang.ClickHere}</Text></Button>
                                </View>
                                {user == null ? null : <View>
                                    <Button onPress={()=>dispatch(useDifferentAccount())}>{utils.lang.UseDifferentAccount}</Button>
                                </View>}
                            </ScrollView>
                        </View>
                    </KeyboardAvoidingView>  
                </View>
            
}



/**
 * STYLES
 */

 const styles = StyleSheet.create({
     
    loginButton:{
        
        paddingVertical:5,
        marginHorizontal:10
    },
    loginButtonText:{
        color:Colors.light,
    },
    forgetAction:{
        justifyContent:'center',
        alignItems:'center',
        paddingTop:20,
        paddingBottom:10,
        flexDirection:'row'
    }
 })
