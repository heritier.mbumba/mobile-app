import React, {useContext, useState, useCallback} from 'react'
import { View, Text, ScrollView, StyleSheet, RefreshControl} from 'react-native'
import { normalize } from 'react-native-elements'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Button, Title, Divider, Subheading, Card, List, ActivityIndicator } from 'react-native-paper'
import Colors from '../../constants/Colors'

import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import { AuthContext } from '../../context/Auth'
import * as utils from '../../app/utils'
import WebView from 'react-native-webview'
import { REMOTE_ADDRESS } from '../../app/redux/actions'
import PayInvoice from '../../components/Modals/PayInvoice'


export default function BillingScreen(props){

    const {fetchInvoices, fetchProfile} = useContext(AuthContext)

    /**
     * STATE
     */
    const [isModalOpened, setIsModalOpened] = useState(false)
    const [uri, setUri] = useState()

     /**
      * REDUX STATE
      */
     const {totalUnpaid, unpaidInvoices} = useSelector(state=>state.billing)
     const user = useSelector(state=>state.auth.user)

      const _payAll = () => {
        setIsModalOpened(true)
    }

    const _payNow = () => {
        setIsModalOpened(true)
    }

    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){
            fetchInvoices()
           
        }
        return () => {
            isCancelled = true
        }
    }, [unpaidInvoices.length])

    useEffect(()=>{

        if(unpaidInvoices.length == 1){
            setUri(`${REMOTE_ADDRESS}viewinvoice.php?id=${unpaidInvoices[0].id}&mobile=true`)
        }

        if(unpaidInvoices.length > 1){
            setUri(`${REMOTE_ADDRESS}clientarea.php?action=masspay&all=true&mobile=true`)
        }

    }, [uri])


    const _closeModalHandler = () => {
        fetchInvoices()
        fetchProfile()
        setIsModalOpened(false)
        
    }

    return  <AppWrapper style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                <ScrollView contentContainerStyle={{paddingBottom:40}} style={{height:'100%'}}>
                    <Card>
                        <Card.Content>
                        <Subheading style={{textTransform:'uppercase', textAlign:'center'}}>Available Credit</Subheading>
                        <Text style={{textAlign:'center', fontSize:normalize(30), color:Colors.accent, paddingBottom:10}}>{user ? 'R' + user.credit : ''}</Text>
                        <Divider />
                        {totalUnpaid > 0 ? <View style={styles.total}>
                            <Subheading style={{textTransform:'uppercase'}}>Invoice unpaid amount</Subheading>
                            <Text style={styles.totalAmount}>R{totalUnpaid}</Text>
                            
                            {unpaidInvoices.length > 1 ? <Button mode="contained" labelStyle={{color:Colors.light}} onPress={_payAll}>Pay all</Button>
                            : <Button mode="contained" labelStyle={{color:Colors.light}} onPress={_payNow}>Pay now</Button>}
                        </View>: null}
                        
                        </Card.Content>
                    </Card>
                </ScrollView>
                <PayInvoice uri={uri} onCloseModal={()=>_closeModalHandler()} invoiceNumber={unpaidInvoices.length > 0 ? unpaidInvoices[0].id : 0} isModalOpened={isModalOpened}/>

            </AppWrapper>
            
}


/**
 * STYLES
 */

 const styles = StyleSheet.create({
     screen:{
        width:'100%',
        paddingHorizontal:10,
        marginTop:10
         
     },
     total:{
         paddingVertical:20,
         width:'100%',
         alignItems:'center',
         
     },
     totalAmount:{
         fontSize:normalize(40),
         paddingBottom:20,
         color:Colors.danger,
         
     },
     box:{
         width:'100%',
         paddingVertical:10
     }
 })