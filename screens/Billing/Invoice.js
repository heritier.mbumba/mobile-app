import React, { Fragment, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { View, StyleSheet, FlatList, Dimensions } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Divider, Card, List, Subheading } from 'react-native-paper'
import Colors from '../../constants/Colors'
import InvoicePayButton from '../../components/Invoice/InvoicePayButton'
import { normalize } from 'react-native-elements'
import { SafeAreaView, useSafeArea } from 'react-native-safe-area-context'
import {MaterialCommunityIcons} from '@expo/vector-icons'
import * as utils from '../../app/utils'


export default function InvoiceScreen({route, navigation}){

    
    /**
     * REDUX STATE
     */

    let unpaidInvoices = useSelector(state=>state.billing.unpaidInvoices)


    let height = Dimensions.get('screen').height

    return  unpaidInvoices.length > 0 ? 
                <AppWrapper>
                    <Card>
                        <Card.Title title={unpaidInvoices != null ? unpaidInvoices.length + " Unpaid Invoices" : 'Unpaid Invoices'} subtitle="Your latest unpaid invoices" titleStyle={{...styles.title, color:Colors.danger}}/>
                        <Divider />
                        <Card.Content style={{maxHeight:height < 600 ? '75%' : '85%'}}>
                            <FlatList 
                                data={unpaidInvoices.reverse()}
                                renderItem={({item})=><Fragment>
                                    <List.Item 
                                    title={'#' + item.id} 
                                    description={'Due date: ' + utils.date(item.duedate)}
                                    right={()=><View style={{flexDirection:'row', alignItems:'center'}}><Subheading style={{color:Colors.danger}}>R{item.total}</Subheading><MaterialCommunityIcons name="chevron-right" size={25} color={Colors.secondary}/></View>}
                                    style={{paddingLeft:0, marginLeft:0}}
                                    onPress={()=>navigation.navigate(utils.navigation.InvoiceDetail, {invoice:item, invoiceNo:'#' + item.id})}
                                    />
                                    <Divider />
                                </Fragment>}
                                getItemLayout = {(data, index) => ({
                                    length: 60,
                                    offset: 60 * index,
                                    index
                                    })}
                                keyExtractor={item=>item.id.toString()}
                                style={{}}
                                />
                        </Card.Content>  
                    </Card>
                </AppWrapper>:<View style={{justifyContent:'center', alignItems:'center', height:'100%'}}><Subheading>No unpaid invoice</Subheading></View>
                
            
}

/**
 * STYLE
 */

const styles = StyleSheet.create({
    screen:{
        width:'100%',
        paddingHorizontal:10,
        marginTop:10
        
    },
    unpaidInvoicesContainer:{ 
        width:'100%', 
        paddingHorizontal:10, 
        marginVertical:10,
        
    },
    flatList:{
        width:'100%',
        height: 'auto',
        maxHeight:200,
        flexGrow: 0
    },

    title:{fontSize:normalize(16), textTransform:'uppercase'}
})