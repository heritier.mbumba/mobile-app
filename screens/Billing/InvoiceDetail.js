import React, { Fragment, useState, useEffect, useContext } from 'react'
import { View, StyleSheet, Dimensions, ScrollView } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, Divider, List, Subheading, Button } from 'react-native-paper'
import Colors from '../../constants/Colors'
import AppLoading from '../../components/Loaders/AppLoader'
import * as utils from '../../app/utils'
import { normalize } from 'react-native-elements'
import PayInvoice from '../../components/Modals/PayInvoice'
import { REMOTE_ADDRESS } from '../../app/redux/actions'
import { AuthContext } from '../../context/Auth'

export default function InvoiceDetailScreen({route, navigation}){

    const {fetchInvoices, fetchProfile} = useContext(AuthContext)

    const [invoice, setInvoice] = useState()
    const [isModalOpened, setIsModalOpened] = useState(false)
    const [uri, setUri] = useState()

    /**
     * REDUX STATE
     */
    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){

            setInvoice(route.params.invoice)
            setUri(`${REMOTE_ADDRESS}viewinvoice.php?id=${route.params.invoice.id}&mobile=true`)
        }
        return () => {
            isCancelled = true
            setInvoice(null)
        }
    }, [invoice])



    const _payNow = () => {
        setIsModalOpened(true)
    }

    
    const _getInvoiceItems = items => {


        if(!items){
            return <Subheading style={{textAlign:'center', paddingVertical:10}}>No Invoice items available</Subheading>
        }
        
        return items.map((item, i)=><Fragment key={i}>
            <List.Item 
                title={item.type} 
                description={item.description} 
                descriptionStyle={{width:'100%'}} right={()=><View style={{ justifyContent:'center', alignItems:'center'}}><Subheading style={{color:Colors.accent,}}>R{item.amount}</Subheading></View>} style={{paddingLeft:0, marginLeft:-8}}/><Divider /></Fragment>)
    }


    const _closeModalHandler = () => {
        fetchInvoices()
        fetchProfile()
        setIsModalOpened(false)
        navigation.navigate('Unpaid')
        
    }
    
    return  !invoice ? 
                <AppLoading /> : 
                <AppWrapper>
                    <ScrollView contentContainerStyle={{paddingBottom:40}} style={{width:'100%', height:'100%'}}>
                        <Card style={{paddingVertical:10}}>
                            <Subheading style={{textAlign:'center', fontSize:normalize(14), color:Colors.secondary, textTransform:'uppercase'}}>Total</Subheading>
                            <Subheading style={{textAlign:'center', fontSize:normalize(20), color:Colors.secondary, color:Colors.danger}}>R{invoice.total}</Subheading>
                            {invoice.status == 'Unpaid' ? 
                               <View style={{alignItems:"center", width:'100%', justifyContent:'center'}}>
                                    <Subheading style={{textAlign:'center', fontSize:normalize(14), color:Colors.secondary,paddingVertical:10}}>
                                        Due date: {utils.date(invoice.duedate)}
                                    </Subheading>
                                    <Button mode="contained" labelStyle={{color:Colors.light}} style={{width:150, backgroundColor:'#cc0000'}} onPress={()=>_payNow()}>Pay now</Button>
                               </View>:
                                <Subheading style={{textAlign:'center', fontSize:normalize(20), textTransform:'uppercase', lineHeight:35, color:Colors.accent}}>Paid</Subheading>}
                        </Card>
                        <Card style={{marginTop:5}}>
                            <Card.Content>
                                <Subheading style={{textAlign:'center', textTransform:'uppercase', fontSize:normalize(16), paddingBottom:10}}>Items</Subheading>
                                <Divider />
                                {_getInvoiceItems(invoice.invoiceitems)}
                                <List.Item title="Tax" right={()=><Subheading style={{color:Colors.danger}}>R{invoice.tax}</Subheading>}  style={{paddingLeft:0, marginLeft:-8}}/>
                                <Divider />
                                <List.Item title="Tax rate" right={()=><Subheading style={{color:Colors.secondary}}>R{invoice.taxrate}</Subheading>}  style={{paddingLeft:0, marginLeft:-8}}/>
                                <Divider />
                                <List.Item title="Sub Total" right={()=><Subheading style={{color:Colors.tintColor}}>R{invoice.subtotal}</Subheading>}  style={{paddingLeft:0, marginLeft:-8}}/>
                                <List.Item title="Total" titleStyle={{fontWeight:'bold', textTransform:'uppercase'}}  right={()=><Subheading style={{color:Colors.accent, fontWeight:'bold'}}>R{invoice.total}</Subheading>}  style={{paddingLeft:0, marginLeft:-8}}/>
                                {invoice.status == 'Unpaid' ? <Fragment><Divider />
                                </Fragment>:null}
                            </Card.Content>
                        </Card>
                    </ScrollView>
                    <PayInvoice isModalOpened={isModalOpened} uri={uri} onCloseModal={()=>_closeModalHandler()} invoiceNumber={invoice.id}/>
                </AppWrapper>
}

