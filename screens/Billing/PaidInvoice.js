import React, { Fragment } from 'react'
import { View, Text, StyleSheet, FlatList, Dimensions } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { useSelector } from 'react-redux'
import { Card, Divider, List, Subheading } from 'react-native-paper'
import Colors from '../../constants/Colors'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import * as utils from '../../app/utils'
import { normalize } from 'react-native-elements'

export default function PaidInvoiceScreen({route, navigation}){

    /**
     * REDUX STATE
     */
    const invoices = useSelector(state=>state.billing.invoices)
    
    
    return  <AppWrapper>
                <Card>
                    <Card.Content>
                        
                        <FlatList 
                            data={invoices}
                            renderItem={({item})=><Fragment>
                                
                                <List.Item 
                                title={'#' + item.id} 
                                description={item.date}
                                right={()=><View style={{flexDirection:'row', alignItems:'center'}}><Subheading style={{color:Colors.accent}}>R{item.total}</Subheading><MaterialCommunityIcons name="chevron-right" size={25} color={Colors.secondary}/></View>}
                                style={{paddingLeft:0, marginLeft:0}}
                                onPress={()=>navigation.navigate(utils.navigation.InvoiceDetail, {invoice:item, invoiceNo:'#' + item.id})}
                                />
                                <Divider />
                            </Fragment>}
                            keyExtractor={item=>item.id.toString()}
                            style={styles.flatList}
                            ListEmptyComponent={<View style={{justifyContent:'center', alignItems:'center'}}><Subheading>Empty</Subheading></View>}
                            />
                    </Card.Content>
                    
                </Card>
            </AppWrapper>
            
}


/**
 * STYLES
 */
const styles = StyleSheet.create({
    screen:{
        width:'100%',
        height:'100%',
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor:'#f1f1f1'
    },
    
    unpaidInvoicesContainer:{ 
        width:'100%', 
        paddingHorizontal:10, 
        marginVertical:10, 
        overflow:'hidden',
        height:Dimensions.get('screen').height <=568 ? 260 : null,
    },
    flatList:{
        width:'100%',
        height: 'auto',
        maxHeight:200,
        flexGrow: 0
    },

    title:{fontSize:normalize(16), textTransform:'uppercase'}
})