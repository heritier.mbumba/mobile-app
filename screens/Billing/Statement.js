import React from 'react'
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, ScrollView } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, List, Divider } from 'react-native-paper'
import moment from 'moment'
import { normalize } from 'react-native-elements'

export default function StatementScreen(props){

    const _get30DaysStatement = () => {}
    const _get60DaysStatement = () => {}
    const _get90DaysStatement = () => {}
    const _get120DaysStatement = () => {}
    const _getFullDaysStatement = () => {}
    
    return  <AppWrapper>
                <Card>
                    <ScrollView style={{width:'100%', paddingHorizontal:10, paddingVertical:10}}>
                        <TouchableOpacity onPress={()=>_get30DaysStatement()}>
                            <List.Item title="30 Day Statement" description={moment().subtract('30', 'days').format('DD-MM-YYYY')} right={()=><List.Icon icon="chevron-right"/>}/>
                            <Divider />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>_get60DaysStatement()}>
                            <List.Item title="60 Day Statement" description={moment().subtract('60', 'days').format('DD-MM-YYYY')} right={()=><List.Icon icon="chevron-right"/>}/>
                            <Divider />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>_get90DaysStatement()}>
                            <List.Item title="90 Day Statement" description={moment().subtract('90', 'days').format('DD-MM-YYYY')} right={()=><List.Icon icon="chevron-right"/>}/>
                            <Divider />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>_get120DaysStatement()}>
                            <List.Item title="120+ Day Statement" description={moment().subtract('120', 'days').format('DD-MM-YYYY')} right={()=><List.Icon icon="chevron-right"/>}/>
                            <Divider />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>_getFullDaysStatement()}>
                            <List.Item title="Full Statement" right={()=><List.Icon icon="chevron-right"/>}/>
                            <Divider />
                        </TouchableOpacity>
                    </ScrollView>
                </Card>
            </AppWrapper>
            
}

/**
 * STYLES
 */

 const styles = StyleSheet.create({
    screen:{
        width:'100%',
        paddingHorizontal:10,
        marginTop:10
    },
    
    unpaidInvoicesContainer:{ 
        width:'100%', 
        paddingHorizontal:10, 
        marginVertical:10, 
        overflow:'hidden',
        height:Dimensions.get('screen').height <=568 ? 260 : null,
    },
    flatList:{
        width:'100%',
        height: 'auto',
        maxHeight:200,
        flexGrow: 0
    },

    title:{fontSize:normalize(16), textTransform:'uppercase'}
 })