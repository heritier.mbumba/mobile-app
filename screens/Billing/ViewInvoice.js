import React, { useEffect, useState } from 'react'
import {View, ActivityIndicator} from 'react-native'
import {WebView} from 'react-native-webview'
import { useSelector } from 'react-redux'
import AppLoading from '../../components/Loaders/AppLoader'
import Colors from '../../constants/Colors'
import {REMOTE_ADDRESS} from '../../app/redux/actions'
import {Linking} from 'expo'


export default function ViewInvoiceScreen({navigation, route}){

    const [isLoading, setIsLoading] = useState(true)
    const [getInvoiceId, setInvoiceId] = useState()
    const [getPayAll, setPayAll] = useState()

    useEffect(()=>{

        let isCancelled = false

        if(!isCancelled){
            if(route.params.id){
                setInvoiceId(route.params.id)
                setPayAll(false)
               
            }else{
                setPayAll(route.params.payall)
            }
            
        }

       

        return () => {
            isCancelled = true
        }
    }, [getPayAll, getInvoiceId])
    
   
    /**
     * REDUX STATE
     */
    const user = useSelector(state=>state.auth.user)
    const pswd = useSelector(state=>state.auth.svgp)
    

    const loading = <View style={{flex:1, justifyContent:'flex-start', alignItems:'center'}}><ActivityIndicator size="large" color={Colors.tintColor}/></View>

    
    const INJECTED_SCRIPT = `
                                document.getElementById('inputEmail').value = '${user.email}';
                                document.getElementById('inputPassword').value = '${pswd}';
                                document.forms[0].submit();
                                document.querySelector('.sidebar').style.display = 'none'
                            `
    const loadWithRequest = request => {
        
        
    }
    let content = <AppLoading title="Loading"/>

    

    if(getPayAll){
        content =  <WebView
                    source={
                        {
                            uri:`${REMOTE_ADDRESS}/clientarea.php?action=masspay&all=true&mobile=true`
                        }
                    }
                    incognito={true}
                    injectedJavaScript={INJECTED_SCRIPT}
                    originWhitelist={['*']}
                    startInLoadingState={isLoading}
                    renderLoading={() => loading}
                    onLoadEnd={syntheticEvent=>{
                        const { nativeEvent } = syntheticEvent;
                        navigation.setParams({invoiceTitle:nativeEvent.title})
                        setIsLoading(false)
                    }}
                    onError={onError}
                    style={{backgroundColor:Colors.light, flex:1}}/>
    }
    if(getInvoiceId){

       
        
        
        content =  <WebView
                source={
                    {
                        uri:`${REMOTE_ADDRESS}/viewinvoice.php?id=${getInvoiceId}&mobile=true`
                    }
                }
                
                // incognito={true}
                injectedJavaScript={INJECTED_SCRIPT}
                // originWhitelist={['*']}
                startInLoadingState={isLoading}
                renderLoading={() => loading}
                onLoadEnd={syntheticEvent=>{
                    const { nativeEvent } = syntheticEvent;
                    navigation.setParams({invoiceTitle:nativeEvent.title})
                    setIsLoading(false)
                }}
                
                
                style={{backgroundColor:Colors.light, flex:1}}/>
    }
    return  content
}

