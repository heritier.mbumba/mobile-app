import React, { useEffect, useContext } from 'react'
import { Notifications } from 'expo'
import * as Permissions from 'expo-permissions'
import {useSelector, useDispatch} from 'react-redux'
import { Text, ScrollView,  RefreshControl, TouchableOpacity} from 'react-native'
import AppWrapper from '../components/Hoc/AppWrapper'
import { Title, Subheading } from 'react-native-paper'
import CardBox from '../components/Card/Card'
import Colors from '../constants/Colors'
import CustomCard from '../components/Card/CustomCard'
import {ActionCard} from '../components/Card/ActionCard'
import { saveExpoToken } from '../app/redux/dispatchers/app'
import { AuthContext } from '../context/Auth'
import * as utils from '../app/utils'
import * as Device from 'expo-device'



const DashboardScreen = (props) => {

    /**
     * CONTEXT
     */
    const {fetchProfile, user, getDepartments, fetchInvoices, updateNotification, logActivity} = useContext(AuthContext)
    
    /**
     * STATE
     */
    const [refreshing, setRefreshing] = React.useState(false); //refresh scrollview state
    const [device, setDevice] = React.useState()
    /**
     * REFRESHING CONTROLLER
     */
    const onRefresh = React.useCallback(() => {
                        setRefreshing(true);
                        fetchProfile()
                        fetchInvoices()
                        utils.waitToRefresh(2000).then(() => setRefreshing(false));
                        }, [refreshing]);


    const dispatch = useDispatch()

    /**
     * REDUX STATE
     */
    const {isAuth} = useSelector(state=>state.auth)
    const {totalUnpaid, unpaidInvoices} = useSelector(state=>state.billing)
    const {totalActiveService, totalSuspendedService} = useSelector(state=>state.service)
    const {totalActiveDomains, totalExpiredDomains} = useSelector(state=>state.domain)


    /**
     * LISTEN TO NOTIFICATION
     */

     const listenToNofication = () => {
         Notifications.addListener(_handleNotification)
         
     }

     const _handleNotification = notification => {
         if(notification.data){ 
             // do whatever you want to do with the notification
             updateNotification('select', notification.data)
         }
      };

      /**
       * DEVICE INFO
       */
      const getDeviceInfo =  () => {
            setDevice(p=>({...p, brand:Device.brand,model:Device.modelName,osName:Device.osName,osVersion:Device.osVersion}))
      }

     /**
     * REGISTER EXO TOKEN
     */
    const registerPushNotification = async () => {
        try {
            
            const {status:existingStatus} = await Permissions.getAsync(Permissions.NOTIFICATIONS)
            let finalStatus = existingStatus;
            
            if(existingStatus != 'granted'){
                const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS)
                finalStatus = status;
            }
            
            if (finalStatus !== 'granted') {
                return;
            }
           
            let token = await Notifications.getExpoPushTokenAsync();
            setDevice(prev=>({...prev, token:token}))
            const platform = Device.osName
            dispatch(saveExpoToken(token, platform))
            
        } catch (error) {
           
            return
        }
        
    }

    useEffect(()=>{
        
        let isCancelled = false
        if(!isCancelled && isAuth){
            fetchProfile()
            fetchInvoices()
            registerPushNotification()
            getDepartments()
            listenToNofication()
            getDeviceInfo()
            
        }
        
        return () => {
            
            isCancelled = true
            
        }
    }, [isAuth])


    React.useEffect(()=>{
        if(device && device.token){
            logActivity('login', device)
        }
    }, [device])

    

   const _makePayment = () => {
        props.navigation.navigate(utils.navigation.Billing)
   }

   
   /**
    * MAKE INVOICE SINGLE PAYMENT
    */
   const _makeSinglePayment = () => {
        props.navigation.navigate(utils.navigation.Billing, {screen:utils.navigation.ViewInvoice, params:{id:unpaidInvoices[0].id}})
   }


    
    return  <AppWrapper>
                <ScrollView showsHorizontalScrollIndicator={false} contentContainerStyle={{paddingBottom:30}}  refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
                    <CustomCard title={utils.lang.CompanyName} subtitle={user != null ? user.companyname: ''} {...props} onPress={()=>props.navigation.push(utils.navigation.Account)}>
                        <Title style={{marginTop:0}}>{user != null ? user.firstname + ' ' + user.lastname: ''}</Title>
                            <Text style={{ color:'#ccc', paddingBottom:0 }}>{user != null ? user.email : ''}</Text>
                        <Subheading>{user != null ? utils.lang.CustomerCode + user.id : ''}</Subheading>
                        
                    </CustomCard>

                    {totalUnpaid == 0 ? null :<CardBox 
                        title={utils.lang.Currency + totalUnpaid} 
                        description={utils.lang.UnpaidAmount}
                        buttonName={utils.lang.PayButton}
                        single={unpaidInvoices.length > 1 ? false : true}
                        onPress={unpaidInvoices.length > 1 ? _makePayment : _makeSinglePayment}

                        />}

                    {totalActiveService > 0 ? 
                        <ActionCard 
                            title={totalActiveService > 1 ? utils.lang.ActiveServices : utils.lang.ActiveService} 
                            description={utils.lang.ViewYourActiveServices} 
                            content={totalActiveService} 
                            onPress={()=>props.navigation.navigate(utils.navigation.Services)} 
                            color={Colors.tintColor} padding/> : 
                            null
                    }

                    {totalSuspendedService > 0 && totalActiveService == 0 ? 
                        <ActionCard 
                            title={utils.lang.SuspendedServices} 
                            description={utils.lang.ViewSuspendedServices} 
                            content={totalSuspendedService} 
                            onPress={()=>props.navigation.navigate(utils.navigation.Services, {screen:utils.navigation.SuspendedService})} 
                            color={Colors.danger} padding/> : 
                            null
                    }

                    {totalActiveDomains > 0 ? 
                        <ActionCard 
                            title={utils.lang.ActiveDomains} 
                            description={utils.lang.ViewYourActiveDomains} 
                            content={totalActiveDomains} 
                            onPress={()=>props.navigation.navigate(utils.navigation.Domains)} 
                            color={Colors.accent} padding/> : 
                            null
                    }

                    {totalExpiredDomains > 0 && totalActiveDomains == 0 ? 
                        <ActionCard 
                            title={utils.lang.ExpiredDomains} 
                            description={utils.lang.ViewYourExpiredDomains} 
                            content={totalExpiredDomains} 
                            onPress={()=>props.navigation.navigate(utils.navigation.ExpiredDomain)} 
                            color={Colors.danger} padding/> : 
                            null
                    }

                    <ActionCard 
                        title={utils.lang.Feedback} 
                        description={utils.lang.SendFeedBack} 
                        icon="message-draw"  
                        onPress={()=>props.navigation.navigate(utils.navigation.Feedback)}/>

                    <ActionCard 
                        title={utils.lang.Billing} 
                        description={utils.lang.ViewYourBillingStatus} 
                        icon="receipt" 
                        onPress={()=>props.navigation.navigate(utils.navigation.Billing)}/>

                    <ActionCard 
                        title={utils.lang.SupportAndQueries} 
                        description={utils.lang.ViewAndOpenTicket} 
                        icon="face-agent"  
                        onPress={()=>props.navigation.navigate(utils.navigation.Support)}/>

                    <ActionCard 
                        title={utils.lang.AccountSettings} 
                        description={utils.lang.ViewAccountSetting} 
                        icon="account"  
                        onPress={()=>props.navigation.navigate(utils.navigation.More)}/>

                    
                </ScrollView>
            </AppWrapper>
            
}

export default DashboardScreen




