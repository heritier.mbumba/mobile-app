import React, { Fragment, useState, useEffect, useContext, useReducer, useCallback, useRef } from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { View, Text, StyleSheet, Dimensions, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native'
// import Header from '../../components/Header/Header'
import {Header, normalize} from 'react-native-elements'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, List, Divider, Subheading, Button } from 'react-native-paper'
import {ViewButton} from '../../components/Button/ViewButton'
import Colors from '../../constants/Colors'
import { LeftElement, RightElement } from '../../components/Header/HeaderElement'
import { AuthContext } from '../../context/Auth'
import { SafeAreaView, useSafeArea} from 'react-native-safe-area-context'
import moment from 'moment'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import * as utils from '../../app/utils'
import { loadMore, incrementIndex, decrementIndex, previousLoad } from '../../app/redux/dispatchers/domain'



const initialState = {
    
    loadMore:false
}

const reducer = (state, action) => {
    switch (action.type) {

        case 'init':
            return {
                ...state,
                loadMore:false
            }
        
        case 'loadMore':
            return {
                ...state,
                loadMore:true
            }
       
        default:
            throw new Error('failed')
    }
}

export default function DomainScreen(props){
    /**
     * REDUX STORE
     */
    const {activeDomains, activeLoaded, index} = useSelector(state=>state.domain)
    const [loadedDomains, setLoadedDomains] = useState()
    const {buttonLoading} = useSelector(state=>state.app)
    const [state, dispatch] = useReducer(reducer, initialState)
    const {fetchDomains} = useContext(AuthContext)

    const reduxDispatcher = useDispatch()

    

    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){
           
            fetchDomains()

        }


        return () => {
            isCancelled = true
        }
    }, [])


    useEffect(()=>{

        let isCancelled = false 
        if(!isCancelled){
           
        }
        return () => isCancelled = true
    }, [index])




    let height = Dimensions.get('screen').height

    const _loadMoreHandler = () => {
        
        dispatch({type:'loadMore'})
       
    }

    
    

    return  <AppWrapper>
                    <Card>
                        <Card.Title title={activeDomains.length > 0 ? activeDomains.length + " Active domains " : 'Active domains'} titleStyle={{fontSize:normalize(16), textTransform:'uppercase', color:Colors.tintColor, paddingTop:5}} subtitleStyle={{color:Colors.danger}}/>
                        <Divider />
                        <Card.Content style={{maxHeight:height < 600 ? '75%' : '90%'}}>
                            {buttonLoading ?<Subheading style={{textAlign:'center', paddingVertical:10}}>Loading...</Subheading> : (index > activeDomains.length ? <Subheading style={{textAlign:'center', paddingVertical:10}}>No more data to load</Subheading> : <FlatList 
                                data={activeLoaded}
                                renderItem={({item, index})=><Fragment>
                                    <List.Item
                                        title={item.domain}
                                        description={'Expiry Date: ' + utils.date(item.expirydate)}
                                        style={{paddingLeft:0, marginLeft:-8}}
                                        right={()=><View style={{flexDirection:'row', alignItems:'center'}}><MaterialCommunityIcons name="chevron-right" size={25} color={Colors.secondary}/></View>}
                                        onPress={()=>props.navigation.navigate('DomainDetail', {domain:item, domainName:item.domain})} />
                                        
                                        {index === (activeLoaded.length - 1) ? null : <Divider />}
                                </Fragment>}
                                getItemLayout = {(data, index) => ({
                                    length: 60,
                                    offset: 60 * index,
                                    index
                                    })}
                                style={{}}
                                initialNumToRender={10}
                                keyExtractor={item=>item.id.toString()}
                                onEndReached={()=>_loadMoreHandler()}
                                onEndReachedThreshold={.1}
                                ListEmptyComponent={<View style={{width:'100%', justifyContent:'center', paddingTop:10}}><Subheading style={{color:Colors.secondary}}>No domain available</Subheading></View>}
                                
                                />)}
                               
                            {buttonLoading ? 
                                <ActivityIndicator size="small" color={Colors.tintColor}/> :

                                (activeDomains.length > 20 ?

                                    <View style={{flexDirection:'row', justifyContent: 'center', paddingTop:10}}>
                                        {index == 0 ? null : 

                                            <Button onPress={()=>reduxDispatcher(previousLoad())} labelStyle={{color:Colors.secondary}}>Previous</Button>
                                        }

                                        {index > activeDomains.length ? null :

                                            <Button onPress={()=>reduxDispatcher(loadMore('domains', state.index))} labelStyle={{}}>Load more</Button>
                                        }</View> : 
                                    null
                                )
                            }
                        </Card.Content>
                    </Card>
                                    
                </AppWrapper>
            
}


