import React, { useState, useEffect } from 'react'
import { useSelector} from 'react-redux'
import { StyleSheet, ScrollView } from 'react-native'
import {normalize} from 'react-native-elements'
import AppWrapper from '../../components/Hoc/AppWrapper'
import AppLoading from '../../components/Loaders/AppLoader'
import { Card, List, Divider, Subheading } from 'react-native-paper'
import Colors from '../../constants/Colors'
import moment from 'moment'


export default function DomainSettingScreen({route}){
    const [domain, setDomain] = useState()
    /**
     * REDUX STATE
     */

     const {activeDomains} = useSelector(state=>state.domain)
   
    useEffect(() => {

        let isCancelled = false

        if(!isCancelled){

            setDomain(route.params.domain)
        }
       
        return () => {
            isCancelled = true
        };
    }, [domain, activeDomains])
    

    return  !domain ? <AppLoading /> : <AppWrapper>
                <ScrollView contentContainerStyle={{paddingBottom:20}}>
                        <Card>
                            <Subheading style={{textAlign:'center', fontSize:normalize(18), color:Colors.accent, paddingTop:20, paddingHorizontal:15}}>{domain.domain}</Subheading>
                            <Subheading style={{textAlign:'center', fontSize:normalize(14), color:Colors.secondary}}>Expires on {moment(domain.expirydate).format('DD-MM-YYYY')}</Subheading>
                            <Subheading style={{textAlign:'center', fontSize:normalize(14), color:Colors.secondary, textTransform:'uppercase', paddingVertical:5}}>Renewal cost</Subheading>
                            <Subheading style={{textAlign:'center', fontSize:normalize(20), color:Colors.danger,paddingBottom:20}}>R{domain.recurringamount}</Subheading>
                        </Card>
                        
                        <Card style={{marginTop:5}}>
                            <Card.Content>
                                <Subheading style={{textAlign:'center', textTransform:'uppercase', fontSize:normalize(16), paddingBottom:10}}>Settings</Subheading>
                                <Divider />
                                <List.Item 
                                    title="ID Protection"
                                    description={domain.idprotection == 0 ? 'Inactive' : 'Active'}
                                    titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                    descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                    style={{paddingLeft:0, marginLeft:-8}}
                                    />
                                <Divider />
                                <List.Item 
                                    title="Auto renew"
                                    description={domain.donotrenew == 1 ? 'Disabled' : 'Enabled'}
                                    titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                    descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                    style={{paddingLeft:0, marginLeft:-8}}
                                    />
                                <Divider />
                                <List.Item 
                                    title="Status"
                                    description={domain.status}
                                    titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                    descriptionStyle={{fontSize:normalize(18), color:Colors.accent}} 
                                    style={{paddingLeft:0, marginLeft:-8}}
                                    />
                                <Divider />
                                <List.Item 
                                    title="Next due date"
                                    description={domain.nextduedate}
                                    titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                    descriptionStyle={{fontSize:normalize(18), color:Colors.accent}} 
                                    style={{paddingLeft:0, marginLeft:-8}}
                                    />
                                <Divider />
                                <List.Item 
                                    title="Registration date"
                                    description={domain.registrationdate}
                                    titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                    descriptionStyle={{fontSize:normalize(18), color:Colors.accent}} 
                                    style={{paddingLeft:0, marginLeft:-8}}
                                    />
                                <Divider />
                                <List.Item 
                                    title="Period"
                                    description={domain.registrationperiod + ' year'}
                                    titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                    descriptionStyle={{fontSize:normalize(18), color:Colors.accent}} 
                                    style={{paddingLeft:0, marginLeft:-8}}
                                    />
                                
                            </Card.Content>
                        </Card>
                </ScrollView>
                </AppWrapper>
            
}

/**
 * STYLES
 */
const styles = StyleSheet.create({
    screen:{
        width:'100%',
        height:'100%',
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor:'#f1f1f1'
    },
    header:{
        width:'100%'
    },
    activeDomains:{
        width:'100%',
        paddingHorizontal:10,  
        marginTop:10
    },
    autoRenew:{
        width:'100%',
        paddingHorizontal:10,  
        marginTop:10
    }
    
})