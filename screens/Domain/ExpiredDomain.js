import React, { Fragment } from 'react'
import { useSelector } from 'react-redux'
import { View, StyleSheet, Dimensions, FlatList } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, List, Divider, Subheading } from 'react-native-paper'
import Colors from '../../constants/Colors'
import { normalize } from 'react-native-elements'
import {ViewButton} from '../../components/Button/ViewButton'


export default function ExpiredDomainScreen(props){
    /**
     * REDUX STORE
     */
    const expiredDomains = useSelector(state=>state.domain.expiredDomains)

    let height = Dimensions.get('screen').height
    
    return  <AppWrapper>
                <Card>
                    <Card.Title title="Expired Domains" subtitle="These domains will be lost" titleStyle={{color:Colors.danger, fontSize:normalize(14), textTransform:'uppercase'}} />
                    <Divider />
                    <Card.Content style={{maxHeight:height < 600 ? '75%' : '85%'}}>
                        <FlatList
                            data={expiredDomains}
                            renderItem={({item, index})=><Fragment>
                                <List.Item
                                        title={item.domain}
                                        description={'Domain price: ' + item.recurringamount ? 'R' + item.recurringamount : ''}
                                        descriptionStyle={{color:Colors.dangerLight, fontSize:normalize(12)}}
                                        style={{paddingLeft:0, marginLeft:-8}}
                                        right={()=><ViewButton title="Renew" color={Colors.danger}/>} />
                                {index === expiredDomains.length - 1 ?  null : <Divider />}
                            </Fragment>}
                            getItemLayout = {(data, index) => ({
                                length: 60,
                                offset: 60 * index,
                                index
                                })}
                            style={styles.flatList1}
                            initialNumToRender={10}
                            keyExtractor={item=>item.id.toString()}
                            ListEmptyComponent={<View style={{justifyContent:'center', paddingTop:10}}><Subheading>No expired domains</Subheading></View>}
                             />
                    </Card.Content>
                </Card>     
        </AppWrapper>
}


/**
 * STYLES
 */
const styles = StyleSheet.create({
    screen:{
        width:'100%',
        height:'100%',
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor:'#f1f1f1'
    },
    header:{
        width:'100%'
    },
    activeDomains:{
        width:'100%',
        paddingHorizontal:10,  
        marginTop:10
    },
    expiredDomains:{
        width:'100%',
        paddingHorizontal:10,
        marginTop:10
    }
    
})