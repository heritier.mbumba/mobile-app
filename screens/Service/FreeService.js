import React, {  useEffect} from 'react'
import {useSelector } from 'react-redux'
import { View, FlatList, Dimensions, TouchableOpacity } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import { Card, Divider, Subheading, List} from 'react-native-paper'
import { normalize } from 'react-native-elements'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import * as utils from '../../app/utils'

export default function FreeServiceScreen({route, navigation}){

    /**
     * REDUX STATE
     */
    const {freeServices} = useSelector(state=>state.service)

    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){
            
        }

        return () => {
            isCancelled = true
        }
        
    }, [])
    

     let height = Dimensions.get('screen').height


     
    return  <AppWrapper>
                <Card>
                    <Card.Title title={freeServices.length + " Free Services"} titleStyle={{color:Colors.danger, fontSize:normalize(15), textTransform:'uppercase'}}/>
                    <Divider />
                    <Card.Content style={{maxHeight:height < 600 ? '75%' : '85%'}}>
                        <FlatList 
                            data={[]}
                            renderItem={({item})=><TouchableOpacity onPress={()=>navigation.navigate(utils.navigation.FreeServiceDetail, {service:item, serviceName:item.product != null ? item.product.name:'No Product Name'})}>
                                <List.Item 
                                title={item.product != null ? item.product.name:'No Product Name'} 
                                description={'Next due date: ' + utils.dateFromNow(item.nextduedate)}
                                descriptionStyle={{color:Colors.danger}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                right={()=><View style={{justifyContent:'center', alignItems:'center'}}><MaterialCommunityIcons name="chevron-right" size={25} color={Colors.secondary}/></View>}
                                />
                                <Divider />
                            </TouchableOpacity>}
                            getItemLayout = {(data, index) => ({
                                length: 60,
                                offset: 60 * index,
                                index
                                })}
                            keyExtractor={item=>item.id.toString()}
                            style={{}}
                            />
                    </Card.Content>
                
                </Card>
            </AppWrapper>
}

