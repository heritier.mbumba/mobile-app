import React, { useContext, useEffect, useState, useReducer } from 'react'
import {useSelector} from 'react-redux'
import { View,  StyleSheet, FlatList, Dimensions } from 'react-native'
import { normalize} from 'react-native-elements'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import { Card, Divider, Button, Title, List, Subheading } from 'react-native-paper'
import ListItem from '../../components/PC/List'
import { AuthContext } from '../../context/Auth'
import * as utils from '../../app/utils'

const initialState = {
    index:0,
    max:50,
    next:10
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'load':

            return {
                ...state,
                next:action.payload
            }

    
        default:
            throw new Error();
    }
}

export default function ServiceScreen({route, navigation}){

 
    /**
     * REDUX STATE
     */
    const activeServices = useSelector(state=>state.service.activeServices)

    const [state, dispatch] = useReducer(reducer, initialState)

    const {fetchServices} = useContext(AuthContext)


    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){

            fetchServices()
        }

        return () => {
            isCancelled = true
        }
    }, [])
   
    let height = Dimensions.get('screen').height



    let content = <View style={{width:'100%', justifyContent:'center', alignItems:'center', height:'100%'}}><Subheading style={{color:Colors.secondary}}>No available service</Subheading></View>

    if (activeServices.length > 0) {
        content = <Card>
                    <Card.Title title={activeServices.length > 0 ? activeServices.length + ' Active Services': 'Active Services'} titleStyle={{color:Colors.tintColor, fontSize:normalize(16), textTransform:'uppercase', paddingTop:5}} subtitleStyle={{color:Colors.danger}}/>
                    <Divider />
                    <Card.Content style={{maxHeight:height < 600 ? '75%' : '85%'}}>
                        <FlatList
                            data={activeServices}
                            renderItem={({item})=><ListItem title={item.product != null ? item.product.name : 'No product name'} description={'Next due: ' + utils.date(item.nextduedate)} navigate={()=>navigation.navigate(utils.navigation.ServiceDetail, {service:item, serviceName:item.product != null ? item.product.name : 'No product name'})} buttonText="Detail"/>}
                            getItemLayout = {(data, index) => ({
                                length: 60,
                                offset: 60 * index,
                                index
                                })}
                            keyExtractor={item=>item.id.toString()}
                            initialNumToRender={10}
                            style={styles.flatList}
                        />
                    </Card.Content>
                    </Card>
        }

    
    return   <AppWrapper>
                       {content} 
            </AppWrapper>
            
}


/**
 * STYLES
 */
const styles = StyleSheet.create({
    header:{
        width:'100%',
        paddingHorizontal:10,
        paddingVertical:10
    },
    activeServicesContainer:{
        width:'100%',
        paddingHorizontal:10,
        marginTop:10
    },

})