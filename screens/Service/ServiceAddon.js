import React, { useEffect } from 'react'
import {useSelector} from 'react-redux'
import { View, FlatList, Dimensions } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import { Card, Divider, Subheading, List} from 'react-native-paper'
import { normalize } from 'react-native-elements'
import ListItem from '../../components/PC/List'
import * as utils from '../../app/utils'

export default function ServiceAddonScreen({route, navigation}){

    /**
     * REDUX STATE
     */
    const {addons} = useSelector(state=>state.service)
    

    let height = Dimensions.get('screen').height

   
    return  <AppWrapper>
                <Card>
                <Card.Title title={addons.length > 0 ? addons.length + ' Active Addons':0} titleStyle={{color:Colors.tintColor, fontSize:normalize(16), textTransform:'uppercase'}} subtitleStyle={{color:Colors.danger}}/>
                    <Divider />
                    <Card.Content style={{maxHeight:height < 600 ? '75%' : '85%'}}>
                        <FlatList
                            data={addons}
                            renderItem={({item})=><ListItem title={item.addondetail != null ? item.addondetail.name : 'No addon name'} description={'Next due: ' + utils.date(item.nextduedate)} navigate={()=>navigation.navigate(utils.navigation.ServiceAddonDetail, {addon:item, addonName:item.addondetail != null ? item.addondetail.name : 'No addon name'})} buttonText="Detail"/>}
                            getItemLayout = {(data, index) => ({
                                length: 60,
                                offset: 60 * index,
                                index
                                })}
                            keyExtractor={item=>item.id.toString()}
                            initialNumToRender={10}
                            ListEmptyComponent={<View style={{width:'100%', justifyContent:'center', paddingTop:10}}><Subheading style={{color:Colors.secondary}}>No available addon</Subheading></View>}
                            />
                    </Card.Content>
                
                </Card>
            </AppWrapper>
}

