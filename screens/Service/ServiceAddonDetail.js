import React, {useEffect, useState } from 'react'
import {useSelector} from 'react-redux'
import { ScrollView } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import { Card, Divider, Subheading, List} from 'react-native-paper'
import AppLoading from '../../components/Loaders/AppLoader'
import { normalize } from 'react-native-elements'
import moment from 'moment'

export default function ServiceAddonDetailScreen({route, navigation}){

    const [addon, setAddon] = useState()

    /**
     * REDUX STATE
     */
    const {activeServices} = useSelector(state=>state.service)

    const getServiceName = id => {
        const name = activeServices.find(s=>s.id==id).product.name
        return name
    }

    const getMainServiceStatus = id => {
        return activeServices.find(s=>s.id==id).domainstatus
    }
    
     useEffect(()=>{

        let isCancelled = false

        if(!isCancelled){

            setAddon(route.params.addon)
        }
         
        return () => {
            isCancelled = true
        }
         
     }, [addon])

    return  !addon ? <AppLoading /> : 
            <AppWrapper>
                <ScrollView contentContainerStyle={{paddingBottom:30}}>

                    <Card>
                        <Subheading style={{textAlign:'center', fontSize:normalize(18), color:Colors.accent, paddingTop:20, paddingHorizontal:15}}>{addon.addondetail ? addon.addondetail.name :'No addon name'}</Subheading>
                        
                        <Subheading style={{textAlign:'center', fontSize:normalize(14), color:Colors.secondary, paddingVertical:5}}>Next due {moment(addon.nextduedate).format('DD-MM-YYYY')}</Subheading>
                        <Subheading style={{textAlign:'center', fontSize:normalize(18), color:Colors.secondary,paddingBottom:5}}>R{addon.recurring}</Subheading>
                        <Subheading style={{textAlign:'center', fontSize:normalize(14), color:Colors.tintColor,paddingBottom:10}}>{addon.billingcycle}</Subheading>
                        <Divider />
                        <Subheading style={{textAlign:'center', fontSize:normalize(13), color:Colors.secondary, paddingTop:10, paddingHorizontal:5, textTransform:'uppercase'}}>Main product</Subheading>
                        <Subheading style={{textAlign:'center', fontSize:normalize(13), color:Colors.tintColor, paddingHorizontal:5,}}>{getServiceName(addon.hostingid)}</Subheading>
                        <Subheading style={{textAlign:'center', fontSize:normalize(13), color:Colors.accent, paddingHorizontal:5, paddingBottom:15}}>{getMainServiceStatus(addon.hostingid)}</Subheading>
                    </Card>

                </ScrollView>
            </AppWrapper>
}

