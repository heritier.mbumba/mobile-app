import React, { Fragment, useEffect, useState } from 'react'
import { StyleSheet,ScrollView } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import { Card, Divider, Subheading, List} from 'react-native-paper'
import AppLoading from '../../components/Loaders/AppLoader'
import { normalize } from 'react-native-elements'

export default function ServiceDetailScreen({route}){

    const [service, setService] = useState()

    
     useEffect(()=>{

        let isCancelled = false

        if(!isCancelled){

            setService(route.params.service)
        }
         
        return () => {
            isCancelled = true
        }
         
     }, [service])

    
    return  !service ? <AppLoading /> : 
            <AppWrapper>
                <ScrollView contentContainerStyle={{paddingBottom:30}}>

                    <Card>
                        <Subheading style={{textAlign:'center', fontSize:normalize(18), color:Colors.accent, paddingTop:10, paddingHorizontal:10}}>{service.product ? service.product.name :'No product name'}</Subheading>
                        <Subheading style={{textAlign:'center', fontSize:normalize(14), color:Colors.secondary, paddingVertical:5}}>Next due {service.nextinvoicedate}</Subheading>
                        <Subheading style={{textAlign:'center', fontSize:normalize(18), color:Colors.secondary,paddingBottom:5}}>R{service.amount}</Subheading>
                        <Subheading style={{textAlign:'center', fontSize:normalize(14), color:Colors.tintColor,paddingBottom:10}}>{service.billingcycle}</Subheading>
                    </Card>

                    <Card style={{marginTop:5}}>
                        <Card.Content>
                            <Subheading style={{textAlign:'center', textTransform:'uppercase', fontSize:normalize(14), paddingBottom:10}}>Detail</Subheading>
                            <Divider />
                            
                            <List.Item 
                                title="Status"
                                description={service.domainstatus}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                            <Divider />
                            <List.Item 
                                title="Payment Method"
                                description={service.paymentmethod}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                
                                />
                            <Divider />
                            {service.username ? 
                            <Fragment>
                                <List.Item 
                                title="Username"
                                description={service.username}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                                <Divider />
                            </Fragment> : null }
                        </Card.Content>
                    </Card>
                    {service.bwusage && service.bwlimit ? <Card style={{marginTop:5}}>
                        <Card.Content>
                            <Subheading style={{textAlign:'center', textTransform:'uppercase', fontSize:normalize(14), paddingBottom:10}}>Disk Usage</Subheading>
                            <Divider />
                            {service.bwusage ? 
                            <Fragment>
                                <List.Item 
                                title="Disk usage"
                                description={service.bwusage + 'M'}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                                <Divider />
                            </Fragment>:null}
                            
                            {service.bwlimit ? 
                            <Fragment>
                                <List.Item 
                                title="Disk limit"
                                description={service.bwlimit + 'M'}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                                <Divider />
                            </Fragment>:null}
                            
                            
                        </Card.Content>
                    </Card> : null}

                    {service.hostingaddons.length > 0 ? <Card>
                        <Card.Content>
                            <Subheading style={{textAlign:'center', textTransform:'uppercase', fontSize:normalize(14), paddingBottom:10}}>Addons</Subheading>
                            <Divider />
                        </Card.Content>
                    </Card> : null}

                    {service.servername ? <Card style={{marginTop:5}}>
                        <Card.Content>
                            <Subheading style={{textAlign:'center', textTransform:'uppercase', fontSize:normalize(14), paddingBottom:10}}>Server Detail</Subheading>
                            <Divider />
                            {service.dedicatedip ? 
                            <Fragment><List.Item 
                                title="Dedicated IP"
                                description={service.dedicatedip}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                                <Divider />
                            </Fragment> : null}
                            {service.ns1.length > 1? 
                            <Fragment>
                                <List.Item 
                                title="Server 1"
                                description={service.ns1}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                                <Divider />
                            </Fragment> : null}
                            {service.ns2.length > 1 ? 
                            <Fragment>
                                <List.Item 
                                title="Server 2"
                                description={service.ns2}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}} 
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                                <Divider />
                            </Fragment> : null }
                            
                            {service.servername.assignedips ? 
                            <Fragment>
                                <List.Item 
                                title="Ass. Ips"
                                description={service.servername.assignedips}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                                <Divider />
                            </Fragment> : null}
                            {service.servername.ipaddress ? 
                            <Fragment>
                                <List.Item 
                                title="IP address"
                                description={service.servername.ipaddress}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                                <Divider />
                            </Fragment> : null}
                            {service.servername.name ? 
                            <Fragment>
                                <List.Item 
                                title="Server name"
                                description={service.servername.name}
                                titleStyle={{textTransform:'uppercase', fontSize:normalize(14)}}
                                descriptionStyle={{fontSize:normalize(18), color:Colors.accent}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                />
                            <Divider />
                            </Fragment> : null}
                            
                        </Card.Content>
                    </Card> : null }

                </ScrollView>
            </AppWrapper>
}
