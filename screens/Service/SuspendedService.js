import React, {  useEffect} from 'react'
import {useSelector } from 'react-redux'
import { View, FlatList, Dimensions, TouchableOpacity } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import { Card, Divider, Subheading, List} from 'react-native-paper'
import { normalize } from 'react-native-elements'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import * as utils from '../../app/utils'

export default function SuspendedServiceScreen({route, navigation}){

    /**
     * REDUX STATE
     */
    const {suspendedServices} = useSelector(state=>state.service)
    const {invoiceIdSuspendedService} = useSelector(state=>state.app)

    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){
            if(invoiceIdSuspendedService != null){
                navigation.navigate(utils.navigation.ViewInvoice, {id:invoiceIdSuspendedService})
            }
        }

        return () => {
            isCancelled = true
        }
        
    }, [])
    

     let height = Dimensions.get('screen').height
     
    return  <AppWrapper>
                <Card>
                    <Card.Title title={suspendedServices.length > 0 ? suspendedServices.length + " Suspended Services" : 'Suspended Services'} subtitle="These services will be terminated soon" titleStyle={{color:Colors.danger, fontSize:normalize(15), textTransform:'uppercase'}}/>
                    <Divider />
                    <Card.Content style={{maxHeight:height < 600 ? '75%' : '85%'}}>
                        <FlatList 
                            data={suspendedServices}
                            renderItem={({item})=><TouchableOpacity onPress={()=>navigation.navigate(utils.navigation.SuspendedServiceDetail, {service:item, serviceName:item.product != null ? item.product.name:'No Product Name'})}>
                                <List.Item 
                                title={item.product != null ? item.product.name:'No Product Name'} 
                                description={'Next due date: ' + utils.dateFromNow(item.nextduedate)}
                                descriptionStyle={{color:Colors.danger}}
                                style={{paddingLeft:0, marginLeft:-8}}
                                right={()=><View style={{justifyContent:'center', alignItems:'center'}}><MaterialCommunityIcons name="chevron-right" size={25} color={Colors.secondary}/></View>}
                                />
                                <Divider />
                            </TouchableOpacity>}
                            getItemLayout = {(data, index) => ({
                                length: 60,
                                offset: 60 * index,
                                index
                                })}
                            keyExtractor={item=>item.id.toString()}
                            style={{}}
                            ListEmptyComponent={<View style={{justifyContent:'center', paddingTop:10}}><Subheading>No suspended service</Subheading></View>}
                            />
                    </Card.Content>
                
                </Card>
            </AppWrapper> 
}

