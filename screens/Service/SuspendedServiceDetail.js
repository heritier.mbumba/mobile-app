import React, { useEffect, useState, useContext } from 'react'
import {useSelector} from 'react-redux'
import { Text } from 'react-native'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import { Card, Divider, Subheading, Button} from 'react-native-paper'
import AppLoading from '../../components/Loaders/AppLoader'
import { ScrollView } from 'react-native-gesture-handler'
import { normalize } from 'react-native-elements'
import { AuthContext } from '../../context/Auth'
import * as utils from '../../app/utils'

export default function SuspendedServiceDetailScreen({route, navigation}){

    const {getInvoiceIdSuspendedService} = useContext(AuthContext)

    const [service, setService] = useState()
    

    /**
     * REDUX STATE
     */
    const {invoiceIdSuspendedService} = useSelector(state=>state.app)

    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){
            if(route.params.service){
                setService(route.params.service)
                getInvoiceIdSuspendedService(route.params.service.id)
            }
            
        }

        return () => {
            isCancelled = true
        }
       
    }, [])


    let addons = null
    if(service){
        if(service.hostingaddons.length > 0){
            addons = service.hostingaddons.map((a, i)=><Subheading key={i} style={{ textAlign:'center', paddingVertical:5, paddingHorizontal:15}}>{a.addondetail.name} - <Text style={{color:Colors.accent, fontSize:normalize(10)}}>{a.status}</Text></Subheading>)

        }
    }

    
    
    
    return  service ? <AppWrapper>
                        <ScrollView contentContainerStyle={{paddingBottom:40}}>
                            <Card>
                                <Card.Content>
                                    <Subheading style={{ textAlign:'center', textTransform:'uppercase'}}>Invoice due date</Subheading>
                                    <Subheading style={{textAlign:'center', textTransform:'uppercase', fontSize:normalize(12), paddingBottom:10}}>{utils.date(service.nextduedate)} - {utils.dateFromNow(service.nextduedate)}</Subheading>
                                    <Divider />
                                    <Subheading style={{ textAlign:'center', textTransform:'uppercase', paddingTop:10}}>Amount</Subheading>
                                    <Subheading style={{ textAlign:'center', textTransform:'uppercase', paddingBottom:10, fontSize:normalize(30), lineHeight:40, color:Colors.accent}}>R{service.amount}</Subheading>
                                    {invoiceIdSuspendedService !== 'already paid' || !invoiceIdSuspendedService ? <Button labelStyle={{color:Colors.danger}} onPress={()=>navigation.navigate(utils.navigation.ViewInvoice, {id:invoiceIdSuspendedService.id})}>Pay now</Button> : <Subheading style={{ textAlign:'center', textTransform:'uppercase', color:Colors.accent}}>{invoiceIdSuspendedService}</Subheading>}
                                </Card.Content>
                            </Card>
                            <Card style={{marginTop:5}}>
                                <Card.Content>
                                    <Subheading style={{ textAlign:'center', textTransform:'uppercase', paddingBottom:10}}>Service Detail</Subheading>
                                    <Divider />
                                    <Subheading style={{ textAlign:'center', paddingVertical:10, color:Colors.tintColor}}>{service.product.name}</Subheading>

                                </Card.Content>
                            </Card>

                            {service.hostingaddons.length > 0 ? <Card style={{marginTop:5}}>
                                <Card.Content>
                                    <Subheading style={{ textAlign:'center', textTransform:'uppercase'}}>Addons</Subheading>
                                    {addons}
                                </Card.Content>
                            </Card>:null}
                        </ScrollView>
                    </AppWrapper> : <AppLoading />

}


