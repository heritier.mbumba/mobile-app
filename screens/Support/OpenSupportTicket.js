import React, { useState, useEffect, useContext, useReducer } from 'react'
import { View, Text, StyleSheet, KeyboardAvoidingView,ActivityIndicator, Platform } from 'react-native'
import { Button, Subheading } from 'react-native-paper'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import DropdownInput from '../../components/Dropdown/Dropdown'
import { useSelector } from 'react-redux'
import Error from '../../components/Error/Error'
import Response from '../../components/Message/Response'
import { normalize, Input } from 'react-native-elements'
import { AuthContext } from '../../context/Auth'
import * as utils from '../../app/utils'


const initialState = {
    subject:null,
    priority:null,
    service:null,
    message:null,
    attachements:null,
    next:'subject'
};

function reducer(state, action) {
    switch (action.type) {
      case 'subject':
        
        return {
            ...state,
            subject:action.payload,
            next:'service'
        }
        
     
     case 'service':
        
         return {
             ...state,
             service:action.payload,
             next:'message'
         }

     case 'message':
         return {
             ...state,
             message:action.payload,
         }

    case 'attachements':
        return {
            ...state,
            attachements:action.payload
        }

    case 'reset':
        return {
            ...state,
            next:'subject'
        }
      default:
        throw new Error();
    }
  }




export default function OpenSupportTicketScreen({route, navigation}){
    const { submitTicket, getTickets } = useContext(AuthContext)


    const [ticket, setTicket] = useState({
        subject:null,
        priority:2,
        service:null,
        message:null,
        attachements:[],

    })

    

    const [state, dispatch] = useReducer(reducer, initialState);

    const [error, setError] = useState()

    /**
     * REDUX STATE
     */
    const {successMsg, buttonLoading,} = useSelector(state=>state.app)
    const errorMsg = useSelector(state=>state.app.error)
    const {activeServices} = useSelector(state=>state.service)

    

    useEffect(()=>{
        let isCancelled = false
        let timeOut;
        if(!isCancelled){
            setTicket(prev=>({...prev, groupid:route.params.departId, groupname:route.params.department}))

            if(successMsg){
                timeOut = setTimeout(()=>{
                    navigation.navigate(utils.navigation.Support)
                    getTickets()
                }, 2500)
            }
        }

        

        return () => {
            isCancelled =true
            clearTimeout(timeOut)
        }
        
        
    }, [successMsg, error, errorMsg])

    


    const services = []

    if(activeServices.length > 0){
        activeServices.map(s=>services.push({label:s.product ? s.product.name : 'No name -' + s.id, value:s.id}))
    }

    /**
     * GET PRIORITY
     */
    const _getPriority = value => {
        setTicket(prev=>({...prev, priority:value}))
        
        dispatch({type:'priority', payload:value})
    }

    /**
     * GET SUBJECT
     */
    const _getSubject = value => {
        setTicket(prev=>({...prev, subject:value}))
    }

    /**
     * GET RELATED SERVICE
     */
    const _getRelatedService = value => {
        setTicket(prev=>({...prev, service:value}))
    }


    /**
     * SUBJECT TICKET HANDLER
     */
    const _submitTicketHandler = () => {
        if(!ticket.subject || !ticket.message ){
            setError('You cannot submit an empty form')
            return 
        }
        const data = {
            ...ticket
        }

     
        submitTicket(data)
    }

    const goNext = () => {
        switch (state.next) {
            case 'subject':
                if(!ticket.subject){
                    setError('Please enter the subject')
                    return
                }
                dispatch({type:'subject'})
            break;
        
            case 'service':
                dispatch({type:'service'})
            break;

            case 'message':
                dispatch({type:'message'})
            break;

        }
    }

    return  <AppWrapper>
                <Error title={errorMsg}/>
                {successMsg ? <Response message={successMsg}/>:
                <KeyboardAvoidingView style={{justifyContent:'center', alignItems:'center', height:'100%'}} behavior={Platform.OS == 'ios' ? "padding" : null}>
                    <View style={{width:'100%'}}>
                        <Error title={error}/>
                        
                    {buttonLoading ? 
                        <View style={{flexDirection:'row', alignItems:'center', paddingLeft:10}}>
                            <ActivityIndicator size="small" color={Colors.tintColor}/> 
                            <Subheading style={{textAlign:'center', paddingVertical:10, fontSize:normalize(18), color:Colors.tintColor, paddingLeft:10}}>creating ticket...</Subheading>
                        </View> :  null}
                    {state.next == 'subject' ?
                    
                        <Input
                            label="Subject"
                            onChangeText={subject=>_getSubject(subject)}
                            labelStyle={{textTransform:'uppercase', paddingVertical:10}}
                            placeholder="i.e website design changes"
                            inputContainerStyle={{borderBottomColor:Colors.grayText, paddingVertical:10, backgroundColor:Colors.light}}
                            inputStyle={{paddingLeft:10, color:Colors.secondary}}
                            autoFocus
                            returnKeyType="next"
                           
                            onContentSizeChange={()=>setError('')}
                            onSubmitEditing={()=>ticket.subject ? dispatch({type:'subject', payload:ticket.subject}) : setError('Please enter the subject')}
                            /> : null
                    
                    }
                    
                    
                    {state.next == 'service' ? (activeServices.length > 0 ? <View style={{paddingHorizontal:10}}>
                        <Subheading style={{paddingVertical:10, textTransform:'uppercase', fontWeight:'bold'}}>Related Service</Subheading>
                        <View style={{backgroundColor:Colors.light}}>
                            <DropdownInput data={services} label="Related service" onValueChange={service=>_getRelatedService(service)}/>
                        </View>
                    </View>:dispatch({type:'service'})):null}

                   {state.next == 'message' ? 
                   <View>
                       <Input
                            label="Message"
                            onChangeText={message=>setTicket(p=>({...p, message:message}))}
                            labelStyle={{textTransform:'uppercase', paddingVertical:10}}
                            placeholder="write your message"
                            inputContainerStyle={{borderBottomColor:Colors.grayText, paddingVertical:10, backgroundColor:Colors.light}}
                            inputStyle={{paddingLeft:10, color:Colors.secondary, height:Platform.OS == 'ios' ? 150 : null}}
                            autoFocus
                            multiline
                            numberOfLines={6}
                            disabled={buttonLoading ? true : false}
                            textAlignVertical="top"
                            onContentSizeChange={()=>setError('')}
                            
                        />
                        <View style={{flexDirection:'row', paddingHorizontal:10, paddingVertical:10}}>
                            <Button mode="contained" onPress={()=>{dispatch({type:"reset"}); setTicket(p=>({...p, subject:null, message:null}))}} style={{backgroundColor:Colors.danger,}} labelStyle={{color:Colors.light}} disabled={buttonLoading ? true : false}>Reset</Button>
                            <Button mode="contained" onPress={_submitTicketHandler} labelStyle={{color:Colors.light}} disabled={buttonLoading ? true : false} style={{marginLeft:10}}>Submit</Button>
                        </View>
                   </View>:null}
                    {state.next == 'message' ? null : <Button onPress={()=>goNext()} style={{marginTop:10, marginHorizontal:10}}>Next</Button>}
                    </View>
                </KeyboardAvoidingView>}
            </AppWrapper>
               
}


/**
 * STYLES
 */
function ActiveIndicators({data}){

    let arr = []

    for(let key in data){
        if(data[key]){
            arr.push(key)
        }
    }

    let dots = null
    if(arr.length > 0){
        dots = arr.map((d, i)=><Subheading key={i} style={{backgroundColor:Colors.tintColor, fontSize:normalize(12), width:20, height:20, textAlign:'center', borderRadius:50, marginRight:5, color:Colors.light}}>{i}</Subheading>)
    }
    return dots
}


const styles = StyleSheet.create({
    input:{
        
    }
})