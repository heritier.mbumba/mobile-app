import React, { useEffect, useContext } from 'react'
import { View, RefreshControl, ScrollView, Alert } from 'react-native'
import Colors from '../../constants/Colors'
import { useSelector } from 'react-redux'
import AppWrapper from '../../components/Hoc/AppWrapper'
import { Card, List, Divider, Subheading } from 'react-native-paper'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {normalize} from 'react-native-elements'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import Response from '../../components/Message/Response'
import { AuthContext } from '../../context/Auth'
import {REMOTE_ADDRESS} from '../../app/redux/actions'
import * as utils from '../../app/utils'
import * as WebBrowser from 'expo-web-browser'


const getIcon = name => {
    switch (name) {
        case 'Billing':
            return 'cash'
        case 'Hosting':
            return 'server'

        case 'Domains':
            return 'earth'

        case 'Sales':
            return 'face-agent'
        case 'Web Services':
            return 'application'

        case 'VPS and Dedicated Servers':
            return 'server-network'

    }
}



function wait(timeout) {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  }




export default function TicketScreen({navigation}){

    const {getTickets} = useContext(AuthContext)

    /**
     * REDUX STATE
     */
    const {groups} = useSelector(state=>state.group)
    const {tickets} = useSelector(state=>state.support)
    const {successMsg} = useSelector(state=>state.app)


    const [refreshing, setRefreshing] = React.useState(false); //refresh scrollview state

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getTickets()
        wait(2000).then(() => setRefreshing(false));
      }, [refreshing]);

    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){
            getTickets()
        }
        return () => {
            isCancelled = true
        }
    }, [])


    const _viewResolvedTicket = async () => {
        await WebBrowser.openBrowserAsync(`${REMOTE_ADDRESS}viewticket.php`)
    }

    const _resolveTicketAlert = () => {
        Alert.alert(utils.lang.Sorry, utils.lang.ResolvedTicketMessage, [{text:'No, thanks'}, {text:'Yes', onPress:_viewResolvedTicket}])
    }

    
    const departments = () => {
        if(groups.length > 0){
            return groups.map((item, i)=>   <TouchableOpacity key={i} onPress={()=>navigation.navigate(utils.navigation.OpenSupportTicket, {departId:item.id, department:item.name})}>
                <List.Item 
                    title={item.name} 
                    style={{}}
                    left={()=><View style={{justifyContent:'center'}}><MaterialCommunityIcons name={getIcon(item.name)} size={normalize(20)} color={Colors.gray}/></View>}
                    right={()=><List.Icon icon="chevron-right"/>}/>{i == groups.length - 1 ? null : <Divider />}
                </TouchableOpacity>)
        }
    }
    
    return  <AppWrapper>
                <ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}  contentContainerStyle={{paddingBottom:40}}>
                    <Response message={successMsg}/>
                    <Card>
                        <Subheading style={{textTransform:'uppercase', paddingVertical:10, textAlign:'center'}}>Ticket Stats</Subheading>
                        <Card.Content style={{flexDirection:'row', justifyContent:'space-between', flexWrap:'wrap', width:'100%'}}>
                            <TouchableOpacity style={{width:'100%'}} onPress={()=>navigation.navigate(utils.navigation.OpenedTicket)}>
                                <Subheading style={{textAlign:'center', fontSize:normalize(25), lineHeight:30, fontWeight:'bold', color:Colors.accent}}>{tickets.totalOpened ? tickets.totalOpened : 0}</Subheading>
                                <Subheading style={{textAlign:'center', textTransform:'uppercase', color:Colors.accent, fontSize:normalize(11)}}>Opened</Subheading>
                            </TouchableOpacity>
                            <TouchableOpacity style={{width:'100%'}} onPress={()=>navigation.navigate(utils.navigation.PendingTicket)}>
                                <Subheading style={{textAlign:'center',  fontSize:normalize(25), lineHeight:30, fontWeight:'bold', color:Colors.warningText}}>{tickets.totalPending ? tickets.totalPending : 0}</Subheading>
                                <Subheading style={{textAlign:'center', color:Colors.warningText, textTransform:'uppercase', fontSize:normalize(11)}}>Pending</Subheading>
                            </TouchableOpacity>
                            <TouchableOpacity style={{width:'100%'}} activeOpacity={1} onPress={()=>_resolveTicketAlert()}>
                                <Subheading style={{textAlign:'center', fontSize:normalize(25), lineHeight:30, fontWeight:'bold', color:Colors.tintColor}}>{tickets.totalResolved ? tickets.totalResolved : 0}</Subheading>
                                <Subheading style={{textAlign:'center',  color:Colors.tintColor, textTransform:'uppercase', fontSize:normalize(11)}}>Resolved</Subheading>
                            </TouchableOpacity>
                            <TouchableOpacity style={{width:'100%'}} activeOpacity={1}>
                                <Subheading style={{textAlign:'center', fontSize:normalize(25), lineHeight:30, fontWeight:'bold', color:Colors.danger, opacity:.5}}>{tickets.totalClosed ? tickets.totalClosed : 0}</Subheading>
                                <Subheading style={{textAlign:'center',  color:Colors.danger, textTransform:'uppercase', fontSize:normalize(11), opacity:.5}}>Closed</Subheading>
                            </TouchableOpacity>
                        </Card.Content>
                    </Card>
                    
                {groups.length > 0 ? <Card style={{marginTop:10}}>
                    <Card.Content>
                        <Subheading style={{paddingVertical:10, textTransform:'uppercase'}}>Open ticket in</Subheading>
                        <Divider />
                        {departments()}
                    </Card.Content>
                </Card> : <Subheading style={{paddingVertical:10, color:Colors.danger, textAlign:'center'}}>Support Channel is not available</Subheading>}
            </ScrollView>
        </AppWrapper>

                
            
}
