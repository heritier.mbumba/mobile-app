import React, { useState, useEffect, Fragment, useContext, useRef, useCallback } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Picker, KeyboardAvoidingView,ActivityIndicator, Platform, Dimensions, Keyboard, Image, RefreshControl, SafeAreaView } from 'react-native'
import { Appbar, Card, Divider, List, TextInput, Button, Snackbar, Subheading } from 'react-native-paper'
import Header from '../../components/Header/Header'
import AppWrapper from '../../components/Hoc/AppWrapper'
import {ViewButton} from '../../components/Button/ViewButton'
import Colors from '../../constants/Colors'
// import Input from '../../components/Input/Input'
import DropdownInput from '../../components/Dropdown/Dropdown'
import { useSelector, useDispatch } from 'react-redux'
import Error from '../../components/Error/Error'
import AppLoading from '../../components/Loaders/AppLoader'
import { openTicket, getConversations, replyToTicket, closeTicket, resolveTicket } from '../../app/redux/dispatchers/support'
import { ScrollView } from 'react-native-gesture-handler'
import { closeSnack } from '../../app/redux/dispatchers/app'
import Response from '../../components/Message/Response'
import moment from 'moment'
import { normalize, Input } from 'react-native-elements'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { AuthContext } from '../../context/Auth'
import MessageTools from '../../components/Message/MessageTools'
import Attachement from '../../components/Message/Attachement'
import {navigation as nav} from '../../app/utils'


const refresh = time => {
    return new Promise((resolve=>{
        setTimeout(resolve, time)
    }))
}


export default function TicketDetailScreen({route, navigation}){

    const {user} = useContext(AuthContext)

    let _scrollRef = useRef()
    
    const [getTicket, setTicket] = useState()
    const [getInputPadding, setInputPadding] = useState(0)
    const [reply, setReply] = useState({ticketId:null, body:'', subject:''})
    const [refreshing, setRefreshing] = useState(false)
   
    const dispatch = useDispatch()


    
    
    /**
     * REDUX STATE
     */

     const {conversations} = useSelector(state=>state.support)
     const {isReplyingTicket, isFetching, buttonLoading, successMsg} = useSelector(state=>state.app)
     const {ticket} = useSelector(state=>state.group)


     /**
      * REFRESH CONTROL
      */
     const refreshHandler = useCallback(()=>{
        setRefreshing(true)
        dispatch(getConversations(ticket.id))
        refresh(2000).then(()=>setRefreshing(false))
    }, [refreshing])


    /**
     * EFFECT
     */

    useEffect(()=>{

        let isCancelled = false

        if(!isCancelled){
            if(ticket){
                dispatch(getConversations(ticket.id))
                setTicket(ticket)
                setReply(prev=>({...prev, ticketId:ticket.id, subject:ticket.subject}))
            }
    
        }

        return () => {
            isCancelled = true
            
        }

    }, [])

    
    const scrollHandler = () => {
        _scrollRef.current.scrollToEnd()
    }


    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){
            if(_scrollRef != null){
              
                scrollHandler()
            }
    
        }
        return () => {
            isCancelled = false
        }

    }, [])



    const cleanMessage = message => {
       
        if(ticket){
            
            const text = message.substr(ticket.id.toString().length, message.length)
            return text.trim()
        }

              
    }

    const cleanTicketMessage = message => {
        if(ticket){
           
            let text;
            if(ticket.description_text.indexOf(ticket.id) == -1){
                text = message.replace(/\s{2,}/g, ' ').trim()
                return text
            }
            
        }
        return
    }

    
    let content = <View style={{flex:1, justifyContent:'center', alignItems:'center'}}><Subheading>No response yet</Subheading></View>
    if(conversations && conversations.length > 0){
      
    content = conversations.map((c, i)=>c.responder_email == user.email ? 
   
    <View key={i} style={{marginVertical:8, backgroundColor:'#f5f7f9'}}>          
        
        <View style={{flexDirection:'row',justifyContent:'flex-start', borderBottomWidth:1, width:'100%', borderBottomColor:'#f1f1f1', alignItems:'center'}}>
            <View style={{padding:10, alignItems:'flex-end'}}>
                <MaterialCommunityIcons name="account" size={25} color={Colors.secondary}/>
            </View>
            <Text style={{textTransform:'none', color:Colors.secondary, padding:10}}>{moment(c.updated_at).fromNow()}</Text>
        </View>
        <View style={{}}>
            <Subheading style={{padding:10}}>{cleanMessage(c.message.trim())}</Subheading>
           
        </View>
    </View> : 
    <View key={i} style={{marginVertical:10, backgroundColor:'#e5f2fd'}}>
            <View style={{flexDirection:'row', justifyContent:'flex-start', borderBottomColor:'#bbdcfe', borderBottomWidth:1, width:'100%'}}>
                <View style={{padding:10, }}>
                    <Image source={require('../../assets/images/android/logo-hdpi.png')} style={{width:60, height:20}} resizeMethod="scale"/>
                </View>
                <Text style={{textTransform:'none', color:Colors.secondary, padding:10}}>{moment(c.updated_at).fromNow()}</Text>
            </View>
            <View style={{paddingBottom:10}}>
                <Subheading style={{padding:10}}>{c.message.replace(/\s{2,}/g, ' ')}</Subheading>
                 <Attachement data={c.attachments}/>
            </View>
        </View>)
    }

    const _clearInputRef = useRef()
    const _submitHandler = () => {
        dispatch(replyToTicket(reply))
        _clearInputRef.current.clear()
    }

    const _resolveTicket = id => {
       dispatch(resolveTicket(id))
       navigation.goBack()
    }



    return  <AppWrapper color={Colors.light}>
                    <Response message={successMsg} />
                    <SafeAreaView>
                    <View style={{height:'100%', justifyContent:'space-between', backgroundColor:Colors.light}}>
                        <ScrollView style={{height:'80%'}} ref={_scrollRef} refreshControl={<RefreshControl refreshing={refreshing} onRefresh={refreshHandler}/>}>
                            {isFetching ? <ActivityIndicator size="small" color={Colors.tintColor}/> : <Fragment>
                            
                            <View style={{marginVertical:8, backgroundColor:'#f5f7f9'}}>          
                                <View style={{flexDirection:'row',justifyContent:'flex-start', borderBottomWidth:1, width:'100%', borderBottomColor:'#f1f1f1', alignItems:'center'}}>
                                    <View style={{padding:10, alignItems:'flex-end'}}>
                                        <MaterialCommunityIcons name="account" size={25} color={Colors.secondary}/>
                                    </View>
                                    <Text style={{textTransform:'none', color:Colors.secondary, padding:10}}>{getTicket ? moment(getTicket.created_at).fromNow() : null}</Text>
                                </View>
                                <View style={{}}>
                                    <Subheading style={{padding:10}}>{getTicket ? cleanTicketMessage(getTicket.description_text) : null}</Subheading>
                                
                                </View>
                            </View>
                            {content}
                            </Fragment>}
                            
                        </ScrollView>
                        <KeyboardAvoidingView behavior={Platform.OS == "ios"?"padding":null} style={{width:Dimensions.get('screen').width, marginLeft:-10}} keyboardVerticalOffset={Platform.OS == "ios" ? 64 : -64}>
                        <View style={{backgroundColor:Colors.light, justifyContent:'center', alignItems:'center', marginVertical:5, borderTopColor:Colors.grayText, borderTopWidth:1}}>
                            <Divider />
                            {ticket.status == 2 || ticket.status == 3 ? <Input placeholder="Reply here" 
                            inputStyle={{paddingVertical:15, color:Colors.secondary, fontSize:normalize(13)}} 
                            inputContainerStyle={{borderBottomColor:Colors.light}}
                            onChangeText={message=>setReply(prev=>({...prev, body:message}))}
                            rightIconContainerStyle={{backgroundColor:Colors.tintColor, justifyContent:'center', alignContent:'flex-end'}}
                            ref={_clearInputRef}
                            rightIcon={()=>isReplyingTicket ? <View style={{width:50, height:20}}><ActivityIndicator size="small" color={Colors.light}/></View> : <TouchableOpacity onPress={()=>_submitHandler()}><List.Icon icon="send" color={Colors.light}/></TouchableOpacity>}
                            />:null}
                            
                            <MessageTools media={false}  onResolveTicket={()=>_resolveTicket(getTicket.id)} loading={buttonLoading}/>
                        </View>
                        </KeyboardAvoidingView>
                    </View>
                    </SafeAreaView>
                    
            </AppWrapper>
               
}


/**
 * STYLES
 */
const styles = StyleSheet.create({
    screen:{
        width:'100%',
        height:'100%',
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor:'#f1f1f1'
    },
    box:{
        width:'100%',
        paddingHorizontal:10,
        marginTop:10
    },
    snackbar:{
        backgroundColor:Colors.light,
        color:Colors.secondary 
    }
})