import React, { useState, useEffect, useContext } from 'react'
import { View, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Card, Divider, Subheading } from 'react-native-paper'
import AppWrapper from '../../components/Hoc/AppWrapper'
import Colors from '../../constants/Colors'
import { useSelector } from 'react-redux'
import { ScrollView } from 'react-native-gesture-handler'
import Response from '../../components/Message/Response'
import moment from 'moment'
import { normalize } from 'react-native-elements'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { AuthContext } from '../../context/Auth'
import * as utils from '../../app/utils'





export default function PendingTicketScreen({route, navigation}){

    const {setActiveTicket, getTickets} = useContext(AuthContext)
    const [getGroup, setGroup] = useState()
    /**
     * REDUX STATE
     */
    const {tickets} = useSelector(state=>state.support)
    const {groups} = useSelector(state=>state.group)
    const {buttonLoading, successMsg} = useSelector(state=>state.app)

    useEffect(()=>{
        let isCancelled = false
        if(!isCancelled){    
            setGroup(groups)
        }
        if(successMsg){
            utils.waitToRefresh(2000).then(()=>getTickets())
        }
        return () => {
            isCancelled = true
        }
    }, [successMsg])


    /**
     * SET ACTIVE TICKET
     */
    const _setActiveTicketHandler = ticket => {
        setActiveTicket(ticket)
        navigation.navigate(utils.navigation.TicketDetail, {title:ticket.subject})
    }

    let content;

    if(tickets && tickets.tickets.length > 0){
            content = tickets.tickets.filter(t=>t.status==3).map((item, i)=><TouchableOpacity key={i} onPress={()=>_setActiveTicketHandler(item)}>
                            <View style={{marginBottom:10}}>
                                <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                                    <View>
                                        <Subheading style={{color:Colors.secondary, fontSize:normalize(12), color:Colors.tintColor, textTransform:'uppercase'}}>{item.id}</Subheading>
                                        <Subheading>{item.subject}</Subheading>
                                        <Subheading style={{paddingBottom:10, fontSize:normalize(13), color:Colors.accent}}>{moment(item.created_at).fromNow()}</Subheading>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        {buttonLoading ? <ActivityIndicator size="small" color={Colors.tintColor}/> : null}
                                        <MaterialCommunityIcons name="chevron-right" size={normalize(20)} color={Colors.secondary}/>
                                    </View>
                                </View>
                                {i == tickets.tickets.length -1 ? null :<Divider />}
                            </View>
                        </TouchableOpacity>)
    }else{
            content = <View style={{flex:1, justifyContent:'center', alignItems:'center'}}><Subheading>{utils.lang.NoPendingTicket}</Subheading></View>

    }
    

    
    return  <AppWrapper>
                <ScrollView contentContainerStyle={{paddingBottom:30}}>
                    <Response message={successMsg}/>
                    <Card>
                        <Card.Content>
                            {content}
                        </Card.Content>
                    </Card>
                </ScrollView>
            </AppWrapper>
               
}
